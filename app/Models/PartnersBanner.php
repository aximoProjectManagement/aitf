<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PartnersBanner extends Model
{
    use HasFactory;
    protected $table = 'partners';
}

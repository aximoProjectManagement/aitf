<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\WhoweareBanner;

class WhoWeAreBannerController extends Controller
{
    public function whoweareBanner(Request $request){
        if($request->hasFile('image')){
        $imageName = time().'.'.$request->image->extension();
        $request->image->move(public_path('assets/whoweare'), $imageName);
        
        $data = new WhoweareBanner;
        $data->image = $imageName;
        $data->save();
        return redirect('/admin/who_we_are');
    }
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LimitedLiabilityPartnership extends Model
{
    protected $table = 'limited_liability_partnership';
    use HasFactory;
}

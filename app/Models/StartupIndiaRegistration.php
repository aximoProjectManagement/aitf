<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StartupIndiaRegistration extends Model
{
    protected $table = 'startup_india_registration';
    use HasFactory;
}

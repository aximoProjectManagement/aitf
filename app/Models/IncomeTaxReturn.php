<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IncomeTaxReturn extends Model
{
    protected $table = 'income_tax_return';
    use HasFactory;
}

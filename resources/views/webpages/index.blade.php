<!DOCTYPE html>
<html lang="en">
<head>
    <title>All India Tax Filing</title>
    @include('components/head-link')
</head>
@include('components/header')
    
<body class="s-it main-box">
    {{-- <div id="preloader"></div> --}}
    <div class="up">
        <a href="#" class="scrollup text-center"><i class="fas fa-chevron-up"></i></a>
    </div>
    
  

    <!-- Start of banner section
	============================================= -->
    <section id="it-up-banner" class="it-up-banner-section position-relative">
        <span class="it-up-banner-deco1 position-absolute"> <img src="assets/img/its/b-vector1.png" alt=""></span>
        <span class="it-up-banner-deco2 position-absolute"> <img src="assets/img/its/b-shape1.png" alt=""></span>
        <span class="it-up-banner-deco3 position-absolute"> <img src="assets/img/its/b-shape2.png" alt=""></span>
        <span class="it-up-banner-deco4 position-absolute"> <img src="assets/img/its/b-shape3.png" alt=""></span>
        <div class="container">
            <div class="it-up-banner-content position-relative">
                <div class="it-up-banner-text headline-1 pera-content">
                    <span>Focus On Business</span>
                    <h1>Our target is to reach the goal</h1>
                    <p class="focus-business">Our vertical solutions expertise allows your business to streamline workflow.</p>
                    <div class="it-up-banner-btn d-flex">
                        <div class="it-up-banner-play-btn text-center">
                            <a class="video_box" href="#"><i class="fas fa-play"></i></a>
                        </div>
                        <div class="it-up-banner-cta-btn text-center">
                            <a href="#">Get in Touch <i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                </div>
                <div class="it-up-banner-img ">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                          <div class="carousel-item active">
                            <img src="assets/img/its/b-m.png" class="d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="assets/img/its/b-m.png" class="d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="assets/img/its/b-m.png" class="d-block w-100" alt="...">
                          </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                    {{-- <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                          <div class="carousel-item active">
                            <img src="assets/img/its/b-m.png" class="d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="assets/img/its/abt1.jpg" class="d-block w-100" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="assets/img/its/b-m.png" class="d-block w-100" alt="...">
                          </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div> --}}
                    {{-- <img src="assets/img/its/b-m.png" alt="">
                    <span class="it-up-img-deco1 position-absolute" data-parallax='{"x" : -70}'> <img src="assets/img/its/slack.png" alt=""></span>
                    <span class="it-up-img-deco2 position-absolute" data-parallax='{"y" : -70}'> <img src="assets/img/its/dimond.png" alt=""></span>
                    <span class="it-up-img-deco3 position-absolute" data-parallax='{"y" : 70}'> <img src="assets/img/its/en.png" alt=""></span> --}}
                </div>
            </div>
        </div>
    </section>
    <!-- End of banner section
	============================================= -->

    <!-- Start of Featured section
	============================================= -->

    <section id="it-up-featured" class="it-up-featured-section position-relative">
        <span class="it-up-ft-bg position-absolute"><img src="assets/img/its/ft-bg.png" alt=""></span>
        <!-- <span class="it-up-ft-shape position-absolute"><img src="assets/img/its/ft-shape1.png" alt=""></span> -->
        <span class="it-up-ft-shape2 position-absolute"><img src="assets/img/its/b-shape3.png" alt=""></span>
        <div class="container">
            <div class="it-up-section-title-2 headline-1 text-center">
                
                <h2>Our Featured Services</h2>
            </div>
            <div class="it-up-featured-content">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="it-up-featured-innerbox text-center">
                            <div class="it-up-featured-icon">
                                <img src="assets/img/its/icon/ft-icon1.png" alt="">
                            </div>
                            <div class="it-up-featured-text headline-1 pera-content">
                                <h3><a href="gst_regis">GST REGISTRATION</a></h3>
                                <p>As per the GST Council, entities in the Northeaster and hill states with an annual turnover of Rs.20 lakhs and above would be required to obtain GST registration..</p>
                                <a class="it-up-ft-more" href="gst_regis">Read more <i class="flaticon-right-arrow"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="it-up-featured-innerbox text-center">
                            <div class="it-up-featured-icon">
                                <img src="assets/img/its/icon/ft-icon2.png" alt="">
                            </div>
                            <div class="it-up-featured-text headline-1 pera-content">
                                <h3><a href="propritership">PROPRIETORSHIP</a></h3>
                                <p>The Sole Proprietorship Is A Popular Business Form Due To Its Simplicity, Ease Of Setup, And Nominal Cost But Is Not A Legal Entity</p>
                                <a class="it-up-ft-more" href="propritership">Read more <i class="flaticon-right-arrow"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="it-up-featured-innerbox text-center">
                            <div class="it-up-featured-icon">
                                <img src="assets/img/its/icon/ft-icon3.png" alt="">
                            </div>
                            <div class="it-up-featured-text headline-1 pera-content">
                                <h3><a href="partnership">PARTNERSHIP</a></h3>
                                <p>A General Partnership is a business structure in which two or more individuals manage and operate a business in accordance with the terms and objectives set out in the Partnership Deed.</p>
                                <a class="it-up-ft-more" href="partnership">Read more <i class="flaticon-right-arrow"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="it-up-featured-innerbox text-center">
                            <div class="it-up-featured-icon">
                                <img src="assets/img/its/icon/ft-icon4.png" alt="">
                            </div>
                            <div class="it-up-featured-text headline-1 pera-content">
                                <h3><a href="one_person_company">ONE PERSON COMPANY</a></h3>
                                <p>One Person Company Is The Preferred Choice Of Business For Startups Who Have Only 1 Founder</p>
                                <a class="it-up-ft-more" href="one_person_company">Read more <i class="flaticon-right-arrow"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="it-up-featured-innerbox text-center">
                            <div class="it-up-featured-icon">
                                <img src="assets/img/its/icon/ft-icon5.png" alt="">
                            </div>
                            <div class="it-up-featured-text headline-1 pera-content">
                                <h3><a href="private_limited_company">PRIVATE LIMITED COMPANY</a></h3>
                                <p>Private Limited Company Is The Preferred Choice Of Business For Startups, Or If You Plan To Raise Funding</p>
                                <a class="it-up-ft-more" href="private_limited_company">Read more <i class="flaticon-right-arrow"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="it-up-featured-innerbox text-center">
                            <div class="it-up-featured-icon">
                                <img src="assets/img/its/icon/ft-icon6.png" alt="">
                            </div>
                            <div class="it-up-featured-text headline-1 pera-content">
                                <h3><a href="llp">LIMITED LIABILITY PARTNERSHIP</a></h3>
                                <p>Limited Liability Partnership Is The Preferred Choice For Businesses Who Want Limited Liability And A Separate Legal Entity</p>
                                <a class="it-up-ft-more" href="llp">Read more <i class="flaticon-right-arrow"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="it-up-featured-innerbox text-center">
                            <div class="it-up-featured-icon">
                                <img src="assets/img/its/icon/ft-icon1.png" alt="">
                            </div>
                            <div class="it-up-featured-text headline-1 pera-content">
                                <h3><a href="public_limited_company">PUBLIC LIMITED COMPANY</a></h3>
                                <p>Public Limited Company Is The Biggest Business Setup In India And Is Preferred For Businesses Who Want To Get Listed & Raise Funds</p>
                                <a class="it-up-ft-more" href="public_limited_company">Read more <i class="flaticon-right-arrow"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="it-up-featured-innerbox text-center">
                            <div class="it-up-featured-icon">
                                <img src="assets/img/its/icon/ft-icon2.png" alt="">
                            </div>
                            <div class="it-up-featured-text headline-1 pera-content">
                                <h3><a href="section_8_company">SECTION 8 COMPANY(NGO)</a></h3>
                                <p>Section 8 Company Is The Preferred Choice Of Business For Charitable Business</p>
                                <a class="it-up-ft-more" href="section_8_company">Read more <i class="flaticon-right-arrow"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="it-up-featured-innerbox text-center">
                            <div class="it-up-featured-icon">
                                <img src="assets/img/its/icon/ft-icon3.png" alt="">
                            </div>
                            <div class="it-up-featured-text headline-1 pera-content">
                                <h3><a href="trademark_registration">TRADEMARK REGISTRATION</a></h3>
                                <p>Trademark is very Important for any business or brand in India. Anyone who's want to secure their brand name and logo of the company, must Register for trademark in India.</p>
                                <a class="it-up-ft-more" href="trademark_registration">Read more <i class="flaticon-right-arrow"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="it-up-featured-innerbox text-center">
                            <div class="it-up-featured-icon">
                                <img src="assets/img/its/icon/ft-icon4.png" alt="">
                            </div>
                            <div class="it-up-featured-text headline-1 pera-content">
                                <h3><a href="startup_india">STARTUP INDIA REGISTRATION</a></h3>
                                <p>OStartUp Recognition is based on Government Scheme providing special benefits to Start Up entities subject to specific conditions as laid down in the rules.</p>
                                <a class="it-up-ft-more" href="startup_india">Read more <i class="flaticon-right-arrow"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="it-up-featured-innerbox text-center">
                            <div class="it-up-featured-icon">
                                <img src="assets/img/its/icon/ft-icon5.png" alt="">
                            </div>
                            <div class="it-up-featured-text headline-1 pera-content">
                                <h3><a href="msme_registration">MSME REGISTRATION</a></h3>
                                <p>With a hope to create five crore job opportunities in the MSME sector, the Government had announced about making changes to the MSME definition, where turnover will define an MSME, and not the investment put into it.</p>
                                <a class="it-up-ft-more" href="msme_registration">Read more <i class="flaticon-right-arrow"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- <div class="it-up-ft-btn text-center">
                    <a href="#">View More<i class="flaticon-right-arrow"></i></a>
                </div> -->
            </div>
        </div>
    </section>
    <!-- End of Featured section
	============================================= -->


    <!-- Start of about section
	============================================= -->
    <section id="it-up-about" class="it-up-about-section">
        <div class="container">
            <div class="it-up-about-content">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="it-up-about-img position-relative">
                            <span></span>
                            <div class="it-up-about-circle-progress position-absolute text-center pera-content">
                                <!-- <span class="circle-progress-icon"><img src="assets/img/its/cp-icon.png" alt=""></span> -->
                                <div class="first progress_area position-relative"><strong><span>%</span></strong></div>
                                <p>Overall Experiences</p>
                            </div>
                            <div class="it-up-about-img-wrap text-right">
                                <img src="assets/img/its/abt1.jpg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="it-up-about-text">
                            <div class="it-up-section-title headline-1">
                                <span>_About our Company_</span>
                                <h2>WHO WE ARE</h2>
                                <p>Allindiataxfiling.com provides fast and end-to-end Business Incorporation and associated services. We also provide Accounting, Taxation and Legal Services. We guarantee hassle-free service delivery in the shortest possible time without compromising on quality.</p>
                                <p>Our team of experienced professionals assists entrepreneurs in selecting the best business structure suiting individual requirements, and to convert their dream business into appropriate corporate entities.</p>
                            </div>
                            <div class="it-up-about-feature clearfix">
                                <div class="it-up-about-ft-item">
                                    <div class="it-up-about-ft-icon text-center float-left">
                                        <img src="assets/img/its/icon/ab-ft1.png" alt="">
                                    </div>
                                    <div class="it-up-about-ft-text headline-1 pera-content">
                                        <h3><a href="filing">Filing</a></h3>
                                        <p>Vertical solutions expertise allows your business.</p>
                                    </div>
                                </div>
                                <div class="it-up-about-ft-item">
                                    <div class="it-up-about-ft-icon text-center float-left">
                                        <img src="assets/img/its/icon/ab-ft2.png" alt="">
                                    </div>
                                    <div class="it-up-about-ft-text headline-1 pera-content">
                                        <h3><a href="registration">Registration</a></h3>
                                        <p>IT solutions expertise allows your business.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="it-up-about-btn">
                                <span>Want to work with us?</span>
                                <a href="#">Get started <i class="flaticon-right-arrow"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End of about section
	============================================= -->

    {{-- why aitf --}}
    <section id="it-up-achivement" class="it-up-achivement-section position-relative it-sec">
        <span class="it-up-achive-shape1 position-absolute"><img src="assets/img/its/achive-bg.png" alt=""></span>
        <span class="it-up-achive-shape2 position-absolute"><img src="assets/img/its/achive-bg2.png" alt=""></span>
        <div class="container">
            <div class="it-up-section-title headline-1 text-center">

                <h2>WHY AITF</h2>
            </div>
            <div class="it-up-achivement-content">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/taxes.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">24/7 SUPPORT</h5>
                                {{-- <p>Satisfied Clients</p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/idea.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">EXPERT ADVICE</h5>
                                {{-- <p>Team Member</p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/cyber-security.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">SECURITY</h5>
                                {{-- <p>Award Winner</p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/pie-chart.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">FILE IT RIGHT</h5>
                                {{-- <p>Works Done</p> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Start of service section
	============================================= -->
    <section id="it-up-service" class="it-up-service-section position-relative">
        <span class="it-up-service-shape position-absolute deco1"><img src="assets/img/its/s-shape1.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco2"><img src="assets/img/its/s-shape2.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco3"><img src="assets/img/its/s-shape3.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco4"><img src="assets/img/its/s-shape4.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco5"><img src="assets/img/its/s-shape5.png" alt=""></span>
        <div class="container">
            <div class="it-up-section-title headline-1 text-center">
                <span>_What are we doing?_</span>
                <h2>We are happy to share our client’s review.</h2>
            </div>
            <div class="it-up-service-content">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="it-up-service-tab-btn">
                            <ul id="tabs" class="nav text-capitalize nav-tabs">
                                <li class="nav-item"><a href="#" data-target="#marketing" data-toggle="tab" class="nav-link text-capitalize active">WE ARE DIFFERENT!</a></li>
                                <li class="nav-item"><a href="#" data-target="#designing" data-toggle="tab" class="nav-link text-capitalize">WHY CHOOSE</a></li>
                                <li class="nav-item"><a href="#" data-target="#development" data-toggle="tab" class="nav-link text-capitalize">SERVICES OFFERING !</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="it-up-service-tab-content">
                            <div id="tabsContent" class="tab-content">
                                <div id="marketing" class="tab-pane fade active show">
                                    <div class="it-up-service-tab-wrap clearfix">
                                        <div class="it-up-service-tab-text position-relative float-left ul-li-block headline-1">
                                            <h3>WE ARE DIFFERENT!</h3>
                                            <p>At Allindiataxfiling.com we ensure complete transparency at every step of the business incorporation process.We specialise in excellent customer service. When you call, email with our offices, you will receive personal attention from a knowledgeable specialist happy to help you .Our team of business startup specialists are here to keep you updated on the latest incorporation, tax and financial strategies and to help you manage important business details.</p>
                                            {{-- <a class="it-up-ser-btn" href="#">Check Details <i class="flaticon-right-arrow"></i></a>
                                            <span class="it-up-tab-icon position-absolute"><img src="assets/img/its/icon/tab-icon1.png" alt=""></span> --}}
                                        </div>
                                        <div class="it-up-service-img float-right">
                                            <img src="assets/img/its/ser-tab1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div id="designing" class="tab-pane fade">
                                    <div class="it-up-service-tab-wrap clearfix">
                                        <div class="it-up-service-tab-text position-relative float-left ul-li-block headline-1">
                                            <h3>WHY CHOOSE</h3>
                                            <ul>
                                                <li>Specialized knowledge and experience in Taxation & Business Registration.</li>
                                                <li>More communication than what larger agencies provide.</li>
                                                <li>Strategic partnership and advice for potential opportunities to grow your brand.</li>
                                                <li>Work is done in-house and not outsourced to a third party.</li>
                                                <li>We practice what we preach and use our own services to grow our business.</li>
                                            </ul>
                                            {{-- <a class="it-up-ser-btn" href="#">Check Details <i class="flaticon-right-arrow"></i></a>
                                            <span class="it-up-tab-icon position-absolute"><img src="assets/img/its/icon/tab-icon1.png" alt=""></span> --}}
                                        </div>
                                        <div class="it-up-service-img float-right">
                                            <img src="assets/img/its/ser-tab1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div id="development" class="tab-pane fade">
                                    <div class="it-up-service-tab-wrap clearfix">
                                        <div class="it-up-service-tab-text position-relative float-left ul-li-block headline-1">
                                            <h3>SERVICES OFFERING !</h3>
                                            <p>Allindiataxfiling.com provides fast and end-to-end Business Incorporation and associated services. We also provide Accounting, Taxation and Legal Services. We guarantee hassle-free service delivery in the shortest possible time without compromising on quality.
                                                <br>
                                                Our team of experienced professionals assists entrepreneurs in selecting the best business structure suiting individual requirements, and to convert their dream business into appropriate corporate entities.
                                            </p>
                                           
                                           
                                        </div>
                                        <div class="it-up-service-img float-right">
                                            <img src="assets/img/its/ser-tab1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pt-3">
        <div class="container">
        <div class="row">
                <div class="col-md-12 pt-5 pb-5">
                    <div class="it-up-section-title headline-1 text-center">
                        <h2>OUR ACHEIVEMENTS</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr">
                        <div class="counter-icon">
                            <i class="fas fa-medal"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Years</h3>
                            <span class="counter-value">10</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr green">
                        <div class="counter-icon">
                            <i class="fas fa-briefcase"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Projects</h3>
                            <span class="counter-value">947</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr blue">
                        <div class="counter-icon">
                            <i class="fas fa-user-tie"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Clients</h3>
                            <span class="counter-value">867</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr purple">
                        <div class="counter-icon">
                            <i class="fas fa-map-marker-alt"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Branches</h3>
                            <span class="counter-value">4</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </section>
    <section class="pt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 pt-5 pb-5">
                    <div class="it-up-section-title headline-1 text-center">
                        <h2>OUR PARTNERS</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="it-up-sponsor" class="it-up-sponsor-section">
                        <div class="container">
                            <div id="it-up-sponsor-slide" class="it-up-sponsor-slider owl-carousel">
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/gluehost.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/spellbound.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/aa.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/db.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/daxy.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img pt-4">
                                    <img src="assets/img/its/shrex.png" alt="">
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Start of achivement section
	============================================= -->
    <!-- <section id="it-up-achivement" class="it-up-achivement-section position-relative">
        <span class="it-up-achive-shape1 position-absolute"><img src="assets/img/its/achive-bg.png" alt=""></span>
        <span class="it-up-achive-shape2 position-absolute"><img src="assets/img/its/achive-bg2.png" alt=""></span>
        <div class="container">
            <div class="it-up-section-title headline-1 text-center">
                <span>_Our awesome funfact_</span>
                <h2>Our Achievement!</h2>
            </div>
            <div class="it-up-achivement-content">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/icon/ach-icon1.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h3 class="counter">1200</h3><strong>+</strong>
                                <p>Satisfied Clients</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/icon/ach-icon2.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h3 class="counter">55</h3><strong>+</strong>
                                <p>Team Member</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/icon/ach-icon3.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h3 class="counter">15</h3><strong>+</strong>
                                <p>Award Winner</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/icon/ach-icon4.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h3 class="counter">155</h3><strong>+</strong>
                                <p>Works Done</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <section id="it-up-testimonial" class="it-up-testimonial-section">
        <div class="container">
            <div class="it-up-section-title-2 headline-1 text-center">
                <span>Our Client’s Feedback</span>
                <h2>We are happy to share our client’s review.</h2>
            </div>
            <div class="it-up-testimonial-content position-relative">
                <span class="it-up-testi-shape1 position-absolute"><img src="assets/img/its/b-shape3.png" alt=""></span>
                <span class="it-up-testi-shape2 position-absolute" data-parallax='{"y" : 70}'><img src="assets/img/its/tst-sh1.png" alt=""></span>
                <span class="it-up-testi-shape3 position-absolute" data-parallax='{"x" : -70}'><img src="assets/img/its/tst-sh-1.png" alt=""></span>
                
                <div class="it-up-testimonial-slider-wrap owl-carousel">
                    <div class="it-up-testimonial-innerbox it-up-testimonial-blue text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst2.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p class="testi-para">I really like the services provided by AITF for its admirable service, instructive and precise. 
AITF always accommodating and ready to support for any query. I do recommend AITF.
</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Abhishek Raj Rathod</a></h4>
                                <span>- Founder- Aximo Infotech Pvt Ltd</span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="it-up-testimonial-innerbox it-up-testimonial-blue text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst1.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p class="testi-para">Everything from incorporation of your company to the trademark and all filing they coverd it all. one of the best CA consultant services they provide.</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Pranav Dhakulkar</a></h4>
                                <span>- Founder- Dryomix Concrete Solutions Pvt Ltd</span>
                            </div>
                        </div>
                    </div>
                    <div class="it-up-testimonial-innerbox it-up-testimonial-blue text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst2.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p class="testi-para">In single sentence "AITF " is one stop solution for services Like Company Incorporation, Trademark Registration or any work related to CA. Cost & services are very good.</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Bestayur Lifecare</a></h4>
                                <span>- Bestayur Lifecare</span>
                            </div>
                        </div>
                    </div>
                    <div class="it-up-testimonial-innerbox it-up-testimonial-blue text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst3.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p class="testi-para">Professional services at competitive price. They give us complete end to end support from incorporation to expansion. Highly recommended.

                                Keep it up.</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Abhishek kumar</a></h4>
                                <span>-  Founder-Koibhikaamcube</span>
                            </div>
                        </div>
                    </div>
                   
                    
                </div>
            </div>
        </div>
    </section>

    <!-- Start of contact section
	============================================= -->
    <section id="it-up-contact" class="it-up-contact-section position-relative">
        <span class="it-up-service-shape position-absolute deco1"><img src="assets/img/its/s-shape1.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco2"><img src="assets/img/its/s-shape2.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco3"><img src="assets/img/its/s-shape3.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco4"><img src="assets/img/its/s-shape4.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco5"><img src="assets/img/its/s-shape5.png" alt=""></span>
        <div class="container">
            <div class="it-up-section-title-2 headline-1 text-center">
                <span>_Let’s Work with us_</span>
                <h2>If you want to work please contact with us! </h2>
            </div>
            <div class="it-up-contact-content">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="it-up-contact-img position-relative">
                            <img src="assets/img/its/cn-bg.jpg" alt="">
                            <!-- <span class="contact-shape position-absolute" data-parallax='{"y" : -50}'><img src="assets/img/its/cn-sh1.png" alt=""></span> -->
                            <div class="it-up-contact-text text-center headline-1">
                                {{-- <img src="assets/img/its/marker.png" alt=""> --}}
                                {{-- <h4>14/A, Brown City, Keder Road, Australia.
                                </h4> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-7">
                        <div class="it-up-form-wrap">
                            <form method="post" action="save_contactusindex">
                                {{csrf_field()}}
                                <div class="it-up-form-input">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Full name</label>
                                            <input type="text" name="name" placeholder="Full Name" required>
                                        </div>
                                        <div class="col-md-6">
                                            <label>Email address</label>
                                            <input type="text" placeholder="Email Address" name="email" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="it-up-form-select position-relative">
                                    <label>Phone</label>
                                    <input type="text" placeholder="Phone Number" name="mobile" required> 
                                   
                                </div>
                                <div class="it-up-form-input">
                                    <label>Type your message</label>
                                    <textarea placeholder="Type your message" name="message" required></textarea>
                                </div>

                                <button type="submit" class="new-btn">Submit Now</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End of contact section
	============================================= -->

    <!-- Start of blog section
	============================================= -->
    <section id="it-up-blog" class="it-up-blog-section position-relative">
        <span class="it-up-blog-shape-bg1 position-absolute"><img src="assets/img/its/blg-shape1.png" alt=""></span>
        <span class="it-up-blog-shape-bg2 position-absolute"><img src="assets/img/its/blg-shape2.png" alt=""></span>
        <div class="container">
            <div class="it-up-section-title headline-1 text-center">
                <span>_Let’s Work with us_</span>
                <h2>If you want to work please contact with us! </h2>
            </div>
            <div class="it-up-blog-content">
                <div class="it-up-blog-slide  owl-carousel">

                    <div class="it-up-blog-innerbox it-up-blog-orange">
                        <div class="it-up-blog-img">
                            <img src="assets/img/its/blg1.jpg" alt="">
                        </div>
                        <div class="it-up-blog-text position-relative headline-1 pera-content">
                            <div class="it-up-blog-meta text-center">
                                08
                                <span>Nov/21</span>
                            </div>
                            <h3><a href="blog_page">In this context, our main
							approach was to build.</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiu.</p>
                            <div class="it-up-blog-bottom clearfix">
                                <div class="it-up-blog-author float-left">
                                    {{-- <div class="it-up-blog-ath-img float-left">
                                        <img src="assets/img/its/ba1.jpg" alt="">
                                    </div> --}}
                                    {{-- <div class="it-up-blog-ath-text text-uppercase">
                                        <a href="#">By Admin</a>
                                        <a href="#">0 Comments</a>
                                    </div> --}}
                                </div>
                                {{-- <div class="it-up-blog-share ul-li-block position-relative float-right">
                                    <span><i class="fas fa-share-alt"></i></span>
                                    <ul>
                                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-behance"></i></a></li>
                                        <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                    </ul>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div class="it-up-blog-innerbox it-up-blog-orange">
                        <div class="it-up-blog-img">
                            <img src="assets/img/its/blg2.jpg" alt="">
                        </div>
                        <div class="it-up-blog-text position-relative headline-1 pera-content">
                            <div class="it-up-blog-meta text-center">
                                08
                                <span>Nov/21</span>
                            </div>
                            <h3><a href="blog_page">In this context, our main
							approach was to build.</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiu.</p>
                            <div class="it-up-blog-bottom clearfix">
                                <div class="it-up-blog-author float-left">
                                    {{-- <div class="it-up-blog-ath-img float-left">
                                        <img src="assets/img/its/ba1.jpg" alt="">
                                    </div> --}}
                                    {{-- <div class="it-up-blog-ath-text text-uppercase">
                                        <a href="#">By Admin</a>
                                        <a href="#">0 Comments</a>
                                    </div> --}}
                                </div>
                                {{-- <div class="it-up-blog-share ul-li-block position-relative float-right">
                                    <span><i class="fas fa-share-alt"></i></span>
                                    <ul>
                                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-behance"></i></a></li>
                                        <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                    </ul>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div class="it-up-blog-innerbox it-up-blog-orange">
                        <div class="it-up-blog-img">
                            <img src="assets/img/its/blg3.jpg" alt="">
                        </div>
                        <div class="it-up-blog-text position-relative headline-1 pera-content">
                            <div class="it-up-blog-meta text-center">
                                08
                                <span>Nov/21</span>
                            </div>
                            <h3><a href="blog_page">In this context, our main
							approach was to build.</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiu.</p>
                            <div class="it-up-blog-bottom clearfix">
                                <div class="it-up-blog-author float-left">
                                    {{-- <div class="it-up-blog-ath-img float-left">
                                        <img src="assets/img/its/ba1.jpg" alt="">
                                    </div> --}}
                                    {{-- <div class="it-up-blog-ath-text text-uppercase">
                                        <a href="#">By Admin</a>
                                        <a href="#">0 Comments</a>
                                    </div> --}}
                                </div>
                                {{-- <div class="it-up-blog-share ul-li-block position-relative float-right">
                                    <span><i class="fas fa-share-alt"></i></span>
                                    <ul>
                                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-behance"></i></a></li>
                                        <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                    </ul>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div class="it-up-blog-innerbox it-up-blog-orange">
                        <div class="it-up-blog-img">
                            <img src="assets/img/its/blg2.jpg" alt="">
                        </div>
                        <div class="it-up-blog-text position-relative headline-1 pera-content">
                            <div class="it-up-blog-meta text-center">
                                08
                                <span>Nov/21</span>
                            </div>
                            <h3><a href="blog_page">In this context, our main
							approach was to build.</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiu.</p>
                            <div class="it-up-blog-bottom clearfix">
                                <div class="it-up-blog-author float-left">
                                    {{-- <div class="it-up-blog-ath-img float-left">
                                        <img src="assets/img/its/ba1.jpg" alt="">
                                    </div> --}}
                                    {{-- <div class="it-up-blog-ath-text text-uppercase">
                                        <a href="#">By Admin</a>
                                        <a href="#">0 Comments</a>
                                    </div> --}}
                                </div>
                                {{-- <div class="it-up-blog-share ul-li-block position-relative float-right">
                                    <span><i class="fas fa-share-alt"></i></span>
                                    <ul>
                                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-behance"></i></a></li>
                                        <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                    </ul>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div class="it-up-blog-innerbox it-up-blog-orange">
                        <div class="it-up-blog-img">
                            <img src="assets/img/its/blg3.jpg" alt="">
                        </div>
                        <div class="it-up-blog-text position-relative headline-1 pera-content">
                            <div class="it-up-blog-meta text-center">
                                08
                                <span>Nov/21</span>
                            </div>
                            <h3><a href="blog_page">In this context, our main
							approach was to build.</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiu.</p>
                            <div class="it-up-blog-bottom clearfix">
                                <div class="it-up-blog-author float-left">
                                    {{-- <div class="it-up-blog-ath-img float-left">
                                        <img src="assets/img/its/ba1.jpg" alt="">
                                    </div> --}}
                                    {{-- <div class="it-up-blog-ath-text text-uppercase">
                                        <a href="#">By Admin</a>
                                        <a href="#">0 Comments</a>
                                    </div> --}}
                                </div>
                                {{-- <div class="it-up-blog-share ul-li-block position-relative float-right">
                                    <span><i class="fas fa-share-alt"></i></span>
                                    <ul>
                                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-behance"></i></a></li>
                                        <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                    </ul>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                    <div class="it-up-blog-innerbox it-up-blog-orange">
                        <div class="it-up-blog-img">
                            <img src="assets/img/its/blg2.jpg" alt="">
                        </div>
                        <div class="it-up-blog-text position-relative headline-1 pera-content">
                            <div class="it-up-blog-meta text-center">
                                08
                                <span>Nov/21</span>
                            </div>
                            <h3><a href="blog_page">In this context, our main
							approach was to build.</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiu.</p>
                            <div class="it-up-blog-bottom clearfix">
                                <div class="it-up-blog-author float-left">
                                    {{-- <div class="it-up-blog-ath-img float-left">
                                        <img src="assets/img/its/ba1.jpg" alt="">
                                    </div> --}}
                                    {{-- <div class="it-up-blog-ath-text text-uppercase">
                                        <a href="#">By Admin</a>
                                        <a href="#">0 Comments</a>
                                    </div> --}}
                                </div>
                                {{-- <div class="it-up-blog-share ul-li-block position-relative float-right">
                                    <span><i class="fas fa-share-alt"></i></span>
                                    <ul>
                                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-behance"></i></a></li>
                                        <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                    </ul>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


  
    @include('components/footer')
    @include('components/foot-link')
</body>

</html>
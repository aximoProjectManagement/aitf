<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OnePersonCompany extends Model
{
    protected $table = 'one_person_company';
    use HasFactory;
}

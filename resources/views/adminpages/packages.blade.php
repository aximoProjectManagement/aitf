<!DOCTYPE html>
<html lang="en">

<head>
    <title>Packages</title>
    @include('adminpages/include/head-link')



</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        @include('adminpages/include/side-bar')

        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->

                @include('adminpages/include/header')

                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->



                    <!-- DataTales Example -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-2 text-gray-800">Packages</h1>
                        <a href="#" data-toggle="modal" data-target="#addpackages"
                            class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                            <i class="fas fa-plus fa-sm "></i> Add Image</a>
                    </div>
                    <div class="card shadow mb-4">

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Heading</th>
                                            <th>Description</th>
                                            <th>Price</th>
                                            <th>Offer</th>
                                            <th>Package Action</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach ($data as $i)
                                        <tr>
                                            <td>{{ $i->heading }}</td>
                                            <td>{{ $i->description}}</td>
                                            <td>{{ $i->price }}</td>
                                            <td>{{ $i->offer }}</td>
                                            <th>{{ $i->package_action }}</th>
                                            <td> <a href="#"><button data-toggle="tooltip" title="Edit"
                                                        class="pd-setting-ed"><i
                                                            class="fas fa-pencil-alt"></i></button></a>

                                                <a href="#"><button data-toggle="tooltip" title="Trash"
                                                        class="pd-setting-ed-1"><i
                                                            class="fas fa-trash"></i></button></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">

                @include('adminpages/include/footer')
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

   {{-- add packages modal start--}}
   <div class="modal fade" id="addpackages" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Client</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <div class="form-bg">
                        <div class="container">
                            <div class="row allot-form-row">
                                <div class="col-md-12">
                                    <div class="form-container pb-3">

                                        <form class="form-horizontal" method="POST" action="packages" enctype="multipart/form-data">
                                            {{csrf_field()}}
                                            
                                            <div class="form-group">
                                                <label>Heading</label>
                                                <input type="text" class="form-control"
                                                name="description"
                                                    placeholder="Heading" />
                                            </div>
                                            <div class="form-group">
                                                <label>Description</label>
                                                <input type="text" name="name" class="form-control" placeholder="Description" />
                                            </div>
                                           
                                            <div class="form-group">
                                                <label>Price</label>
                                                <input type="text" class="form-control"  name="designation" placeholder="Price" />
                                            </div>
                                            <div class="form-group">
                                                <label>Offer</label>
                                                <input type="text" name="name" class="form-control" placeholder="Offer" />
                                            </div>
                                            {{-- <div class="form-group">
                                                <label>Packages Action</label>
                                                <select class="custom-select">
                                                    <option selected>Open this select menu</option>
                                                    <option value="1"><i class="fas fa-check"></i></option>
                                                    <option value="2"><i class="fas fa-times"></i></option>
                                                
                                                  </select>
                                            </div> --}}
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
          
        </div>
    </div>
</div>

   {{-- add packages modal end --}}

    

    @include('adminpages/include/foot-link')

</body>

</html>

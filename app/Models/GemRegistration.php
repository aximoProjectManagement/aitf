<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GemRegistration extends Model
{
    protected $table = 'gem_registration';
    use HasFactory;
}

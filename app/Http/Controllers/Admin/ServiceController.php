<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\models\service;

class ServiceController extends Controller
{
    public function service(Request $request){
    if($request->hasFile('image')){
    $imageName = time().'.'.$request->image->extension();
    $request->image->move(public_path('assets/servicesimg'), $imageName);
    
    $data = new service;
    
    $data->heading= $request->input('heading');
    $data->description = $request->input('description');
    $data->read_more_link = $request->input('read_more_link');
    $data->image = $imageName;
    $data->save();
    return redirect('/admin/service_dashboard');
 }
    }
}

<?php

namespace App\Http\Controllers\Webpages;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ContactUs;


class UsersController extends Controller
{
    public function index(){
        return view('webpages.index');
    }
    public function about(){
        return view('webpages.about');
    }
    public function confidentiality(){
        return view('webpages.confidentiality');
    }
    public function disclaimer(){
        return view('webpages.disclaimer');
    }
    public function privacypolicy(){
        return view('webpages.privacy-policy');
    }
    public function refundpolicy(){
        return view('webpages.refund-policy');
    }

    public function saveContactUs(Request $request){
        $data = new ContactUs;
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->mobile = $request->input('mobile');
        $data->message = $request->input('message');
        $data->save();
        return redirect('/contact_us');
    }
    public function saveContactUsIndex(Request $request){
        $data = new ContactUs;
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->mobile = $request->input('mobile');
        $data->message = $request->input('message');
        $data->save();
        return redirect('/');
    }
   
}

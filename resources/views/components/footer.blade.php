<section id="it-up-footer" class="it-up-footer-section position-relative">
    <div class="container">
        <div class="it-up-footer-content-wrap">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="it-up-footer-widget headline-1 pera-content">
                        <div class="it-up-footer-logo-widget it-up-headline pera-content">
                            <div class="it-up-footer-logo">
                                <a href="index"><img src="assets/img/its/logo-1.png" alt=""></a>
                            </div>
                            <p>We consider it a real honor to help you Start Your Business and we're here to help you be
                                successful.The promoters of Allindiataxfiling.com are professionals with experience in
                                the fields of Corporate Law, Accounting & Taxation. We are committed to deliver quality
                                incorporation services utilizing standardized processing and modern technology.</p>

                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 ">
                    <div class="it-up-footer-widget headline-1 pera-content ">
                        <div class="it-up-footer-info-widget ul-li df-col-cc footer-info">
                            <h3 class=" text-center widget-title-before">Official info:</h3>
                            <ul class="footer-ul">
                                <li class="pt-2 pb-2">
                                    <i class="fas fa-chevron-right"></i>
                                    <a href="confidentiality">Confidentiality</a>
                                </li>
                                <li class="pt-2 pb-2">
                                    <i class="fas fa-chevron-right"></i>
                                    <a href="disclaimer">Disclaimer</a>
                                </li>
                                <li class="pt-2 pb-2">
                                    <i class="fas fa-chevron-right"></i>
                                    <a href="privacy_policy">Privacy Policy</a>
                                </li>
                                <li class="pt-2 pb-2">
                                    <i class="fas fa-chevron-right"></i>
                                    <a href="refund_policy">Refund Policy</a>
                                </li>
                                <li class="pt-2 pb-2">
                                    <i class="fas fa-chevron-right"></i>
                                    <a href="contact_us">Contact Us</a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 ">
                    <div class="it-up-footer-widget headline-1 pera-content ">
                        <div class="it-up-footer-info-widget ul-li df-col-cc footer-info">
                            <h3 class=" text-center widget-title-before">Useful Links:</h3>
                            <ul class="footer-ul">
                                <li class="pt-2 pb-2">
                                    <i class="fas fa-chevron-right"></i>
                                    <a href="confidentiality">About</a>
                                </li>
                                <li class="pt-2 pb-2">
                                    <i class="fas fa-chevron-right"></i>
                                    <a href="disclaimer">Startups</a>
                                </li>
                                <li class="pt-2 pb-2">
                                    <i class="fas fa-chevron-right"></i>
                                    <a href="privacy_policy">Filing</a>
                                </li>
                                <li class="pt-2 pb-2">
                                    <i class="fas fa-chevron-right"></i>
                                    <a href="refund_policy">Registration</a>
                                </li>
                                <li class="pt-2 pb-2">
                                    <i class="fas fa-chevron-right"></i>
                                    <a href="#">Blog</a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="it-up-footer-widget headline-1 pera-content ">
                        <div class="it-up-footer-info-widget ul-li ">
                            <h3 class="widget-title">Contact info:</h3>
                            <ul>
                                <li>
                                    <i class="fas fa-map-marker-alt"></i>
                                    <a href="#" class="max-w ">Delhi: C-3, 222, Janakpuri, Jail Road, New Delhi</a>
                                </li>
                                <li>
                                    <i class="fas fa-map-marker-alt"></i>
                                    <a href="#" class="max-w ">Ghaziabad: SF-9, 2nd Floor, Astoria Boulavard, RDC
                                        Rajnagar, Ghaziabad, 201001, U.P, India</a>
                                </li>
                                <li>
                                    <i class="fas fa-map-marker-alt"></i>
                                    <a href="#" class="max-w ">Mumbai: 2nd Floor, Modi House, Dalia Industrial Estate
                                        Off-Link Road, Andheri West, Mumbai - 400053</a>
                                </li>
                                <li>
                                    <i class="fas fa-map-marker-alt"></i>
                                    <a href="#" class="max-w ">Banglore: 7th Floor, 26/27, Raheja Towers, MG Road,
                                        Bengaluru - 560001</a>
                                </li>

                                <li>
                                    <i class="fas fa-phone-volume"></i>
                                    <a href="#" class="max-w ">Phone: +91 84484 49413</a>
                                </li>
                                <li>
                                    <i class="fas fa-envelope"></i>
                                    <a href="#" class="max-w ">Email: info@allindiataxfiling.com</a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="it-up-footer-copyright text-center pera-content">
        <div class="container">
            <p>© 2019 Allindiataxfiling.com, | All rights reserved</p>
        </div>
    </div>
</section>

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PartnersBanner;

class PartnersBannerController extends Controller
{
    public function partnersBanner(Request $request){
        if($request->hasFile('image')){
        $imageName = time().'.'.$request->image->extension();
        $request->image->move(public_path('assets/partners'), $imageName);
        
        $data = new PartnersBanner;
        $data->image = $imageName;
        $data->save();
        return redirect('/admin/partners');
    }
    }
}

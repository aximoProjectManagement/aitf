<!DOCTYPE html>
<html lang="en">

<head>
    <title>Client</title>
    @include('adminpages/include/head-link')



</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->

        @include('adminpages/include/side-bar')

        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->

                @include('adminpages/include/header')

                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->

                

                    <!-- DataTales Example -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-2 text-gray-800">Client</h1>
                        <a href="#" data-toggle="modal" data-target="#addclient"
                            class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                            <i class="fas fa-plus fa-sm "></i> Add Client</a>
                    </div>
                    <div class="card shadow mb-4">

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Client Photo</th>
                                            <th>Client Description</th>
                                            <th>Client Name</th>
                                            <th>Client Designation</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @foreach ($data as $i)
                                        <tr>
                                            <td>{{ $i->image }}</td>
                                            <td>{{ $i->description }}</td>
                                            <td>{{ $i->name }}</td>
                                            <td>{{ $i->designation }}</td>
                                            <td> <a href="#"><button data-toggle="tooltip" title="Edit"
                                                        class="pd-setting-ed"><i
                                                            class="fas fa-pencil-alt"></i></button></a>

                                                <a href="#"><button data-toggle="tooltip" title="Trash"
                                                        class="pd-setting-ed-1"><i
                                                            class="fas fa-trash"></i></button></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">

                @include('adminpages/include/footer')
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    {{-- add client modal --}}
    <div class="modal fade" id="addclient" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Client</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div>
                        <div class="form-bg">
                            <div class="container">
                                <div class="row allot-form-row">
                                    <div class="col-md-12">
                                        <div class="form-container pb-3">

                                            <form class="form-horizontal" method="POST" action="clients_upload" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                <div>
                                                    <h5>Size should be 500 x 700 </h5>
                                                </div>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                                                    </div>
                                                    <div class="custom-file">
                                                        <input type="file" class="custom-file-input" id="inputGroupFile01"
                                                        name="image"
                                                            aria-describedby="inputGroupFileAddon01">
                                                        <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Client Description</label>
                                                    <input type="text" class="form-control"
                                                    name="description"
                                                        placeholder="Client Description" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Client Name</label>
                                                    <input type="text" name="name" class="form-control" placeholder="Client Name" />
                                                </div>
                                               
                                                <div class="form-group">
                                                    <label>Client Designation</label>
                                                    <input type="text" class="form-control"  name="designation" placeholder="Client Designation" />
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Save changes</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
              
            </div>
        </div>
    </div>


    <!-- Bootstrap core JavaScript-->

    @include('adminpages/include/foot-link')

</body>

</html>

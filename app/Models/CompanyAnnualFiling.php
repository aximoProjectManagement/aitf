<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyAnnualFiling extends Model
{
    protected $table = 'company_annual_filing';
    use HasFactory;
}

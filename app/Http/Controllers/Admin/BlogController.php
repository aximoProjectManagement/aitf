<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;


class BlogController extends Controller
{
    public function blog(Request $request){
    if($request->hasFile('image')){
    $imageName = time().'.'.$request->image->extension();
    $request->image->move(public_path('assets/blogs'), $imageName);
    
    $data = new Blog;
    $data->date = $request->input('date');
    $data->heading= $request->input('heading');
    $data->content = $request->input('content');
    $data->image = $imageName;
    $data->save();
    return redirect('/admin/blogs');
 }
    }
}
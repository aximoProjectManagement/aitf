<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrademarkRegistration extends Model
{
    protected $table = 'trademark_registration';
    use HasFactory;
}

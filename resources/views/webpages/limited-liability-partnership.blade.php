<!DOCTYPE html>
<html lang="en">

<head>
    <title>Limited Liability Partnership</title>
    @include('components/head-link')
</head>
@include('components/header')

<body class="s-it">
    {{-- <div id="preloader"></div> --}}
    <div class="up">
        <a href="#" class="scrollup text-center"><i class="fas fa-chevron-up"></i></a>
    </div>
    <section class="pb-5">

<div class=" page-banner">
    <span class=""></span>
    <div class="row">
        <div class="col-md-12">
            <div>
            <ul class="my-breadcrump">
               <li class="home-bread"><a href="index">Home</a></li>
                <li class=""><i class="fas fa-chevron-right my-arrow"><a class="home-bread-1" href="startups">Startups</a></i></li>
                <li class=""><i class="fas fa-chevron-right my-arrow"><a class="home-bread-1" href="#">Limited Liability Partnership</a></i></li>

            </ul>

           <h1 class="page-banner-heading">Limited Liability Partnership</h1>

            </div>
        </div>
    </div>
</div>
</section>


    <!-- Start of banner section
 ============================================= -->
    <section id="it-up-banner" class="it-up-banner-section position-relative">
        <span class="it-up-banner-deco1 position-absolute"> <img src="assets/img/its/b-vector1.png" alt=""></span>
        <span class="it-up-banner-deco2 position-absolute"> <img src="assets/img/its/b-shape1.png" alt=""></span>
        <span class="it-up-banner-deco3 position-absolute"> <img src="assets/img/its/b-shape2.png" alt=""></span>
        <span class="it-up-banner-deco4 position-absolute"> <img src="assets/img/its/b-shape3.png" alt=""></span>
        <div class="container">

            <div class="it-up-banner-content position-relative ">
                <div class="it-up-banner-text headline-1 pera-content it-up-pages-text">

                    <!-- <h1>Limited Liability Partnership</h1> -->
                    {{-- <p>Our vertical solutions expertise allows your business to streamline workflow.</p> --}}
                    <div>
                    <img src="assets/img/its/banner-pic/limited-liability.png" alt="gst registration" class="img-class img-fluid">
                    </div>

                </div>
                <div class="it-up-banner-img my-form">
                    <div class="card bg-light mb-3">
                        <div class="p-3">
                            <h5 class="text-center">LIMITED LIABILITY PARTNERSHIP</h5>
                            <h5 class="text-center">Rs. 3999.00/- All Inclusive</h5>
                        </div>
                    </div>
                    <div class="card bg-light form-bg">
                        <article class="card-body ">

                            <form method="post" action="save_limitedlp">
                                {{csrf_field()}}
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                                    </div>
                                    <input name="name" class="form-control" placeholder="Full name" type="text" required>
                                </div> <!-- form-group// -->
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                                    </div>
                                    <input name="email" class="form-control" placeholder="Email address" type="email" required>
                                </div> <!-- form-group// -->
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                                    </div>

                                    <input name="mobile" class="form-control" placeholder="Phone number" type="text" required>
                                </div>
                                <div class="form-group input-group">
                                    <div class="form-group w-100">
                                        <label for="comment">Type your message: </label>
                                        <textarea class="form-control" rows="" id="comment" name="message" required></textarea>
                                    </div>
                                </div>
                                <div>
                                    <div class="it-up-header-cta-btn  text-center">
                                        <button type="submit" class="new-btn">Submit Now</button>
                                    </div>
                                </div>

                            </form>
                        </article>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- End of banner section
 ============================================= -->
 <!-- PACKAGE START -->
 <div class="demo pt-5 mt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="pricingTable">
                        <div class="pricing-content">
                            <div class="pricingTable-header">
                                <h3 class="title">Basic</h3>
                            </div>
                            <ul>
                                <li>DSC of 2 Partners(Class-3)</li>
                                <li>DIN of 2 Partners</li>
                                <li>Name approval of LLP</li>
                                <li>LLP Agreement</li>
                                <li>Incorporation Certificate</li>
                                <li>LLP PAN</li>
                                <li>LLP TAN</li>                             
                                <li class="disable">GST Registration</li>
                                <li class="disable">MSME Registration</li>
                                <li class="disable">3 Months GST Filing</li>
                                <li class="disable">Dedicated Account Manager</li>
                                <li class="disable">Startup India Registration</li>
                                <li class="disable">1st Year LLP ROC Filing</li>
                            </ul>
                            <div class="price-value">
                                <p><strong> ₹3999.00(All Inclusive)</p></strong>
                                <p><strong>Just pay ₹3999.00 to start</p></strong>
                            </div>
                            <div class="pricingTable-signup">
                                <a href="#">Buy Now</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="pricingTable purple">
                        <div class="pricing-content">
                            <div class="pricingTable-header">
                                <h3 class="title">Advance</h3>
                            </div>
                            <ul>
                            <li>DSC of 2 Partners(Class-3)</li>
                                <li>DIN of 2 Partners</li>
                                <li>Name approval of LLP</li>
                                <li>LLP Agreement</li>
                                <li>Incorporation Certificate</li>
                                <li>LLP PAN</li>
                                <li>LLP TAN</li>                             
                                <li>GST Registration</li>
                                <li>MSME Registration</li>
                                <li>3 Months GST Filing</li>
                                <li>Dedicated Account Manager</li>
                                <li class="disable">Startup India Registration</li>
                                <li class="disable">1st Year LLP ROC Filing</li>
                            </ul>
                            <div class="price-value">
                                <p><strong> ₹5999.00(All Inclusive)</p></strong>
                                <p><strong>Just pay ₹3999.00 to start</p></strong>
                            </div>
                            <div class="pricingTable-signup">
                                <a href="#">Buy Now</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="pricingTable orange">
                        <div class="pricing-content">
                            <div class="pricingTable-header">
                                <h3 class="title">Premium</h3>
                            </div>
                            <ul>
                            <li>DSC of 2 Partners(Class-3)</li>
                                <li>DIN of 2 Partners</li>
                                <li>Name approval of LLP</li>
                                <li>LLP Agreement</li>
                                <li>Incorporation Certificate</li>
                                <li>LLP PAN</li>
                                <li>LLP TAN</li>                             
                                <li>GST Registration</li>
                                <li>MSME Registration</li>
                                <li>3 Months GST Filing</li>
                                <li>Dedicated Account Manager</li>
                                <li>Startup India Registration</li>
                                <li>1st Year LLP ROC Filing</li>
                            </ul>
                            <div class="price-value">
                                <p><strong>₹10999.00(All Inclusive)</p></strong>
                                <p><strong>Just pay ₹7000.00 to start</p></strong>
                            </div>
                            <div class="pricingTable-signup">
                                <a href="#">Buy Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <!-- PACKAGE END -->

    <!-- Start of Featured section
 ============================================= -->
    <section id="it-up-featured" class="it-up-featured-section position-relative">
        <span class="it-up-ft-bg position-absolute"><img src="assets/img/its/ft-bg.png" alt=""></span>
        <!-- <span class="it-up-ft-shape position-absolute"><img src="assets/img/its/ft-shape1.png" alt=""></span> -->
        <span class="it-up-ft-shape2 position-absolute"><img src="assets/img/its/b-shape3.png" alt=""></span>
        <div class="container">
            <div class="it-up-section-title-2 headline-1 text-center">
                {{-- <span>Our Featured Services</span> --}}
                <h2>Limited Liability Partnership</h2>
            </div>
            <div class="it-up-featured-content">
                <div class="row">
                    {{-- <div class="col-md-12">
                        <div class="it-up-featured-innerbox text-center">
                            <div class="it-up-featured-icon">
                                <img src="assets/img/its/icon/ft-icon1.png" alt="">
                            </div>
                            <div class="it-up-featured-text headline-1 pera-content">
                                <h3><a href="#">GST REGISTRATION</a></h3>
                                <p>As per the GST Council, entities in the Northeaster and hill states with an annual turnover of Rs.20 lakhs and above would be required to obtain GST registration..</p>
                                <a class="it-up-ft-more" href="#">Read more <i class="flaticon-right-arrow"></i></a>
                            </div>
                        </div>
                    </div> --}}
                    <div class="col-md-12">
                        <div>
                            <p class="section-p">Limited Liability Partnership Is The Preferred Choice For Businesses Who Want Limited Liability And A Separate Legal Entity</p>
                            <br>
                            <p>Limited Liability Partnership has been introduced in India by way of Limited Liability Partnership Act, 2008. The basic premise behind the introduction of Limited Liability Partnership (LLP) is to provide a form of business organization that is simple to maintain while at the same time providing limited liability to the owners.</P>

<P>A Limited Liability Partnership combines the advantages of both the Company and Partnership into a single form of organization and one partner is not responsible or liable for another partner's misconduct or negligence.</P>

<P>Therefore, all partners have a limited liability, for each individual's protection within the partnership, similar to that of the shareholders of a corporation.</P>

<P>However, unlike corporate shareholders, the partners have the right to manage the business directly. An LLP also limits the personal liability of a partner for the errors, omissions, incompetence, or negligence of the LLP's employees or other agents. LLP is one of the easiest forms of business to incorporate and manage.</P>

<P>LLP has a separate legal entity, liable to the full extent of its assets, the liability of the partners would be limited to their agreed contribution in the LLP. Further, no partner would be liable on account of the independent or un-authorized actions of other partners, thus allowing individual partners to be shielded from joint liability created by another partner's wrongful business decisions or misconduct. </p>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
    <!-- End of Featured section
 ============================================= -->


    <!-- Start of about section
 ============================================= -->
    <section id="it-up-about" class="it-up-about-section">
        <div class="it-up-section-title-2 headline-1 text-center">

            <h2 class="pb-5">Process We Follow</h2>
        </div>
        <div class="timeline">
            <div class="containerr left">
                {{-- <div class="date">1</div> --}}
                <i class="icon fa ">1</i>
                <div class="content">
                    <h2>OBTAINING DSC</h2>
                    <p>
                    Digital Signature Certificates (DSC) are required for the proposed Partners of the LLP. DSC can be obtained for the proposed Partners within 1 to 3 days.
                    </p>
                </div>
            </div>
            <div class="containerr right">
                {{-- <div class="date">2</div> --}}
                <i class="icon fa">2</i>
                <div class="content">
                    <h2>NAME APPROVAL</h2>
                    <p>
                    A minimum of one and a maximum of Two proposed names must be submitted to the MCA. Subject to availability, naming guidelines and MCA processing time, Name Approval can be obtained in 2 to 3 working days.
                    </p>
                </div>
            </div>
            <div class="containerr left">
                {{-- <div class="date">3</div> --}}
                <i class="icon fa ">3</i>
                <div class="content">
                    <h2>INCORPORATION OF LLP</h2>
                    <p>
                    Incorporation documents can be submitted to the MCA along with an application for incorporation. MCA will usually approve the application for incorporation in 3 to 4 days, subject to their processing time.
                    </p>
                </div>
            </div>
            <div class="containerr right">
                {{-- <div class="date"></div> --}}
                <i class="icon fa ">4</i>
                <div class="content">
                    <h2>PAN,TAN & LLP AGREEMENT</h2>
                    <p>
                    Once LLP Incorporation certificate is received,Application for PAN, TAN & LLP Agreement is completed.It takes 2-3 working days.
                    </p>
                </div>
            </div>
            {{-- <div class="containerr left">
              <div class="date">10 Feb</div>
              <i class="icon fa fa-cog"></i>
              <div class="content">
                <h2>Lorem ipsum dolor sit amet</h2>
                <p>
                  Lorem ipsum dolor sit amet elit. Aliquam odio dolor, id luctus erat sagittis non. Ut blandit semper pretium.
                </p>
              </div>
            </div>
            <div class="containerr right">
              <div class="date">01 Jan</div>
              <i class="icon fa fa-certificate"></i>
              <div class="content">
                <h2>Lorem ipsum dolor sit amet</h2>
                <p>
                  Lorem ipsum dolor sit amet elit. Aliquam odio dolor, id luctus erat sagittis non. Ut blandit semper pretium.
                </p>
              </div>
            </div> --}}
        </div>
    </section>
    <!-- End of about section
 ============================================= -->

    <!-- Start of achivement section
 ============================================= -->
    <section id="it-up-achivement" class="it-up-achivement-section position-relative">
        <span class="it-up-achive-shape1 position-absolute"><img src="assets/img/its/achive-bg.png" alt=""></span>
        <span class="it-up-achive-shape2 position-absolute"><img src="assets/img/its/achive-bg2.png" alt=""></span>
        <div class="container">
            <div class="it-up-section-title headline-1 text-center">

                <h2>FAQs</h2>
            </div>
            <div class="it-up-achivement-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="accordion" id="accordionExample">
                            <div class="card m-1">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left" type="button"
                                            data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne">
                                            How many people are required to incorporate a Limited Liability Partnership
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    To incorporate a Limited Liability Partnership, a minimum of two people is required i.e. two partners and can have a maximum of any number of Partners.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseTwo"
                                            aria-expanded="false" aria-controls="collapseTwo">
                                            What are the documents required for incorporation of LLP?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    The documents requirements are as follows : For Partners Pan Card for Indian Nationals. ID proof- Any one (Voter ID/Aadhar Card/Driving License/Passport) Address Proof- Any one (Electricity Bill/Telephone Bill/Mobile Bill/Bank Statement) In addition, the landlord of the registered office premises must provide a No Objection Certificate (NOC) for having the registered office in his premises and must submit his identity proof and address proof.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseThree"
                                            aria-expanded="false" aria-controls="collapseThree">
                                            How long will it take to incorporate a LLP?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    Allindiataxfiling.com can incorporate a Limited Liability Partnership in 14-18 days. The time taken for incorporation will depend on submission of relevant documents by the client and speed of Government Approvals. To ensure speedy incorporation, please choose a unique name for your LLP and ensure you have all the required documents prior to starting the incorporation process.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingFour">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseFour"
                                            aria-expanded="false" aria-controls="collapseFour">
                                            What are the requirements to be a Partner in a LLP?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    The Designated Partners needs to be over 18 years of age and must be a natural person. There are no limitations in terms of citizenship or residency. Therefore, the LLP Act 2008 allows Foreign Nationals including Foreign Companies & LLPs to incorporate a LLP in India provided at least one designated partner is resident of India.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingFive">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseFive"
                                            aria-expanded="false" aria-controls="collapseFive">
                                            Do I have to be present in person to incorporate a LLP?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFive" class="collapse" aria-labelledby="headingFive"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    No, you will not have to be present at our office or appear at any office for the incorporation of a Limited Liability Partnership. All the documents can be scanned and sent through email to our office. Some documents will also have to be couriered to our office.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingSix">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseSix"
                                            aria-expanded="false" aria-controls="collapseSix">
                                            Is an office required to starting a Limited Liability Partnership?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseSix" class="collapse" aria-labelledby="headingSix"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    You can open a LLP on your residential address, there is no requirement to have a commercial place to open up a LLP. For Registered Office Address Rent agreement along with latest rent receipt (in case the premises are rented) House tax receipts (in case premises are owned) Electricity bill NOC from the Owner
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingSeven">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseSeven"
                                            aria-expanded="false" aria-controls="collapseSeven">
                                            Does LLP has a separate legal entity?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    Yes, LLP has a separate legal entity, liable to the full extent of its assets, the liability of the partners would be limited to their agreed contribution in the LLP. Further, no partner would be liable on account of the independent or un-authorized actions of other partners, thus allowing individual partners to be shielded from joint liability created by another partner’s wrongful business decisions or misconduct.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingEight">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseEight"
                                            aria-expanded="false" aria-controls="collapseEight">
                                            What is the capital required to start a Limited Liability Partnership?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseEight" class="collapse" aria-labelledby="headingEight"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    You can start a Limited Liability Partnership with any amount of capital. There is no requirement to show proof of capital invested during the incorporation process. Partner’s contribution may consist of both tangible and/or intangible property.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingNine">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseNine"
                                            aria-expanded="false" aria-controls="collapseNine">
                                            What is a Digital Signature Certificate?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseNine" class="collapse" aria-labelledby="headingNine"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    A Digital Signature establishes the identity of the sender or signee electronically while filing documents through the Internet. The Ministry of Corporate Affairs (MCA) mandates that all Designated Partners apply with a Digital Signature for Designated Partner Identification Number (DPIN). Hence, a Digital Signature is required for all Designated Partner of a proposed LLP
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingTen">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseTen"
                                            aria-expanded="false" aria-controls="collapseTen">
                                            What is Designated Partner Identification Number (DPIN)?

                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTen" class="collapse" aria-labelledby="headingTen"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    Designated Partner Identification Number is a unique identification number assigned to all existing and proposed Designated Partner of a LLP. It is mandatory for all present or proposed Designated Partners to have a Designated Partner Identification Number (DPIN). Designated Partner Identification Number never expires and a person can have only one DPIN.

                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingEleven">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseEleven"
                                            aria-expanded="false" aria-controls="collapseEleven">
                                            What is LLP Identification Number (LLPIN)?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    What is LLP Identification Number (LLPIN)?
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingTwelve">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseTwelve"
                                            aria-expanded="false" aria-controls="collapseTwelve">
                                            How long is the incorporation of the LLP valid for?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwelve" class="collapse" aria-labelledby="headingTwelve"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    Once a Limited Liability Partnership is incorporated, it will be active and in-existence as long as the annual compliances are met with regularly. In case, annual compliances are not complied with, the LLP will become a Dormant and maybe struck off from the register after a period of time. A struck-off Company can be revived for a period of upto 20 years.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingThirteen">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseThirteen"
                                            aria-expanded="false" aria-controls="collapseThirteen">
                                            Can NRIs / Foreign Nationals be a Designated Partners in a LLP?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseThirteen" class="collapse" aria-labelledby="headingThirteen"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    Yes, a NRI or Foreign National can be a Designated Partner in a Limited Liability Partnership after obtaining Designated Partner Identification Number (DPIN). However, atleast one Designated Partner in the LLP must be a Resident India.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingFourteen">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseFourteen"
                                            aria-expanded="false" aria-controls="collapseFourteen">
                                            Can a Salaried person become a Partner in LLP?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFourteen" class="collapse" aria-labelledby="headingFourteen"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    Yes, a salaried person becomes a Partner, there are no legal bondages in this but you may have to go through with your employment agreement if it contains any restrictions on doing so.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingFifteen">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseFifteen"
                                            aria-expanded="false" aria-controls="collapseFifteen">
                                            What is the nature & extent of liability of a partner of an LLP?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFifteen" class="collapse" aria-labelledby="headingFifteen"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    Every partner of an LLP would be, for the purpose of the business of the LLP, an agent of the LLP but not of the other partners. Liability of partners shall be limited except in case of unauthorized acts, fraud and negligence. But a partner shall not be personally liable for the wrongful acts or omission of any other partner. An obligation of the limited liability partnership whether arising in contract or otherwise, is solely the obligation of the limited liability partnership. The liabilities of LLP shall be met out of the property of the LLP.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingsixteen">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapsesixteen"
                                            aria-expanded="false" aria-controls="collapsesixteen">
                                            What are the annual compliance requirements for a LLP?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapsesixteen" class="collapse" aria-labelledby="headingsixteen"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    LLPs are required to file an annual filing with the Registrar each year. However, if the LLP has a turnover of less than Rs.40 lakhs and/or has a capital contribution of less than Rs.25 lakhs, the financial statements do not have to be audited.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingseventeen">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseseventeen"
                                            aria-expanded="false" aria-controls="collapseseventeen">
                                            Can an LLP converted into Private Limited Company or a Public Limited Company?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseseventeen" class="collapse" aria-labelledby="headingseventeen"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    No, right now act does not permit this conversion. An LLP can’t be converted into Private or Public limited but a Private or Public Limited Company can be converted into LLP.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingeighteen">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseeighteen"
                                            aria-expanded="false" aria-controls="collapseeighteen">
                                            Can an existing partnership firm be converted to LLP?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseeighteen" class="collapse" aria-labelledby="headingeighteen"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    Yes, an existing partnership firm can be converted into LLP by complying with the Provisions of clause 58 and Schedule II of the LLP Act. Form 17 needs to be filed along with Form 2 for such conversion and incorporation of LLP.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingnineteen">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapsenineteen"
                                            aria-expanded="false" aria-controls="collapsenineteen">
                                            Is LLP a better business type to raise funds from Private investors?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapsenineteen" class="collapse" aria-labelledby="headingnineteen"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    No LLP is not a good instrument to raise funds from Private investors. Investors invest in a company in lieu of the equity or stake but in LLP the investors do not get to hold stake.
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="it-up-achivement" class="it-up-achivement-section position-relative it-sec">
        <span class="it-up-achive-shape1 position-absolute"><img src="assets/img/its/achive-bg.png" alt=""></span>
        <span class="it-up-achive-shape2 position-absolute"><img src="assets/img/its/achive-bg2.png" alt=""></span>
        <div class="container">
            <div class="it-up-section-title headline-1 text-center">

                <h2>WHY AITF</h2>
            </div>
            <div class="it-up-achivement-content">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/taxes.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">24/7 SUPPORT</h5>
                                {{-- <p>Satisfied Clients</p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/idea.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">EXPERT ADVICE</h5>
                                {{-- <p>Team Member</p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/cyber-security.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">SECURITY</h5>
                                {{-- <p>Award Winner</p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/pie-chart.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">FILE IT RIGHT</h5>
                                {{-- <p>Works Done</p> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="it-up-service" class="it-up-service-section position-relative">
        <span class="it-up-service-shape position-absolute deco1"><img src="assets/img/its/s-shape1.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco2"><img src="assets/img/its/s-shape2.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco3"><img src="assets/img/its/s-shape3.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco4"><img src="assets/img/its/s-shape4.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco5"><img src="assets/img/its/s-shape5.png" alt=""></span>
        <div class="container">
            {{-- <div class="it-up-section-title headline-1 text-center">
               
                <h2>We are happy to share our client’s review.</h2>
            </div> --}}
            <div class="it-up-service-content">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="it-up-service-tab-btn">
                            <ul id="tabs" class="nav text-capitalize nav-tabs">
                                <li class="nav-item"><a href="#" data-target="#marketing" data-toggle="tab"
                                        class="nav-link text-capitalize active">WE ARE DIFFERENT!</a></li>
                                <li class="nav-item"><a href="#" data-target="#designing" data-toggle="tab"
                                        class="nav-link text-capitalize">WHY CHOOSE</a></li>
                                <li class="nav-item"><a href="#" data-target="#development" data-toggle="tab"
                                        class="nav-link text-capitalize">SERVICES OFFERING !</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="it-up-service-tab-content">
                            <div id="tabsContent" class="tab-content">
                                <div id="marketing" class="tab-pane fade active show">
                                    <div class="it-up-service-tab-wrap clearfix">
                                        <div
                                            class="it-up-service-tab-text position-relative float-left ul-li-block headline-1">
                                            <h3>WE ARE DIFFERENT!</h3>
                                            <h5>At Allindiataxfiling.com we ensure complete transparency at every step
                                                of the business incorporation process.We specialise in excellent
                                                customer service. When you call, email with our offices, you will
                                                receive personal attention from a knowledgeable specialist happy to help
                                                you .Our team of business startup specialists are here to keep you
                                                updated on the latest incorporation, tax and financial strategies and to
                                                help you manage important business details.

                                            </h5>
                                            {{-- <a class="it-up-ser-btn" href="#">Check Details <i class="flaticon-right-arrow"></i></a>
                                            <span class="it-up-tab-icon position-absolute"><img src="assets/img/its/icon/tab-icon1.png" alt=""></span> --}}
                                        </div>
                                        <div class="it-up-service-img float-right">
                                            <img src="assets/img/its/ser-tab1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div id="designing" class="tab-pane fade">
                                    <div class="it-up-service-tab-wrap clearfix">
                                        <div
                                            class="it-up-service-tab-text position-relative float-left ul-li-block headline-1">
                                            <h3>WHY CHOOSE</h3>
                                            <ul>
                                                <li>Specialized knowledge and experience in Taxation & Business
                                                    Registration.</li>
                                                <li>More communication than what larger agencies provide.</li>
                                                <li>Strategic partnership and advice for potential opportunities to grow
                                                    your brand.</li>
                                                <li>Work is done in-house and not outsourced to a third party.</li>
                                                <li>We practice what we preach and use our own services to grow our
                                                    business.</li>
                                            </ul>
                                            {{-- <a class="it-up-ser-btn" href="#">Check Details <i
                                                    class="flaticon-right-arrow"></i></a>
                                            <span class="it-up-tab-icon position-absolute"><img
                                                    src="assets/img/its/icon/tab-icon1.png" alt=""></span> --}}
                                        </div>
                                        <div class="it-up-service-img float-right">
                                            <img src="assets/img/its/ser-tab1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div id="development" class="tab-pane fade">
                                    <div class="it-up-service-tab-wrap clearfix">
                                        <div
                                            class="it-up-service-tab-text position-relative float-left ul-li-block headline-1">
                                            <h3>SERVICES OFFERING !</h3>
                                            <h5>Allindiataxfiling.com provides fast and end-to-end Business
                                                Incorporation and associated services. We also provide Accounting,
                                                Taxation and Legal Services. We guarantee hassle-free service delivery
                                                in the shortest possible time without compromising on quality.</h5>
                                            <h5>Our team of experienced professionals assists entrepreneurs in selecting
                                                the best business structure suiting individual requirements, and to
                                                convert their dream business into appropriate corporate entities.</h5>
                                            {{-- <a class="it-up-ser-btn" href="#">Check Details <i
                                                    class="flaticon-right-arrow"></i></a>
                                            <span class="it-up-tab-icon position-absolute"><img
                                                    src="assets/img/its/icon/tab-icon1.png" alt=""></span> --}}
                                        </div>
                                        <div class="it-up-service-img float-right">
                                            <img src="assets/img/its/ser-tab1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-3">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr">
                        <div class="counter-icon">
                            <i class="fas fa-medal"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Years</h3>
                            <span class="counter-value">10</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr green">
                        <div class="counter-icon">
                            <i class="fas fa-briefcase"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Projects</h3>
                            <span class="counter-value">947</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr blue">
                        <div class="counter-icon">
                            <i class="fas fa-user-tie"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Clients</h3>
                            <span class="counter-value">867</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr purple">
                        <div class="counter-icon">
                            <i class="fas fa-map-marker-alt"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Branches</h3>
                            <span class="counter-value">4</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </section>
    <section class="pt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 pt-5 pb-5">
                    <div class="it-up-section-title headline-1 text-center">
                        <h2>OUR PARTNERS</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="it-up-sponsor" class="it-up-sponsor-section">
                        <div class="container">
                            <div id="it-up-sponsor-slide" class="it-up-sponsor-slider owl-carousel">
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/gluehost.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/spellbound.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/aa.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/db.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/daxy.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img pt-4">
                                    <img src="assets/img/its/shrex.png" alt="">
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="it-up-testimonial" class="it-up-testimonial-section">
        <div class="container">
            <div class="it-up-section-title-2 headline-1 text-center">
                <span>Our Client’s Feedback</span>
                <h2>We are happy to share our client’s review.</h2>
            </div>
            <div class="it-up-testimonial-content position-relative">
                <span class="it-up-testi-shape1 position-absolute"><img src="assets/img/its/b-shape3.png" alt=""></span>
                <span class="it-up-testi-shape2 position-absolute" data-parallax='{"y" : 70}'><img src="assets/img/its/tst-sh1.png" alt=""></span>
                <span class="it-up-testi-shape3 position-absolute" data-parallax='{"x" : -70}'><img src="assets/img/its/tst-sh-1.png" alt=""></span>
                <span class="it-up-testi-shape4 position-absolute"><img src="assets/img/its/tst-sh3.png" alt=""></span>
                <div class="it-up-testimonial-slider-wrap owl-carousel">
                    <div class="it-up-testimonial-innerbox it-up-testimonial-green text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst2.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p>I really like the services provided by AITF for its admirable service, instructive and precise. 
AITF always accommodating and ready to support for any query. I do recommend AITF.
</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Abhishek Raj Rathod</a></h4>
                                <span>- Founder- Aximo infotech Pvt Ltd</span>
                            </div>
                        </div>
                    </div>
                    <div class="it-up-testimonial-innerbox it-up-testimonial-pink text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst1.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p>Everything from incorporation of your company to the trademark and all filing they coverd it all. one of the best CA consultant services they provide.</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Pranav Dhakulkar</a></h4>
                                <span>- Founder- Dryomix Concrete Solutions Pvt Ltd</span>
                            </div>
                        </div>
                    </div>
                    <div class="it-up-testimonial-innerbox it-up-testimonial-green text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst2.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p>In single sentence "AITF " is one stop solution for services Like Company Incorporation, Trademark Registration or any work related to CA. Cost & services are very good.</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Bestayur Lifecare</a></h4>
                                <span>- Bestayur Lifecare</span>
                            </div>
                        </div>
                    </div>
                    <div class="it-up-testimonial-innerbox it-up-testimonial-blue text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst3.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p>Professional services at competitive price. They give us complete end to end support from incorporation to expansion. Highly recommended.

                                Keep it up.</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Abhishek kumar</a></h4>
                                <span>-  Founder-Koibhikaamcube</span>
                            </div>
                        </div>
                    </div>
                   
                    
                </div>
            </div>
        </div>
    </section>
    @include('components/footer')
    @include('components/foot-link')
    <script>
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.display === "block") {
                    panel.style.display = "none";
                } else {
                    panel.style.display = "block";
                }
            });
        }
    </script>

</body>

</html>

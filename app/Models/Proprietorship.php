<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proprietorship extends Model
{
    protected $table = 'proprietorship';
    use HasFactory;
}

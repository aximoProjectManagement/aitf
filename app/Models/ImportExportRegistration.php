<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImportExportRegistration extends Model
{
    protected $table = 'import_export_code';
    use HasFactory;
}

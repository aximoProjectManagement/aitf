<?php

namespace App\Http\Controllers\Webpages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GstRegistration;
use App\Models\ImportExportRegistration;
use App\Models\TrademarkRegistration;
use App\Models\GemRegistration;
use App\Models\StartupIndiaRegistration;
use App\Models\MsmeRegistration;

class RegistrationController extends Controller
{
    public function index(){
        return view('webpages.registration');
    }
    public function GstRegistration(){
        return view('webpages.gst-registration');
    }
    public function ImportExport(){
        return view('webpages.import-export-code');
    }
    public function TrademarkRegistration(){
        return view('webpages.trademark-registration');
    }
    public function GEMRegistration(){
        return view('webpages.gem-registration');
    }
    public function StartupIndiaRegistration(){
        return view('webpages.startup-india-reg');
    }
    public function MsmeRegistration(){
        return view('webpages.msme-regis');
    }
    public function saveGstRegistration(Request $request){
        $data = new GstRegistration;
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->mobile = $request->input('mobile');
        $data->message = $request->input('message');
        $data->save();
        return redirect('/gst_regis');
    }
    public function saveImportExportRegistration(Request $request){
        $data = new ImportExportRegistration;
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->mobile = $request->input('mobile');
        $data->message = $request->input('message');
        $data->save();
        return redirect('/import_export');
    }
    public function saveTrademarkRegistration(Request $request){
        $data = new TrademarkRegistration;
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->mobile = $request->input('mobile');
        $data->message = $request->input('message');
        $data->save();
        return redirect('/trademark_registration');
    }
    public function saveGemRegistration(Request $request){
        $data = new GemRegistration;
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->mobile = $request->input('mobile');
        $data->message = $request->input('message');
        $data->save();
        return redirect('/gem_registration');
    }
    public function saveStartupIndiaRegsitration(Request $request){
        $data = new StartupIndiaRegistration;
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->mobile = $request->input('mobile');
        $data->message = $request->input('message');
        $data->save();
        return redirect('/startup_india');
    }
    public function saveMsmeRegsitration(Request $request){
        $data = new MsmeRegistration;
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->mobile = $request->input('mobile');
        $data->message = $request->input('message');
        $data->save();
        return redirect('/msme_registration');
    }
}

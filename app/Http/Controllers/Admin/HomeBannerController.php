<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\HomeBanner;

class HomeBannerController extends Controller
{
    public function homeBanner(Request $request){
        if($request->hasFile('image')){
        $imageName = time().'.'.$request->image->extension();
        $request->image->move(public_path('assets/banner'), $imageName);
        
        $data = new HomeBanner;
        $data->image = $imageName;
        $data->save();
        return redirect('/admin/home_banner');
    }
    }
}
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="dashboard">
        {{-- <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-laugh-wink"></i>
    </div> --}}
        <div class="sidebar-brand-text mx-3">All India Tax Filing</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="dashboard">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->

    <!-- Heading -->


    <!-- Nav Item - Pages Collapse Menu -->
    <!-- <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
    aria-expanded="true" aria-controls="collapseTwo">
    <i class="fas fa-fw fa-cog"></i>
    <span>Components</span>
</a>
<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Components:</h6>
            <a class="collapse-item" href="buttons.php">Buttons</a>
            <a class="collapse-item" href="cards.php">Cards</a>
        </div>
    </div>
</li> -->

    <!-- Nav Item - Utilities Collapse Menu -->
    <!-- <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
        aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-fw fa-wrench"></i>
        <span>Utilities</span>
    </a>
    <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
        data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Utilities:</h6>
            <a class="collapse-item" href="utilities-color.php">Colors</a>
            <a class="collapse-item" href="utilities-border.php">Borders</a>
            <a class="collapse-item" href="utilities-animation.php">Animations</a>
            <a class="collapse-item" href="utilities-other.php">Other</a>
        </div>
    </div>
</li> -->

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <!-- <div class="sidebar-heading">
    Addons
</div> -->

    <!-- Nav Item - Pages Collapse Menu -->
    <!-- <li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
        aria-expanded="true" aria-controls="collapsePages">
        <i class="fas fa-fw fa-folder"></i>
        <span>Pages</span>
    </a>
    <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Login Screens:</h6>
            <a class="collapse-item" href="login.php">Login</a>
            <a class="collapse-item" href="register.php">Register</a>
            <a class="collapse-item" href="forgot-password.php">Forgot Password</a>
            <div class="collapse-divider"></div>
            <h6 class="collapse-header">Other Pages:</h6>
            <a class="collapse-item" href="404.php">404 Page</a>
            <a class="collapse-item" href="blank.php">Blank Page</a>
        </div>
    </div>
</li> -->
    <li class="nav-item ">
        <a class="nav-link" href="home_banner">
            <i class="far fa-images"></i>
            <span>Home Banner</span></a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="service_dashboard">
            <i class="fas fa-concierge-bell"></i>
            <span>Services</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="who_we_are">
            <i class="fas fa-user-check"></i>
            <span>Who We Are</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="partners">
            <i class="fas fa-users"></i>
            <span>Partners</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="client-review">
            <i class="fas fa-user-plus"></i>
            <span>Clients</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="packages">
            <i class="fas fa-table"></i>
            <span>Packages</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="blogs">
            <i class="fab fa-blogger"></i>
            <span>Blogs</span></a>
    </li>



    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages-1"
            aria-expanded="true" aria-controls="collapsePages=-1">
            <i class="fas fa-fw fa-folder"></i>
            <span>Starts Up</span>
        </a>
        <div id="collapsePages-1" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">

                <a class="collapse-item" href="proprietorship">Proprietorship</a>
                <a class="collapse-item" href="partnership">Partnership</a>
                <a class="collapse-item" href="onepcompany">One person company</a>


                <a class="collapse-item" href="privatelimited">Private limited company</a>
                <a class="collapse-item" href="limitedliability">Limited liability company</a>
                <a class="collapse-item" href="publiclimited">Public limited company</a>
                <a class="collapse-item" href="section_8">Section 8 company(NGO)</a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true"
            aria-controls="collapsePages">
            <i class="fas fa-fw fa-folder"></i>
            <span>Filing</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">

                <a class="collapse-item" href="income_tax_return">Income Tax Return</a>
                <a class="collapse-item" href="tds_return">TDS Return</a>
                <a class="collapse-item" href="company_annual_filing">Company Annual Filing</a>


                <a class="collapse-item" href="llp_annual_filing">LLP Annual Filing</a>
                <a class="collapse-item" href="digital_signature">Digital Signature Certificate</a>

            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages-2"
            aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-folder"></i>
            <span>Registration</span>
        </a>
        <div id="collapsePages-2" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">

                <a class="collapse-item" href="gst_reg">GST Registration </a>
                <a class="collapse-item" href="import_export">Import Export Code </a>
                <a class="collapse-item" href="trademark_regis">Trademark Registration</a>


                <a class="collapse-item" href="gem_reg">GEM Registration</a>
                <a class="collapse-item" href="startup_india_reg">Startup India Registration</a>
                <a class="collapse-item" href="msme_reg">MSME Registration</a>

            </div>
        </div>
    </li>


    <!-- Nav Item - Charts -->
    <!-- <li class="nav-item">
    <a class="nav-link" href="charts.php">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Charts</span></a>
</li> -->

    <!-- Nav Item - Tables -->
    {{-- <li class="nav-item">
        <a class="nav-link" href="tables">
            <i class="fas fa-fw fa-table"></i>
            <span>Tables</span></a>
    </li> --}}

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <!-- <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div> -->

    <!-- Sidebar Message -->
    <!-- <div class="sidebar-card d-none d-lg-flex">
    <img class="sidebar-card-illustration mb-2" src="img/undraw_rocket.svg" alt="...">
    <p class="text-center mb-2"><strong>SB Admin Pro</strong> is packed with premium features, components, and more!</p>
    <a class="btn btn-success btn-sm" href="https://startbootstrap.com/theme/sb-admin-pro">Upgrade to Pro!</a>
</div> -->

</ul>

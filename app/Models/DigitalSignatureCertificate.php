<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DigitalSignatureCertificate extends Model
{
    protected $table = 'digital_signature_certificate';
    use HasFactory;
}

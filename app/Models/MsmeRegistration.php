<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MsmeRegistration extends Model
{
    protected $table = 'msme_registration';
    use HasFactory;
}

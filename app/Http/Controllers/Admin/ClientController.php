<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Clients;

class ClientController extends Controller
{
    public function clients(Request $request){
        if($request->hasFile('image')){
        $imageName = time().'.'.$request->image->extension();
        $request->image->move(public_path('assets/clients'), $imageName);
        
        $data = new Clients;
        $data->description = $request->input('description');
        $data->name = $request->input('name');
        $data->designation = $request->input('designation');
        $data->image = $imageName;
        $data->save();
        return redirect('/admin/client-review');
    }
    }

}

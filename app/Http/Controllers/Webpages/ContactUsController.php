<?php

namespace App\Http\Controllers\Webpages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function index(){
        return view('webpages.contact-us');
    }
}

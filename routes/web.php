<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Webpages\UsersController;
use App\Http\Controllers\Webpages\StartupController;
use App\Http\Controllers\Webpages\FilingController;
use App\Http\Controllers\Webpages\RegistrationController;
use App\Http\Controllers\Webpages\ContactUsController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\HomeBannerController;
use App\Http\Controllers\Admin\WhoWeAreBannerController;
use App\Http\Controllers\Admin\PartnersBannerController;
use App\Http\Controllers\Admin\ClientController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\ServiceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [UsersController::class,'index']);

Route::get('gst_regis', [RegistrationController::class,'GstRegistration']);
Route::get('about_us',[UsersController::class,'about']);
Route::get('start_ups',[StartupController::class,'index']);
Route::get('contact_us',[ContactUsController::class,'index']);

Route::get('filing',[FilingController::class,'index']);
Route::get('registration', [RegistrationController::class,'index']);
Route::get('confidentiality', [UsersController::class,'confidentiality']);
Route::get('disclaimer',[UsersController::class,'disclaimer']);
Route::get('privacy_policy',[UsersController::class,'privacypolicy']);
Route::get('refund_policy', [UsersController::class,'refundpolicy']);
Route::get('import_export', [RegistrationController::class,'ImportExport']);
Route::get('trademark_registration',[RegistrationController::class,'TrademarkRegistration']);
Route::get('gem_registration', [RegistrationController::class,'GEMRegistration']);
Route::get('startup_india',[RegistrationController::class,'StartupIndiaRegistration']);
Route::get('msme_registration', [RegistrationController::class,'MsmeRegistration']);
Route::get('income_tax_return',[FilingController::class,'IncomeTaxreturn']);
Route::get('tds_return', [FilingController::class,'TdsReturn']);
Route::get('company_annual_filling',[FilingController::class,'CompanyAnnualFiling']);
Route::get('llp_annual_filing', [FilingController::class,'LlpAnnualFiling']);
Route::get('digital_signature', [FilingController::class,'DigitalSignature']);
Route::get('propritership', [StartupController::class,'Propritership']);
Route::get('partnership',[StartupController::class,'Partnership']);
Route::get('one_person_company',[StartupController::class,'OnePersonCompany']);
Route::get('private_limited_company', [StartupController::class,'PrivateLimitedCompany']);
Route::get('llp', [StartupController::class,'LLP']);
Route::get('public_limited_company', [StartupController::class,'PublicLimitedCompany']);
Route::get('section_8_company', [StartupController::class,'Section_8']);
Route::post('save_prop',[StartupController::class,'saveProprietorship']);
Route::post('save_part',[StartupController::class,'savePartnership']);
Route::post('save_onep',[StartupController::class,'saveOnePersonCompany']);
Route::post('save_privatelc',[StartupController::class,'savePrivateLimitedCompany']);
Route::post('save_limitedlp',[StartupController::class,'saveLimitedLiabilityPartnership']);
Route::post('save_publiclc',[StartupController::class,'savePublicLimitedCompany']);
Route::post('save_section8',[StartupController::class,'saveSection8Company']);
Route::post('save_incometaxret',[FilingController::class,'saveIncomeTaxReturn']);
Route::post('save_tds',[FilingController::class,'saveTdsReturn']);
Route::post('save_companyaf',[FilingController::class,'saveCompanyAnnualFiling']);
Route::post('save_llpannual',[FilingController::class,'saveLlpAnnualFiling']);
Route::post('save_digitalsignature',[FilingController::class,'saveDigitalSignatureCertificate']);
Route::post('save_gstreg',[RegistrationController::class,'saveGstRegistration']);
Route::post('save_impex',[RegistrationController::class,'saveImportExportRegistration']);
Route::post('save_trademark',[RegistrationController::class,'saveTrademarkRegistration']);
Route::post('save_gemreg',[RegistrationController::class,'saveGemRegistration']);
Route::post('save_startupindia',[RegistrationController::class,'saveStartupIndiaRegsitration']);
Route::post('save_msmereg',[RegistrationController::class,'saveMsmeRegsitration']);
Route::post('save_contactus',[UsersController::class,'saveContactUs']);
Route::post('save_contactusindex',[UsersController::class,'saveContactUsIndex']);

Route::group(['middleware'=> 'authuser','prefix' => 'admin'],function(){
    Route::get('/',[AdminController::class,'index']);
    Route::get('dashboard',[AdminController::class,'dashboard']);
    // Route::get('services',[AdminController::class,'services']);
    Route::get('proprietorship',[AdminController::class,'proprietorship']);
    Route::get('partnership',[AdminController::class,'partnership']);
    Route::get('onepcompany',[AdminController::class,'onepcompanyy']);
    Route::get('privatelimited',[AdminController::class,'PrivateLimited']);
    Route::get('limitedliability',[AdminController::class,'LimitedLiability']);
    Route::get('publiclimited',[AdminController::class,'PublicLimited']);
    Route::get('section_8',[AdminController::class,'Section_8']);
    Route::get('income_tax_return',[AdminController::class,'IncomeTaxReturn']);
    Route::get('tds_return',[AdminController::class,'TdsReturn']);
    Route::get('company_annual_filing',[AdminController::class,'CompanyAnnualFiling']);
    Route::get('llp_annual_filing',[AdminController::class,'LlpAnnualFiling']);
    Route::get('digital_signature',[AdminController::class,'DigitalSignatureCertificate']);
    Route::get('gst_reg',[AdminController::class,'GstRegistration']);
    Route::get('import_export',[AdminController::class,'ImportExport']);
    Route::get('trademark_regis',[AdminController::class,'TrademarkRegistration']);
    Route::get('gem_reg',[AdminController::class,'GemRegistration']);
    Route::get('startup_india_reg',[AdminController::class,'StartupIndiaRegistration']);
    Route::get('msme_reg',[AdminController::class,'MsmeRegistration']);
    Route::get('my_profile',[AdminController::class,'MyProfile']);
    Route::get('home_banner',[AdminController::class,'HomeBanner']);
    Route::get('service_dashboard',[AdminController::class,'ServiceDashboard']);
    Route::get('service_dashboard/edit/{id}',[AdminController::class,'service_dashboard_edit']);
    Route::get('service_dashboard/delete/{id}',[AdminController::class,'ServiceDashboard_delete']);
    Route::post('service_dashboard/update/{id}',[AdminController::class,'ServiceDashboard_update']);
    Route::get('who_we_are',[AdminController::class,'WhoWeAre']);
    Route::get('partners',[AdminController::class,'Partner']);
    Route::get('client-review',[AdminController::class,'Client']);
    Route::get('packages',[AdminController::class,'packages']);
    Route::get('blogs',[AdminController::class,'Blogs']);
    Route::post('home_banner_upload',[HomeBannerController::class,'homeBanner']);
    Route::post('who_we_are_upload',[WhoWeAreBannerController::class,'whoweareBanner']);
    Route::post('partners_upload',[PartnersBannerController::class,'partnersBanner']);
    Route::post('clients_upload',[ClientController::class,'clients']);
    Route::post('blog',[BlogController::class,'blog']);
    Route::post('service_data',[AdminController::class,'ServiceDashboard']);
    Route::post('service_data',[ServiceController::class,'service']);

    // Route::get('/',[AdminController::class,'index']);


});

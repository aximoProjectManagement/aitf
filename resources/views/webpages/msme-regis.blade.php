<!DOCTYPE html>
<html lang="en">

<head>
    <title>MSME Registration</title>
    @include('components/head-link')
</head>
@include('components/header')

<body class="s-it">
    {{-- <div id="preloader"></div> --}}
    <div class="up">
        <a href="#" class="scrollup text-center"><i class="fas fa-chevron-up"></i></a>
    </div>
    <section class="pb-5">

     <div class=" page-banner">
         <span class=""></span>
         <div class="row">
             <div class="col-md-12">
                 <div>
                 <ul class="my-breadcrump">
                    <li class="home-bread"><a href="index">Home</a></li>
                     <li class=""><i class="fas fa-chevron-right my-arrow"><a class="home-bread-1" href="registration">Registration</a></i></li>
                     <li class=""><i class="fas fa-chevron-right my-arrow"><a class="home-bread-1" href="#">MSME Registration</a></i></li>
  
                 </ul>

                <h1 class="page-banner-heading">MSME Registration</h1>

                 </div>
             </div>
         </div>
     </div>
 </section>


    <!-- Start of banner section
 ============================================= -->
    <section id="it-up-banner" class="it-up-banner-section position-relative">
        <span class="it-up-banner-deco1 position-absolute"> <img src="assets/img/its/b-vector1.png" alt=""></span>
        <span class="it-up-banner-deco2 position-absolute"> <img src="assets/img/its/b-shape1.png" alt=""></span>
        <span class="it-up-banner-deco3 position-absolute"> <img src="assets/img/its/b-shape2.png" alt=""></span>
        <span class="it-up-banner-deco4 position-absolute"> <img src="assets/img/its/b-shape3.png" alt=""></span>
        <div class="container">

            <div class="it-up-banner-content position-relative ">
                <div class="it-up-banner-text headline-1 pera-content it-up-pages-text">

                    <!-- <h1>MSME Registration</h1> -->

                    <div>
                    <img src="assets/img/its/banner-pic/msme-regis.png" alt="gst registration" class="img-class img-fluid">
                    </div>

                </div>
                <div class="it-up-banner-img my-form">
                    <div class="card bg-light mb-3">
                        <div class="p-3">
                            <h5 class="text-center">MSME Registration</h5>
                            <h5 class="text-center">Rs. 1500.00/- All Inclusive</h5>
                        </div>
                    </div>
                    <div class="card bg-light form-bg">
                        <article class="card-body ">

                            <form method="post" action="save_msmereg">
                                {{csrf_field()}}
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                                    </div>
                                    <input name="name" class="form-control" placeholder="Full name" type="text" required>
                                </div> <!-- form-group// -->
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                                    </div>
                                    <input name="email" class="form-control" placeholder="Email address" type="email" required>
                                </div> <!-- form-group// -->
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                                    </div>

                                    <input name="mobile" class="form-control" placeholder="Phone number" type="text" required>
                                </div>
                                <div class="form-group input-group">
                                    <div class="form-group w-100">
                                        <label for="comment">Type your message: </label>
                                        <textarea class="form-control" rows="" id="comment" name="message" required></textarea>
                                    </div>
                                </div>
                                <div>
                                    <div class="it-up-header-cta-btn  text-center">
                                        <button type="submit" class="new-btn">Submit Now</button>
                                    </div>
                                </div>

                            </form>
                        </article>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- End of banner section
 ============================================= -->

    <!-- Start of Featured section
 ============================================= -->
    <section id="it-up-featured" class="it-up-featured-section position-relative">
        <span class="it-up-ft-bg position-absolute"><img src="assets/img/its/ft-bg.png" alt=""></span>
        {{-- <span class="it-up-ft-shape position-absolute"><img src="assets/img/its/ft-shape1.png" alt=""></span> --}}
        <span class="it-up-ft-shape2 position-absolute"><img src="assets/img/its/b-shape3.png" alt=""></span>
        <div class="container">
            <div class="it-up-section-title-2 headline-1 text-center mw-100">

                <h2>MSME REGISTRATION</h2>
            </div>
            <div class="it-up-featured-content">
                <div class="row">

                    <div class="col-md-12">
                        <div>
                            <p class="section-p">Micro-Small & Medium Enterprises (MSMEs) contribute largely in the
                                socio-economic development of our country. MSMEs come under the Ministry of Micro, Small
                                & Medium Enterprises and are required to be registered to avail benefits under the
                                Micro, Small and Medium Enterprises Development (MSMED) Act. MSME registration is highly
                                important, as it enables these enterprises in availing benefits from various government
                                initiated schemes. MSMEs are classified in two categories, such as manufacturing and
                                service enterprises.

                            </p>
                            <h5><strong>Benefits of MSME Registration</strong></h5>
                            <ul>
                                <li>MSME registration helps in getting government tenders</li>
                                <li>Under bank loan, 15% import subsidy on fully automatic machinery</li>
                                <li>Becomes easy to get licenses, approvals and registrations, irrespective of field of
                                    business</li>
                                <li>Compensation of ISO certificate expenditure</li>
                                <li>Helps in getting low interest rates</li>
                                <li>Registered MSMEs gets tariff subsidies and tax and capital subsidies</li>
                            </ul>
                            <p>The classification of MSME for both goods and services organisation is done on the basis
                                of the turnover and is as follows:-</p>
                            <div>
                                <table class="table table-bordered">

                                    <tbody>
                                        <tr>
                                            <td><strong>Classification</strong></td>
                                            <td><strong>Turnover</strong></td>

                                        </tr>
                                        <tr>

                                            <td>Micro Enterprise</td>
                                            <td>Upto Rs. 5 Crores & Investment Upto Rs 1 Crores.</td>
                                        </tr>
                                        <tr>
                                            <td>Small Enterprise</td>

                                            <td>Rs. 5 Crores to Rs. 50 Crores & Investment Upto Rs 10 Crores.</td>
                                        </tr>
                                        <tr>
                                            <td>Medium Enterprise</td>
                                            <td>Rs. 50 Crores to Rs. 100 Crores & Investment Upto Rs 20 Crores</td>

                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div>
                                <h5><strong>Benefits of MSME Registration</strong></h5>
                            </div>
                        </div>
                        <div>
                            <div class="it-up-section-title-2 headline-1 text-center mw-100 pb-5">

                                <h2 class="pt-4">BENEFITS OF STARTUP INDIA REGISTRATION</h2>
                            </div>

                            <ul>
                                <li>Startups will be permitted to self-certify compliance with nine labour laws and
                                    environmental laws. In the case of labour laws, no inspection will be conducted for
                                    a period of three years.</li>
                                <li>Startup India enables companies to register through their mobile application and
                                    upload relevant documents. There will also be single window clearances for
                                    approvals, registrations and filing compliances among other things.</li>
                                <li>Patent filing approach will be simplified. The Startup will enjoy a rebate of 80% of
                                    the fee in the patent application. The startup will bear only the statutory fees and
                                    the government will bear all facilitator fees.</li>
                                <li>The Startup India programme will encourage research and innovation among students
                                    who are aspiring entrepreneurs and seven new research parks will be set up to
                                    provide facilities for startups in the R&D sector.</li>
                                <li>Equal opportunities will be provided for both startups and experienced
                                    entrepreneurs. Earlier this was not possible because all applicants required either
                                    ‘prior experience’ or a ‘prior turnover’. But now, public appropriation norms have
                                    been relaxed for startups.</li>
                            </ul>
                        </div>
                        <div>
                            <div class="it-up-section-title-2 headline-1 text-center mw-100 pb-5">

                                <h3 class="pt-4">KEY ANNOUNCEMENTS OF <strong> ATMA-NIRBHAR BHARAT ABHIYAAN</strong>
                                </h3>
                            </div>
                            <div>
                                <ul>
                                    <li>Rs 3 lakh crore collateral free automatic loans for MSMEs</li>
                                    <li>Rs 50,000 crore equity infusion through MSME Fund of Funds</li>
                                    <li>Rs 20 crore subordinate debt for MSMEs</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
    {{-- pricing --}}

<div class="demo pt-5 mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="it-up-section-title headline-1 text-center pb-5">

                    <h2>Packages</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="pricingTable">
                    <div class="pricing-content">
                        <div class="pricingTable-header">
                            <h3 class="title">BASIC</h3>
                            
                        </div>
                        <ul>
                            <li>MSME Registration</li>
                            <li class="disable">Project Report</li>
                            <li class="disable">CMA Data for loan</li>
                            <li class="disable">GEM Registration</li>
                            
                        </ul>
                        <div class="price-value">
                            <p><strong>₹ 1500.00 (All Inclusive)</strong></p>
                            <p><strong>Just pay  1500.00 to start</strong></p>
                        </div>
                        <div class="pricingTable-signup">
                            <a href="#">Buy Now</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="pricingTable purple">
                    <div class="pricing-content">
                        <div class="pricingTable-header">
                            <h3 class="title">ADVANCE</h3>
                        </div>
                        <ul>
                            <li>MSME Registration</li>
                            <li >Project Report</li>
                            <li >CMA Data for loan</li>
                            <li class="disable">GEM Registration</li>
                            
                        </ul>
                        <div class="price-value">
                            <p><strong>₹ 7500.00 (All Inclusive)</strong></p>
                            <p><strong>Just pay ₹ 5000.00 to start</strong></p>
                        </div>
                        <div class="pricingTable-signup">
                            <a href="#">Buy Now</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="pricingTable orange">
                    <div class="pricing-content">
                        <div class="pricingTable-header">
                            <h3 class="title">PREMIUM</h3>
                        </div>
                        <ul>
                            <li>MSME Registration</li>
                            <li>Project Report</li>
                            <li>CMA Data for loan</li>
                            <li>GEM Registration</li>
                            
                        </ul>
                        <div class="price-value bold">
                           <p><strong> ₹ 9000.00 (All Inclusive)</strong></p>
                           <p><strong>Just pay ₹ 7000.00 to start</strong></p>
                        </div>
                        <div class="pricingTable-signup">
                            <a href="#">Buy Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <section id="it-up-about" class="it-up-about-section">
        <div class="it-up-section-title-2 headline-1 text-center">

            <h2 class="pb-5">Process We Follow</h2>
        </div>
        <div class="timeline">
            <div class="containerr left">
                {{-- <div class="date">1</div> --}}
                <i class="icon fa ">1</i>
                <div class="content">
                    <h2>DOCUMENTS COLLECTION</h2>
                    <p>
                        We will require basic documents & bank details to register under MSME.
                    </p>
                </div>
            </div>
            <div class="containerr right">
                {{-- <div class="date">2</div> --}}
                <i class="icon fa">2</i>
                <div class="content">
                    <h2>DOCUMENTS PREPARATION</h2>
                    <p>
                        A Minimum of one and a maximum of 2 working days are required to prepare all the filing documents.After Incorporation,Then the business can be registered as a startup.
                    </p>
                </div>
            </div>
            <div class="containerr left">
                {{-- <div class="date">3</div> --}}
                <i class="icon fa ">3</i>
                <div class="content">
                    <h2>MSME REGISTRATION</h2>
                    <p>
                        Allindiataxfiling.com can Register for MSME in 1 to 2 days.
                    </p>
                </div>
            </div>
            <div class="containerr right">
                {{-- <div class="date"></div> --}}
                <i class="icon fa ">4</i>
                <div class="content">
                    <h2>REGISTRATION CERTIFICATE</h2>
                    <p>
                        Once the Documents are submitted & Approved,MSME certificate will be generated.
                    </p>
                </div>
            </div>

        </div>
    </section>

    <section id="it-up-achivement" class="it-up-achivement-section position-relative">
        <span class="it-up-achive-shape1 position-absolute"><img src="assets/img/its/achive-bg.png" alt=""></span>
        <span class="it-up-achive-shape2 position-absolute"><img src="assets/img/its/achive-bg2.png" alt=""></span>
        <div class="container">
            <div class="it-up-section-title headline-1 text-center">

                <h2>FAQs</h2>
            </div>
            <div class="it-up-achivement-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="accordion" id="accordionExample">
                            <div class="card m-1">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left" type="button"
                                            data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne">
                                            How can I apply for MSME registration?
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Any type of business can apply for MSME registration.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseTwo"
                                            aria-expanded="false" aria-controls="collapseTwo">
                                            Is Aadhaar number mandatory for online MSME registration?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        For MSME registration, Aadhaar card is mandatory.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseThree"
                                            aria-expanded="false" aria-controls="collapseThree">
                                            Can MSME borrowers get collateral free loans from financial institutions?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Yes MSME can get collateral free loan amount
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingFour">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseFour"
                                            aria-expanded="false" aria-controls="collapseFour">
                                            Do I get a hard copy or physical copy of the MSME certificate?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Government has initiated paperless work, so no physical copy is issued. However, the authority emails the certificate instead.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingFive">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseFive"
                                            aria-expanded="false" aria-controls="collapseFive">
                                            What are the Documents required for MSME Registration?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFive" class="collapse" aria-labelledby="headingFive"
                                    data-parent="#accordionExample">
                                    <div class="card-body">

                                        The documents required for MSME registration are mentioned below: Aadhar Number Owners name Name of the Enterprise Type of organization PAN number Official address Date of commencement Bank details Personal details.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingSix">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseSix"
                                            aria-expanded="false" aria-controls="collapseSix">
                                            What is an MSME loan?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseSix" class="collapse" aria-labelledby="headingSix"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        MSME loan is a type of loan which is provided to Micro small and medium enterprises.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingSeven">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseSeven"
                                            aria-expanded="false" aria-controls="collapseSeven">
                                            Can a sole proprietorship firm apply for MSME registration?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Yes, a sole proprietorship firm can apply for MSME registration.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingEight">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseEight"
                                            aria-expanded="false" aria-controls="collapseEight">
                                            Does the MSME holder need any specific experience?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseEight" class="collapse" aria-labelledby="headingEight"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        No, MSME holder requires no minimum experience.
                                    </div>
                                </div>
                            </div>
                            


                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="it-up-achivement" class="it-up-achivement-section position-relative it-sec">
        <span class="it-up-achive-shape1 position-absolute"><img src="assets/img/its/achive-bg.png" alt=""></span>
        <span class="it-up-achive-shape2 position-absolute"><img src="assets/img/its/achive-bg2.png" alt=""></span>
        <div class="container">
            <div class="it-up-section-title headline-1 text-center">

                <h2>WHY AITF</h2>
            </div>
            <div class="it-up-achivement-content">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/taxes.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">24/7 SUPPORT</h5>
                                {{-- <p>Satisfied Clients</p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/idea.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">EXPERT ADVICE</h5>
                                {{-- <p>Team Member</p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/cyber-security.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">SECURITY</h5>
                                {{-- <p>Award Winner</p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/pie-chart.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">FILE IT RIGHT</h5>
                                {{-- <p>Works Done</p> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="it-up-service" class="it-up-service-section position-relative">
        <span class="it-up-service-shape position-absolute deco1"><img src="assets/img/its/s-shape1.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco2"><img src="assets/img/its/s-shape2.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco3"><img src="assets/img/its/s-shape3.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco4"><img src="assets/img/its/s-shape4.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco5"><img src="assets/img/its/s-shape5.png" alt=""></span>
        <div class="container">
            {{-- <div class="it-up-section-title headline-1 text-center">
               
                <h2>We are happy to share our client’s review.</h2>
            </div> --}}
            <div class="it-up-service-content">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="it-up-service-tab-btn">
                            <ul id="tabs" class="nav text-capitalize nav-tabs">
                                <li class="nav-item"><a href="#" data-target="#marketing" data-toggle="tab"
                                        class="nav-link text-capitalize active">WE ARE DIFFERENT!</a></li>
                                <li class="nav-item"><a href="#" data-target="#designing" data-toggle="tab"
                                        class="nav-link text-capitalize">WHY CHOOSE</a></li>
                                <li class="nav-item"><a href="#" data-target="#development" data-toggle="tab"
                                        class="nav-link text-capitalize">SERVICES OFFERING !</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="it-up-service-tab-content">
                            <div id="tabsContent" class="tab-content">
                                <div id="marketing" class="tab-pane fade active show">
                                    <div class="it-up-service-tab-wrap clearfix">
                                        <div
                                            class="it-up-service-tab-text position-relative float-left ul-li-block headline-1">
                                            <h3>WE ARE DIFFERENT!</h3>
                                            <h5>At Allindiataxfiling.com we ensure complete transparency at every step
                                                of the business incorporation process.We specialise in excellent
                                                customer service. When you call, email with our offices, you will
                                                receive personal attention from a knowledgeable specialist happy to help
                                                you .Our team of business startup specialists are here to keep you
                                                updated on the latest incorporation, tax and financial strategies and to
                                                help you manage important business details.

                                            </h5>
                                            {{-- <a class="it-up-ser-btn" href="#">Check Details <i class="flaticon-right-arrow"></i></a>
                                            <span class="it-up-tab-icon position-absolute"><img src="assets/img/its/icon/tab-icon1.png" alt=""></span> --}}
                                        </div>
                                        <div class="it-up-service-img float-right">
                                            <img src="assets/img/its/ser-tab1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div id="designing" class="tab-pane fade">
                                    <div class="it-up-service-tab-wrap clearfix">
                                        <div
                                            class="it-up-service-tab-text position-relative float-left ul-li-block headline-1">
                                            <h3>WHY CHOOSE</h3>
                                            <ul>
                                                <li>Specialized knowledge and experience in Taxation & Business
                                                    Registration.</li>
                                                <li>More communication than what larger agencies provide.</li>
                                                <li>Strategic partnership and advice for potential opportunities to grow
                                                    your brand.</li>
                                                <li>Work is done in-house and not outsourced to a third party.</li>
                                                <li>We practice what we preach and use our own services to grow our
                                                    business.</li>
                                            </ul>
                                            {{-- <a class="it-up-ser-btn" href="#">Check Details <i
                                                    class="flaticon-right-arrow"></i></a>
                                            <span class="it-up-tab-icon position-absolute"><img
                                                    src="assets/img/its/icon/tab-icon1.png" alt=""></span> --}}
                                        </div>
                                        <div class="it-up-service-img float-right">
                                            <img src="assets/img/its/ser-tab1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div id="development" class="tab-pane fade">
                                    <div class="it-up-service-tab-wrap clearfix">
                                        <div
                                            class="it-up-service-tab-text position-relative float-left ul-li-block headline-1">
                                            <h3>SERVICES OFFERING !</h3>
                                            <h5>Allindiataxfiling.com provides fast and end-to-end Business
                                                Incorporation and associated services. We also provide Accounting,
                                                Taxation and Legal Services. We guarantee hassle-free service delivery
                                                in the shortest possible time without compromising on quality.</h5>
                                            <h5>Our team of experienced professionals assists entrepreneurs in selecting
                                                the best business structure suiting individual requirements, and to
                                                convert their dream business into appropriate corporate entities.</h5>
                                            {{-- <a class="it-up-ser-btn" href="#">Check Details <i
                                                    class="flaticon-right-arrow"></i></a>
                                            <span class="it-up-tab-icon position-absolute"><img
                                                    src="assets/img/its/icon/tab-icon1.png" alt=""></span> --}}
                                        </div>
                                        <div class="it-up-service-img float-right">
                                            <img src="assets/img/its/ser-tab1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-3">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr">
                        <div class="counter-icon">
                            <i class="fas fa-medal"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Years</h3>
                            <span class="counter-value">10</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr green">
                        <div class="counter-icon">
                            <i class="fas fa-briefcase"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Projects</h3>
                            <span class="counter-value">947</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr blue">
                        <div class="counter-icon">
                            <i class="fas fa-user-tie"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Clients</h3>
                            <span class="counter-value">867</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr purple">
                        <div class="counter-icon">
                            <i class="fas fa-map-marker-alt"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Branches</h3>
                            <span class="counter-value">4</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <section class="pt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 pt-5 pb-5">
                    <div class="it-up-section-title headline-1 text-center">
                        <h2>OUR PARTNERS</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="it-up-sponsor" class="it-up-sponsor-section">
                        <div class="container">
                            <div id="it-up-sponsor-slide" class="it-up-sponsor-slider owl-carousel">
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/gluehost.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/spellbound.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/aa.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/db.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/daxy.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img pt-4">
                                    <img src="assets/img/its/shrex.png" alt="">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="it-up-testimonial" class="it-up-testimonial-section">
        <div class="container">
            <div class="it-up-section-title-2 headline-1 text-center">
                <span>Our Client’s Feedback</span>
                <h2>We are happy to share our client’s review.</h2>
            </div>
            <div class="it-up-testimonial-content position-relative">
                <span class="it-up-testi-shape1 position-absolute"><img src="assets/img/its/b-shape3.png" alt=""></span>
                <span class="it-up-testi-shape2 position-absolute" data-parallax='{"y" : 70}'><img
                        src="assets/img/its/tst-sh1.png" alt=""></span>
                <span class="it-up-testi-shape3 position-absolute" data-parallax='{"x" : -70}'><img
                        src="assets/img/its/tst-sh-1.png" alt=""></span>
                <span class="it-up-testi-shape4 position-absolute"><img src="assets/img/its/tst-sh3.png" alt=""></span>
                <div class="it-up-testimonial-slider-wrap owl-carousel">
                    <div class="it-up-testimonial-innerbox it-up-testimonial-green text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst2.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p>I really like the services provided by AITF for its admirable service, instructive and precise. 
AITF always accommodating and ready to support for any query. I do recommend AITF.
</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Abhishek Raj Rathod</a></h4>
                                <span>- Founder- Aximo infotech Pvt Ltd</span>
                            </div>
                        </div>
                    </div>
                    <div class="it-up-testimonial-innerbox it-up-testimonial-pink text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst1.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p>Everything from incorporation of your company to the trademark and all filing they coverd
                                it all. one of the best CA consultant services they provide.</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Pranav Dhakulkar</a></h4>
                                <span>- Founder- Dryomix Concrete Solutions Pvt Ltd</span>
                            </div>
                        </div>
                    </div>
                    <div class="it-up-testimonial-innerbox it-up-testimonial-green text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst2.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p>In single sentence "AITF " is one stop solution for services Like Company Incorporation,
                                Trademark Registration or any work related to CA. Cost & services are very good.</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Bestayur Lifecare</a></h4>
                                <span>- Bestayur Lifecare</span>
                            </div>
                        </div>
                    </div>
                    <div class="it-up-testimonial-innerbox it-up-testimonial-blue text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst3.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p>Professional services at competitive price. They give us complete end to end support from
                                incorporation to expansion. Highly recommended.

                                Keep it up.</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Abhishek kumar</a></h4>
                                <span>- Founder-Koibhikaamcube</span>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>
    @include('components/footer')
    @include('components/foot-link')
    <script>
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.display === "block") {
                    panel.style.display = "none";
                } else {
                    panel.style.display = "block";
                }
            });
        }
    </script>


</body>

</html>

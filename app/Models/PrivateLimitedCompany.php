<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrivateLimitedCompany extends Model
{
    protected $table = 'private_limited_company';
    use HasFactory;
}

<?php

namespace App\Http\Controllers\Webpages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\IncomeTaxReturn;
use App\Models\TdsReturn;
use App\Models\CompanyAnnualFiling;
use App\Models\LlpAnnualFiling;
use App\Models\DigitalSignatureCertificate;

class FilingController extends Controller
{
    public function index(){
        return view('webpages.filing');
    }
    public function IncomeTaxReturn(){
        return view('webpages.income-tax-return');
    }
    public function TdsReturn(){
        return view('webpages.tds-return');
    }
    public function CompanyAnnualFiling(){
        return view('webpages.company-annual-filling');
    }
    public function LlpAnnualFiling(){
        return view('webpages.llp-annual-filing');
    }
    public function DigitalSignature(){
        return view('webpages.digital-signature');
    }
    public function saveIncomeTaxReturn(Request $request){
        $data = new IncomeTaxReturn;
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->mobile = $request->input('mobile');
        $data->message = $request->input('message');
        $data->save();
        return redirect('/income_tax_return');
    }
    public function saveTdsReturn(Request $request){
        $data = new TdsReturn;
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->mobile = $request->input('mobile');
        $data->message = $request->input('message');
        $data->save();
        return redirect('/tds_return');
    }
    public function saveCompanyAnnualFiling(Request $request){
        $data = new CompanyAnnualFiling;
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->mobile = $request->input('mobile');
        $data->message = $request->input('message');
        $data->save();
        return redirect('/company_annual_filling');
    }
    public function saveLlpAnnualFiling(Request $request){
        $data = new LlpAnnualFiling;
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->mobile = $request->input('mobile');
        $data->message = $request->input('message');
        $data->save();
        return redirect('/llp_annual_filing');
    }
    public function saveDigitalSignatureCertificate(Request $request){
        $data = new DigitalSignatureCertificate;
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->mobile = $request->input('mobile');
        $data->message = $request->input('message');
        $data->save();
        return redirect('/digital_signature');
    }
    
}

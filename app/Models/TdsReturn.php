<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TdsReturn extends Model
{
    protected $table = 'tds_return';
    use HasFactory;
}

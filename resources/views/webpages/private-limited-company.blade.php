<!DOCTYPE html>
<html lang="en">

<head>
    <title>private limited company</title>
    @include('components/head-link')
</head>
@include('components/header')

<body class="s-it">
    {{-- <div id="preloader"></div> --}}
    <div class="up">
        <a href="#" class="scrollup text-center"><i class="fas fa-chevron-up"></i></a>
    </div>
    <section class="pb-5">

<div class=" page-banner">
    <span class=""></span>
    <div class="row">
        <div class="col-md-12">
            <div>
            <ul class="my-breadcrump">
               <li class="home-bread"><a href="index">Home</a></li>
                <li class=""><i class="fas fa-chevron-right my-arrow"><a class="home-bread-1" href="startups">Startups</a></i></li>
                <li class=""><i class="fas fa-chevron-right my-arrow"><a class="home-bread-1" href="#">Private limited company</a></i></li>

            </ul>

           <h1 class="page-banner-heading">Private Limited Company</h1>

            </div>
        </div>
    </div>
</div>
</section>


    <!-- Start of banner section
 ============================================= -->
    <section id="it-up-banner" class="it-up-banner-section position-relative">
        <span class="it-up-banner-deco1 position-absolute"> <img src="assets/img/its/b-vector1.png" alt=""></span>
        <span class="it-up-banner-deco2 position-absolute"> <img src="assets/img/its/b-shape1.png" alt=""></span>
        <span class="it-up-banner-deco3 position-absolute"> <img src="assets/img/its/b-shape2.png" alt=""></span>
        <span class="it-up-banner-deco4 position-absolute"> <img src="assets/img/its/b-shape3.png" alt=""></span>
        <div class="container">

            <div class="it-up-banner-content position-relative ">
                <div class="it-up-banner-text headline-1 pera-content it-up-pages-text">

                    <!-- <h1>Private Limited Company</h1> -->
                    {{-- <p>Our vertical solutions expertise allows your business to streamline workflow.</p> --}}
                    <div>
                        <img src="assets/img/its/banner-pic/private-limited.png" alt="gst registration" class="img-class img-fluid">
                    </div>

                </div>
                <div class="it-up-banner-img my-form">
                    <div class="card bg-light mb-3">
                        <div class="p-3">
                            <h5 class="text-center">PRIVATE LIMITED COMPANY</h5>
                            <h5 class="text-center">Rs. 3999.00/- All Inclusive</h5>
                        </div>
                    </div>
                    <div class="card bg-light form-bg">
                        <article class="card-body ">

                            <form method="post" action="save_privatelc">
                                {{csrf_field()}}
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                                    </div>
                                    <input name="name" class="form-control" placeholder="Full name" type="text" required>
                                </div> <!-- form-group// -->
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                                    </div>
                                    <input name="email" class="form-control" placeholder="Email address" type="email" required>
                                </div> <!-- form-group// -->
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                                    </div>

                                    <input name="mobile" class="form-control" placeholder="Phone number" type="text" required>
                                </div>
                                <div class="form-group input-group">
                                    <div class="form-group w-100">
                                        <label for="comment">Type your message: </label>
                                        <textarea class="form-control" rows="" id="comment" name="message" required></textarea>
                                    </div>
                                </div>
                                <div>
                                    <div class="it-up-header-cta-btn  text-center">
                                        <button type="submit" class="new-btn">Submit Now</button>
                                    </div>
                                </div>

                            </form>
                        </article>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- End of banner section
 ============================================= -->

    <!-- Start of Featured section
 ============================================= -->
    <section id="it-up-featured" class="it-up-featured-section position-relative">
        <span class="it-up-ft-bg position-absolute"><img src="assets/img/its/ft-bg.png" alt=""></span>
        <!-- <span class="it-up-ft-shape position-absolute"><img src="assets/img/its/ft-shape1.png" alt=""></span> -->
        <span class="it-up-ft-shape2 position-absolute"><img src="assets/img/its/b-shape3.png" alt=""></span>
        <div class="container">
            <div class="it-up-section-title-2 headline-1 text-center">
                {{-- <span>Our Featured Services</span> --}}
                <h2>PRIVATE LIMITED COMPANY</h2>
            </div>
            <div class="it-up-featured-content">
                <div class="row">
                    {{-- <div class="col-md-12">
                        <div class="it-up-featured-innerbox text-center">
                            <div class="it-up-featured-icon">
                                <img src="assets/img/its/icon/ft-icon1.png" alt="">
                            </div>
                            <div class="it-up-featured-text headline-1 pera-content">
                                <h3><a href="#">GST REGISTRATION</a></h3>
                                <p>As per the GST Council, entities in the Northeaster and hill states with an annual turnover of Rs.20 lakhs and above would be required to obtain GST registration..</p>
                                <a class="it-up-ft-more" href="#">Read more <i class="flaticon-right-arrow"></i></a>
                            </div>
                        </div>
                    </div> --}}
                    <div class="col-md-12">
                        <div>
                            <p class="section-p">Private Limited Company Is The Preferred Choice Of Business For Startups, Or If You Plan To Raise Funding

</p>
                            <br>
                            <p>Private Limited Company, the most popular legal structure for businesses, should be chosen by anyone looking to build a scalable business. Indian start-ups and growing companies pick it because it allows outside funding to be raised easily, limits the liabilities of its shareholders and enables them to offer employee stock options to attract top talent.Private Limited Companies are closely held companies where minimum number of members is two and maximum number is Two hundred.</p>
                            <P>A private limited company has the Limited Liability of members, greater Stability and Separate legal entity &has all the advantages of partnership namely flexibility, greater capital combination etc.</P>
                            <P>Private companies may issue stock and have shareholders. However, their shares do not trade on public exchanges and are not issued through an initial public offering.Shareholders may not be able to sell their shares without the agreement of the other shareholders. Scope of expansion is higher because easy to raise capital from financial institutions and the advantage of limited liability.

</P>
<P>In this sense, a private limited company stands between partnership and widely owned public company. Identifying marks of a private limited company are name, number of members, shares, formation, management, directors and meetings, etc.Pvt. Ltd places certain restrictions on its ownership that are defined in the company's bylaws or regulations and are meant to prevent any hostile takeover attempt.</P>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
    <!-- End of Featured section
 ============================================= -->
 <!-- PACKAGE START -->
 <div class="demo pt-5 mt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="pricingTable">
                        <div class="pricing-content">
                            <div class="pricingTable-header">
                                <h3 class="title">Basic</h3>
                            </div>
                            <ul>
                                <li>DSC of 1 Director(Class-3)</li>
                                <li>DIN of 1 Director</li>
                                <li>MOA & AOA of company</li>
                                <li>Incorporation Certificate</li>
                                <li>All Government Fee & Stamp Duty</li>
                                <li>Company PAN</li>
                                <li>Company TAN</li>
                                <li>PF</li>
                                <li>ESIC</li>
                                <li>PT (Maharashtra & Karnataka)</li>
                                <li>Bank Account</li>
                                <li class="disable">Dedicated Account Manager</li>
                                <li class="disable">GST Registration</li>
                                <li class="disable">3 Months GST Filing</li>
                                <li class="disable">MSME Registration</li>
                                <li class="disable">Commencement of Business Certificate (INC 20A)</li>
                                <li class="disable">Share Certificates</li>
                                <li class="disable">Startup India Registration</li>
                                <li class="disable">First Auditor Appointment. Assistance</li>
                            </ul>
                            <div class="price-value">
                                <p><strong> ₹3999.00(All Inclusive)</p></strong>
                                <p><strong>Just pay ₹3999.00 to start</p></strong>
                            </div>
                            <div class="pricingTable-signup">
                                <a href="#">Buy Now</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="pricingTable purple">
                        <div class="pricing-content">
                            <div class="pricingTable-header">
                                <h3 class="title">Advance</h3>
                            </div>
                            <ul>
                                <li>DSC of 1 Director(Class-3)</li>
                                <li>DIN of 1 Director</li>
                                <li>MOA & AOA of company</li>
                                <li>Incorporation Certificate</li>
                                <li>All Government Fee & Stamp Duty</li>
                                <li>Company PAN</li>
                                <li>Company TAN</li>
                                <li>PF</li>
                                <li>ESIC</li>
                                <li>PT (Maharashtra & Karnataka)</li>
                                <li>Bank Account</li>
                                <li >Dedicated Account Manager</li>
                                <li >GST Registration</li>
                                <li >3 Months GST Filing</li>
                                <li>MSME Registration</li>
                                <li class="disable">Commencement of Business Certificate (INC 20A)</li>
                                <li class="disable">Share Certificates</li>
                                <li class="disable">Startup India Registration</li>
                                <li class="disable">First Auditor Appointment. Assistance</li>
                            </ul>
                            <div class="price-value">
                                <p><strong> ₹5999.00(All Inclusive)</p></strong>
                                <p><strong>Just pay ₹3999.00 to start</p></strong>
                            </div>
                            <div class="pricingTable-signup">
                                <a href="#">Buy Now</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="pricingTable orange">
                        <div class="pricing-content">
                            <div class="pricingTable-header">
                                <h3 class="title">Premium</h3>
                            </div>
                            <ul>
                            <li>DSC of 1 Director(Class-3)</li>
                                <li>DIN of 1 Director</li>
                                <li>MOA & AOA of company</li>
                                <li>Incorporation Certificate</li>
                                <li>All Government Fee & Stamp Duty</li>
                                <li>Company PAN</li>
                                <li>Company TAN</li>
                                <li>PF</li>
                                <li>ESIC</li>
                                <li>PT (Maharashtra & Karnataka)</li>
                                <li>Bank Account</li>
                                <li >Dedicated Account Manager</li>
                                <li >GST Registration</li>
                                <li >3 Months GST Filing</li>
                                <li>MSME Registration</li>
                                <li >Commencement of Business Certificate (INC 20A)</li>
                                <li>Share Certificates</li>
                                <li>Startup India Registration</li>
                                <li>First Auditor Appointment. Assistance</li>
                            </ul>
                            <div class="price-value">
                                <p><strong>₹10999.00(All Inclusive)</p></strong>
                                <p><strong>Just pay ₹7000.00 to start</p></strong>
                            </div>
                            <div class="pricingTable-signup">
                                <a href="#">Buy Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <!-- PACKAGE END -->


    <!-- Start of about section
 ============================================= -->
    <section id="it-up-about" class="it-up-about-section">
        <div class="it-up-section-title-2 headline-1 text-center">

            <h2 class="pb-5">Process We Follow</h2>
        </div>
        <div class="timeline">
            <div class="containerr left">
                {{-- <div class="date">1</div> --}}
                <i class="icon fa ">1</i>
                <div class="content">
                    <h2>PRIVATE LIMITED COMPANY REGISTRATION</h2>
                    <p>
                    Allindiataxfiling.com can incorporate a Private Limited Company in 6 to 8 days, subject to ROC processing time.
                    </p>
                </div>
            </div>
            <div class="containerr right">
                {{-- <div class="date">2</div> --}}
                <i class="icon fa">2</i>
                <div class="content">
                    <h2>OBTAINING DSC</h2>
                    <p>
                    Digital Signature Certificate(DSC) is required for the proposed Directors of the Private Limited Company, which can be obtained within 1 day.
                    </p>
                </div>
            </div>
            <div class="containerr left">
                {{-- <div class="date">3</div> --}}
                <i class="icon fa ">3</i>
                <div class="content">
                    <h2>DOCUMENTS PREPARATION</h2>
                    <p>
                    A Minimum of one and a maximum of 2 working days are required to prepare all the filing documents
                    </p>
                </div>
            </div>
            <div class="containerr right">
                {{-- <div class="date"></div> --}}
                <i class="icon fa ">4</i>
                <div class="content">
                    <h2>COMPANY REGISTRATION</h2>
                    <p>
                    Registration documents can be submitted to the MCA along with an application for registration. MCA will usually approve the application for incorporation in 1 to 2 days, subject to their processing time.
                    </p>
                </div>
            </div>
            {{-- <div class="containerr left">
              <div class="date">10 Feb</div>
              <i class="icon fa fa-cog"></i>
              <div class="content">
                <h2>Lorem ipsum dolor sit amet</h2>
                <p>
                  Lorem ipsum dolor sit amet elit. Aliquam odio dolor, id luctus erat sagittis non. Ut blandit semper pretium.
                </p>
              </div>
            </div>
            <div class="containerr right">
              <div class="date">01 Jan</div>
              <i class="icon fa fa-certificate"></i>
              <div class="content">
                <h2>Lorem ipsum dolor sit amet</h2>
                <p>
                  Lorem ipsum dolor sit amet elit. Aliquam odio dolor, id luctus erat sagittis non. Ut blandit semper pretium.
                </p>
              </div>
            </div> --}}
        </div>
    </section>
    <!-- End of about section
 ============================================= -->

    <!-- Start of achivement section
 ============================================= -->
    <section id="it-up-achivement" class="it-up-achivement-section position-relative">
        <span class="it-up-achive-shape1 position-absolute"><img src="assets/img/its/achive-bg.png" alt=""></span>
        <span class="it-up-achive-shape2 position-absolute"><img src="assets/img/its/achive-bg2.png" alt=""></span>
        <div class="container">
            <div class="it-up-section-title headline-1 text-center">

                <h2>FAQs</h2>
            </div>
            <div class="it-up-achivement-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="accordion" id="accordionExample">
                            <div class="card m-1">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left" type="button"
                                            data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne">
                                            How long does it take to register a company?

                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    With New form total time to register a Private Limited company is 6-8 working days.To ensure speedy incorporation, please choose a unique name for your Company and ensure you have all the required documents prior to starting the incorporation process.

                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseTwo"
                                            aria-expanded="false" aria-controls="collapseTwo">
                                            What are the requirements to be a Director?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    The Director needs to be over 18 years of age and must be a natural person. There are no limitations in terms of citizenship or residency. Therefore, even foreign nationals can be Directors in an Indian Private Limited Company.However, atleast one Director on the Board of Directors must be a Resident India.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseThree"
                                            aria-expanded="false" aria-controls="collapseThree">
                                            What are the documents required to open a Private Limited Company?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    The documents requirements are as follows Pan Card for Indian Nationals, ID proof- Any one (Voter ID / Aadhar Card/Driving License / Passport), Address Proof- Any one (Electricity Bill / Telephone Bill / Mobile Bill / Bank Statement)
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingFour">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseFour"
                                            aria-expanded="false" aria-controls="collapseFour">
                                            What is the capital required to start a private limited company?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    You can start a Private Limited Company with any amount of capital. However, fee must be paid to the Government for issuing a minimum of shares worth Rs.1 lakh [Authorized Capital Fee] during the registration of the Company. There is no requirement to show proof of capital invested during the registration process.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingFive">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseFive"
                                            aria-expanded="false" aria-controls="collapseFive">
                                            How many people are required to incorporate a private limited company?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFive" class="collapse" aria-labelledby="headingFive"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    To incorporate a private limited company, a minimum of two shareholders and minimum of two Directors are required. Directors can be the same as shareholders.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingSix">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseSix"
                                            aria-expanded="false" aria-controls="collapseSix">
                                            How long is the company valid for?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseSix" class="collapse" aria-labelledby="headingSix"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    Once a Company is incorporated, it will be active and in-existence as long as the annual compliances are met regularly. In case, annual compliances are not complied with, the Company will become a Dormant Company and maybe struck off from the register after a period of time. A struck-off Company can be revived for a period of upto 20 years.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingSeven">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseSeven"
                                            aria-expanded="false" aria-controls="collapseSeven">
                                            What are the statutory compliances required for a Private Limited Company?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    A private limited company must hold a Board Meeting atleast once in every 3 months. In addition to the Board Meetings, the Private Limited Company, atleast once every year, must conduct an Annual General Meeting.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingEight">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseEight"
                                            aria-expanded="false" aria-controls="collapseEight">
                                            What do I need to quickly incorporate my Company?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseEight" class="collapse" aria-labelledby="headingEight"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    To incorporate a Company quickly, make sure the proposed name of the Private Limited Company is very unique. Names that are similar to an existing private limited company / limited liability partnership / trademark can be rejected and additional time will be required for resubmission of names.

                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingNine">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseNine"
                                            aria-expanded="false" aria-controls="collapseNine">
                                            What is a Digital Signature Certificate?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseNine" class="collapse" aria-labelledby="headingNine"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    A Digital Signature establishes the identity of the sender or signee electronically while filing documents through the Internet. The Ministry of Corporate Affairs (MCA) mandates that the Directors sign some of the application documents using their Digital Signature. Hence, a Digital Signature is required for atleast one Directors of a proposed Company.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingTen">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseTen"
                                            aria-expanded="false" aria-controls="collapseTen">
                                            Can a Salaried person become the director in Private Limited Company?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTen" class="collapse" aria-labelledby="headingTen"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    Yes, a salaried person become the director in private limited company, there are no legal bondages in this but you have to go through with your employment agreement if it contains any restrictions on doing so.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingEleven">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseEleven"
                                            aria-expanded="false" aria-controls="collapseEleven">
                                            What is Director Identification Number (DIN)?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    Director Identification Number is a unique identification number assigned to all existing and proposed Directors of a Company. It is mandatory for all present or proposed Directors to have a Director Identification Number. Director Identification Number never expires and a person can have only one Director Identification Number.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingTwelve">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseTwelve"
                                            aria-expanded="false" aria-controls="collapseTwelve">
                                            Do I have to be present in person to incorporate a Private Limited Company?

                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwelve" class="collapse" aria-labelledby="headingTwelve"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    No, you will not have to be present at our office or appear at any office for the registration of a Private Limited Company. All the documents can be scanned and sent through email to our office. Some documents will also have to be couriered to our office.

                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingThirteen">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseThirteen"
                                            aria-expanded="false" aria-controls="collapseThirteen">
                                            What is MOA?

                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseThirteen" class="collapse" aria-labelledby="headingThirteen"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    Memorandum of Association (MOA) is a legal document prepared in the formation and registration process of a company to define its relationship with shareholders and contains the aims and objectives of the company. MOA has to be drafted very carefully as the company cannot go against anything that is mentioned in it.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingFourteen">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseFourteen"
                                            aria-expanded="false" aria-controls="collapseFourteen">
                                            Can NRIs / Foreigners hold shares of a Private Limited Company?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFourteen" class="collapse" aria-labelledby="headingFourteen"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    Yes, NRIs / Foreign Nationals / Foreign Companies can hold shares of a Private Limited Company subject to Foreign Direct Investment (FDI) Guidelines.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingFifteen">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseFifteen"
                                            aria-expanded="false" aria-controls="collapseFifteen">
                                            What is AOA?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFifteen" class="collapse" aria-labelledby="headingFifteen"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    Articles of Association (AOA) are by-laws of the company. According to which directors and other officers are required to perform their function as regard the management of the company, its accounts and audit. It regulates domestic management of a company and creates certain rights and obligations between the members and the company.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingsixteen">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapsesixteen"
                                            aria-expanded="false" aria-controls="collapsesixteen">
                                            How to choose the name of the company?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapsesixteen" class="collapse" aria-labelledby="headingsixteen"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    First ensure that company name is not similar to any other Private limited, OPC, LLP or Public limited company. Also, do check If your first word of Company name is not a registered trademark taken by anybody under the IP act. Also, make sure the name is not too generic to be accepted by the ROC and also, try not to use abbreviations, adjectives. While choosing the name make sure that name should contain the objective of the business.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingseventeen">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseseventeen"
                                            aria-expanded="false" aria-controls="collapseseventeen">
                                            Is an office required to open a company?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseseventeen" class="collapse" aria-labelledby="headingseventeen"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                    No. You can open a company on your residential address there is no requirement to have a commercial place to open up a company. For Registered Office Address Rent agreement along with latest rent receipt (in case the premises are rented) House tax receipts (in case premises are owned) Electricity bill NOC from the Owner
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="it-up-achivement" class="it-up-achivement-section position-relative it-sec">
        <span class="it-up-achive-shape1 position-absolute"><img src="assets/img/its/achive-bg.png" alt=""></span>
        <span class="it-up-achive-shape2 position-absolute"><img src="assets/img/its/achive-bg2.png" alt=""></span>
        <div class="container">
            <div class="it-up-section-title headline-1 text-center">

                <h2>WHY AITF</h2>
            </div>
            <div class="it-up-achivement-content">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/taxes.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">24/7 SUPPORT</h5>
                                {{-- <p>Satisfied Clients</p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/idea.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">EXPERT ADVICE</h5>
                                {{-- <p>Team Member</p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/cyber-security.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">SECURITY</h5>
                                {{-- <p>Award Winner</p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/pie-chart.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">FILE IT RIGHT</h5>
                                {{-- <p>Works Done</p> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="it-up-service" class="it-up-service-section position-relative">
        <span class="it-up-service-shape position-absolute deco1"><img src="assets/img/its/s-shape1.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco2"><img src="assets/img/its/s-shape2.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco3"><img src="assets/img/its/s-shape3.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco4"><img src="assets/img/its/s-shape4.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco5"><img src="assets/img/its/s-shape5.png" alt=""></span>
        <div class="container">
            {{-- <div class="it-up-section-title headline-1 text-center">
               
                <h2>We are happy to share our client’s review.</h2>
            </div> --}}
            <div class="it-up-service-content">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="it-up-service-tab-btn">
                            <ul id="tabs" class="nav text-capitalize nav-tabs">
                                <li class="nav-item"><a href="#" data-target="#marketing" data-toggle="tab"
                                        class="nav-link text-capitalize active">WE ARE DIFFERENT!</a></li>
                                <li class="nav-item"><a href="#" data-target="#designing" data-toggle="tab"
                                        class="nav-link text-capitalize">WHY CHOOSE</a></li>
                                <li class="nav-item"><a href="#" data-target="#development" data-toggle="tab"
                                        class="nav-link text-capitalize">SERVICES OFFERING !</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="it-up-service-tab-content">
                            <div id="tabsContent" class="tab-content">
                                <div id="marketing" class="tab-pane fade active show">
                                    <div class="it-up-service-tab-wrap clearfix">
                                        <div
                                            class="it-up-service-tab-text position-relative float-left ul-li-block headline-1">
                                            <h3>WE ARE DIFFERENT!</h3>
                                            <h5>At Allindiataxfiling.com we ensure complete transparency at every step
                                                of the business incorporation process.We specialise in excellent
                                                customer service. When you call, email with our offices, you will
                                                receive personal attention from a knowledgeable specialist happy to help
                                                you .Our team of business startup specialists are here to keep you
                                                updated on the latest incorporation, tax and financial strategies and to
                                                help you manage important business details.

                                            </h5>
                                            {{-- <a class="it-up-ser-btn" href="#">Check Details <i class="flaticon-right-arrow"></i></a>
                                            <span class="it-up-tab-icon position-absolute"><img src="assets/img/its/icon/tab-icon1.png" alt=""></span> --}}
                                        </div>
                                        <div class="it-up-service-img float-right">
                                            <img src="assets/img/its/ser-tab1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div id="designing" class="tab-pane fade">
                                    <div class="it-up-service-tab-wrap clearfix">
                                        <div
                                            class="it-up-service-tab-text position-relative float-left ul-li-block headline-1">
                                            <h3>WHY CHOOSE</h3>
                                            <ul>
                                                <li>Specialized knowledge and experience in Taxation & Business
                                                    Registration.</li>
                                                <li>More communication than what larger agencies provide.</li>
                                                <li>Strategic partnership and advice for potential opportunities to grow
                                                    your brand.</li>
                                                <li>Work is done in-house and not outsourced to a third party.</li>
                                                <li>We practice what we preach and use our own services to grow our
                                                    business.</li>
                                            </ul>
                                            {{-- <a class="it-up-ser-btn" href="#">Check Details <i
                                                    class="flaticon-right-arrow"></i></a>
                                            <span class="it-up-tab-icon position-absolute"><img
                                                    src="assets/img/its/icon/tab-icon1.png" alt=""></span> --}}
                                        </div>
                                        <div class="it-up-service-img float-right">
                                            <img src="assets/img/its/ser-tab1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div id="development" class="tab-pane fade">
                                    <div class="it-up-service-tab-wrap clearfix">
                                        <div
                                            class="it-up-service-tab-text position-relative float-left ul-li-block headline-1">
                                            <h3>SERVICES OFFERING !</h3>
                                            <h5>Allindiataxfiling.com provides fast and end-to-end Business
                                                Incorporation and associated services. We also provide Accounting,
                                                Taxation and Legal Services. We guarantee hassle-free service delivery
                                                in the shortest possible time without compromising on quality.</h5>
                                            <h5>Our team of experienced professionals assists entrepreneurs in selecting
                                                the best business structure suiting individual requirements, and to
                                                convert their dream business into appropriate corporate entities.</h5>
                                            {{-- <a class="it-up-ser-btn" href="#">Check Details <i
                                                    class="flaticon-right-arrow"></i></a>
                                            <span class="it-up-tab-icon position-absolute"><img
                                                    src="assets/img/its/icon/tab-icon1.png" alt=""></span> --}}
                                        </div>
                                        <div class="it-up-service-img float-right">
                                            <img src="assets/img/its/ser-tab1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-3">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr">
                        <div class="counter-icon">
                            <i class="fas fa-medal"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Years</h3>
                            <span class="counter-value">10</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr green">
                        <div class="counter-icon">
                            <i class="fas fa-briefcase"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Projects</h3>
                            <span class="counter-value">947</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr blue">
                        <div class="counter-icon">
                            <i class="fas fa-user-tie"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Clients</h3>
                            <span class="counter-value">867</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr purple">
                        <div class="counter-icon">
                            <i class="fas fa-map-marker-alt"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Branches</h3>
                            <span class="counter-value">4</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </section>
    <section class="pt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 pt-5 pb-5">
                    <div class="it-up-section-title headline-1 text-center">
                        <h2>OUR PARTNERS</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="it-up-sponsor" class="it-up-sponsor-section">
                        <div class="container">
                            <div id="it-up-sponsor-slide" class="it-up-sponsor-slider owl-carousel">
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/gluehost.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/spellbound.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/aa.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/db.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/daxy.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img pt-4">
                                    <img src="assets/img/its/shrex.png" alt="">
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="it-up-testimonial" class="it-up-testimonial-section">
        <div class="container">
            <div class="it-up-section-title-2 headline-1 text-center">
                <span>Our Client’s Feedback</span>
                <h2>We are happy to share our client’s review.</h2>
            </div>
            <div class="it-up-testimonial-content position-relative">
                <span class="it-up-testi-shape1 position-absolute"><img src="assets/img/its/b-shape3.png" alt=""></span>
                <span class="it-up-testi-shape2 position-absolute" data-parallax='{"y" : 70}'><img src="assets/img/its/tst-sh1.png" alt=""></span>
                <span class="it-up-testi-shape3 position-absolute" data-parallax='{"x" : -70}'><img src="assets/img/its/tst-sh-1.png" alt=""></span>
                <span class="it-up-testi-shape4 position-absolute"><img src="assets/img/its/tst-sh3.png" alt=""></span>
                <div class="it-up-testimonial-slider-wrap owl-carousel">
                    <div class="it-up-testimonial-innerbox it-up-testimonial-green text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst2.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p>I really like the services provided by AITF for its admirable service, instructive and precise. 
AITF always accommodating and ready to support for any query. I do recommend AITF.
</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Abhishek Raj Rathod</a></h4>
                                <span>- Founder- Aximo infotech Pvt Ltd</span>
                            </div>
                        </div>
                    </div>
                    <div class="it-up-testimonial-innerbox it-up-testimonial-pink text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst1.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p>Everything from incorporation of your company to the trademark and all filing they coverd it all. one of the best CA consultant services they provide.</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Pranav Dhakulkar</a></h4>
                                <span>- Founder- Dryomix Concrete Solutions Pvt Ltd</span>
                            </div>
                        </div>
                    </div>
                    <div class="it-up-testimonial-innerbox it-up-testimonial-green text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst2.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p>In single sentence "AITF " is one stop solution for services Like Company Incorporation, Trademark Registration or any work related to CA. Cost & services are very good.</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Bestayur Lifecare</a></h4>
                                <span>- Bestayur Lifecare</span>
                            </div>
                        </div>
                    </div>
                    <div class="it-up-testimonial-innerbox it-up-testimonial-blue text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst3.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p>Professional services at competitive price. They give us complete end to end support from incorporation to expansion. Highly recommended.

                                Keep it up.</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Abhishek kumar</a></h4>
                                <span>-  Founder-Koibhikaamcube</span>
                            </div>
                        </div>
                    </div>
                   
                    
                </div>
            </div>
        </div>
    </section>
    @include('components/footer')
    @include('components/foot-link')
    <script>
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.display === "block") {
                    panel.style.display = "none";
                } else {
                    panel.style.display = "block";
                }
            });
        }
    </script>

</body>

</html>

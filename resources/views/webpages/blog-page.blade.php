<!DOCTYPE html>
<html lang="en">

<head>
    <title>Filing</title>
    @include('components/head-link')
</head>
@include('components/header')

<body class="s-it">
    
    <div class="up">
        <a href="#" class="scrollup text-center"><i class="fas fa-chevron-up"></i></a>
    </div>
    <section class="pb-5">

<div class=" page-banner">
    <span class=""></span>
    <div class="row">
        <div class="col-md-12">
            <div>
            <ul class="my-breadcrump">
               <li class="home-bread"><a href="index">Home</a></li>
                <li class=""><i class="fas fa-chevron-right my-arrow"><a class="home-bread-1" href="#">Blog</a></i></li>

            </ul>

           <h1 class="page-banner-heading">Blog</h1>

            </div>
        </div>
    </div>
</div>
</section>
<div class="news-page-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="news-box">
                                    <div class="news-img-holder">
                                        <img src="assets/img/its/blog.jpg" class="blog-images" alt="research">
                                        
                                    </div>
                                    <h2 class="blog-title"><a href="#">Bimply dummy text of the printing and typesetting industry</a></h2>
                                   
                                    <p>Bimply dummy text of the printing and typesetting istryrem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer.when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuriesp into electronic.</p>
                                    <!-- <a href="#" class="default-big-btn">Read More</a> -->
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="news-box">
                                    <div class="news-img-holder">
                                    <img src="assets/img/its/blog.jpg" class="blog-images" alt="research">
                                        
                                    </div>
                                    <h2 class="blog-title"><a href="#">Bimply dummy text of the printing and typesetting industry</a></h2>
                                    
                                    <p>Bimply dummy text of the printing and typesetting istryrem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer.when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuriesp into electronic.</p>
                                    <!-- <a href="#" class="default-big-btn">Read More</a> -->
                                </div>
                            </div>
                            
                            
                                
                            
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="sidebar">
                            
                            
                            <div class="sidebar-box">
                                <div class="sidebar-box-inner">
                                    <h3 class="">Latest Posts</h3>
                                    <div class="sidebar-latest-research-area">
                                        <ul class="sidebar-blogs">
                                            <li>
                                                <div class="latest-research-img">
                                                    <a href="#"><img src="assets/img/its/latest-post-1.jpg" class="img-responsive" alt="skilled"></a>
                                                </div>
                                                <div class="latest-research-content">
                                                    <h4>30 Nov, 2016</h4>
                                                    <p>when an unknown printer took.</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="latest-research-img">
                                                    <a href="#"><img src="assets/img/its/latest-post-1.jpg" class="img-responsive" alt="skilled"></a>
                                                </div>
                                                <div class="latest-research-content">
                                                    <h4>10 Aug, 2016</h4>
                                                    <p>when an unknown printer took.</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="latest-research-img">
                                                    <a href="#"><img src="assets/img/its/latest-post-1.jpg" class="img-responsive" alt="skilled"></a>
                                                </div>
                                                <div class="latest-research-content">
                                                    <h4>05 Jul, 2016</h4>
                                                    <p>when an unknown printer took.</p>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="latest-research-img">
                                                    <a href="#"><img src="assets/img/its/latest-post-1.jpg" class="img-responsive" alt="skilled"></a>
                                                </div>
                                                <div class="latest-research-content">
                                                    <h4>30 Feb, 2016</h4>
                                                    <p>when an unknown printer took.</p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="sidebar-box">
                                <div class="sidebar-box-inner">
                                    <h3 class="">Social</h3>
                                    <ul class="product-tags">
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        
                        <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        
                                    </ul>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>


 
    
    
    @include('components/footer')
    @include('components/foot-link')
    

</body>

</html>

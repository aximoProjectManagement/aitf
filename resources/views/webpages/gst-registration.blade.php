<!DOCTYPE html>
<html lang="en">

<head>
    <title>GST Registration</title>
    @include('components/head-link')
</head>
@include('components/header')

<body class="s-it">
    {{-- <div id="preloader"></div> --}}
    <div class="up">
        <a href="#" class="scrollup text-center"><i class="fas fa-chevron-up"></i></a>
    </div>
    <section class="pb-5">

<div class=" page-banner">
    <span class=""></span>
    <div class="row">
        <div class="col-md-12">
            <div>
            <ul class="my-breadcrump">
               <li class="home-bread"><a href="index">Home</a></li>
                <li class=""><i class="fas fa-chevron-right my-arrow"><a class="home-bread-1" href="registration">Registration</a></i></li>
                <li class=""><i class="fas fa-chevron-right my-arrow"><a class="home-bread-1" href="#">GST Registration</a></i></li>

            </ul>

           <h1 class="page-banner-heading">GST Registration</h1>

            </div>
        </div>
    </div>
</div>
</section>


    <!-- Start of banner section
 ============================================= -->
    <section id="it-up-banner" class="it-up-banner-section position-relative">
        <span class="it-up-banner-deco1 position-absolute"> <img src="assets/img/its/b-vector1.png" alt=""></span>
        <span class="it-up-banner-deco2 position-absolute"> <img src="assets/img/its/b-shape1.png" alt=""></span>
        <span class="it-up-banner-deco3 position-absolute"> <img src="assets/img/its/b-shape2.png" alt=""></span>
        <span class="it-up-banner-deco4 position-absolute"> <img src="assets/img/its/b-shape3.png" alt=""></span>
        <div class="container">

            <div class="it-up-banner-content position-relative ">
                <div class="it-up-banner-text headline-1 pera-content it-up-pages-text">

                    <!-- <h1>GST Registration</h1> -->
                    {{-- <p>Our vertical solutions expertise allows your business to streamline workflow.</p> --}}
                    <div>
                    <img src="assets/img/its/banner-pic/gst-regis.png" alt="gst registration" class="img-class img-fluid">
                    </div>

                </div>
                <div class="it-up-banner-img my-form">
                    <div class="card bg-light mb-3">
                        <div class="p-3">
                            <h5 class="text-center">GST REGISTRATION</h5>
                            <h5 class="text-center">Rs. 1500.00/- All Inclusive</h5>
                        </div>
                    </div>
                    <div class="card bg-light form-bg">
                        <article class="card-body ">

                            <form method="post" action="save_gstreg">
                                {{csrf_field()}}
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                                    </div>
                                    <input name="name" class="form-control" placeholder="Full name" type="text" required>
                                </div> <!-- form-group// -->
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                                    </div>
                                    <input name="email" class="form-control" placeholder="Email address" type="email" required>
                                </div> <!-- form-group// -->
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                                    </div>

                                    <input name="mobile" class="form-control" placeholder="Phone number" type="text" required>
                                </div>
                                <div class="form-group input-group">
                                    <div class="form-group w-100">
                                        <label for="comment">Type your message: </label>
                                        <textarea class="form-control" rows="" id="comment" name="message" required></textarea>
                                    </div>
                                </div>
                                <div>
                                    <div class="it-up-header-cta-btn  text-center">
                                        <button type="submit" class="new-btn">Submit Now</button>
                                    </div>
                                </div>

                            </form>
                        </article>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- End of banner section
 ============================================= -->

    <!-- Start of Featured section
 ============================================= -->
    <section id="it-up-featured" class="it-up-featured-section position-relative">
        <span class="it-up-ft-bg position-absolute"><img src="assets/img/its/ft-bg.png" alt=""></span>
        <span class="it-up-ft-shape position-absolute"><img src="assets/img/its/ft-shape1.png" alt=""></span>
        <span class="it-up-ft-shape2 position-absolute"><img src="assets/img/its/b-shape3.png" alt=""></span>
        <div class="container">
            <div class="it-up-section-title-2 headline-1 text-center">
                {{-- <span>Our Featured Services</span> --}}
                <h2>GST Registration</h2>
            </div>
            <div class="it-up-featured-content">
                <div class="row">
                    {{-- <div class="col-md-12">
                        <div class="it-up-featured-innerbox text-center">
                            <div class="it-up-featured-icon">
                                <img src="assets/img/its/icon/ft-icon1.png" alt="">
                            </div>
                            <div class="it-up-featured-text headline-1 pera-content">
                                <h3><a href="#">GST REGISTRATION</a></h3>
                                <p>As per the GST Council, entities in the Northeaster and hill states with an annual turnover of Rs.20 lakhs and above would be required to obtain GST registration..</p>
                                <a class="it-up-ft-more" href="#">Read more <i class="flaticon-right-arrow"></i></a>
                            </div>
                        </div>
                    </div> --}}
                    <div class="col-md-12">
                        <div>
                            <p class="section-p">As per the GST Council, entities in the Northeaster and hill states
                                with an annual turnover of Rs.20 lakhs and above would be required to obtain GST
                                registration. For all other entities in rest of India would be required to obtain GST
                                registration, if annual turnover exceeds Rs.40 lakhs. Entities required to obtain GST
                                registration as per regulations must file for GST registration within 30 days from the
                                date on which the entity became liable for obtaining GST registration.</p>
                            <br><br>
                            <p class="section-p">Allindiataxfiling.com is the leading business services platform in
                                India, offering a variety of services like GST registration, GST return filing, private
                                limited company registration, trademark filing and more. Allindiataxfiling.com can help
                                you obtain GST registration in India. The average time taken to obtain GST registration
                                is about 3 - 5 working days, subject to government processing time and client document
                                submission. </p>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
    <!-- End of Featured section
 ============================================= -->


    <!-- Start of about section
 ============================================= -->
    <section id="it-up-about" class="it-up-about-section">
        <div class="it-up-section-title-2 headline-1 text-center">

            <h2 class="pb-5">Process We Follow</h2>
        </div>
        <div class="timeline">
            <div class="containerr left">
                {{-- <div class="date">1</div> --}}
                <i class="icon fa ">1</i>
                <div class="content">
                    <h2>GST REGISTRATION</h2>
                    <p>
                        Allindiataxfiling.com can Register for GST in 3 to 5 days
                    </p>
                </div>
            </div>
            <div class="containerr right">
                {{-- <div class="date">2</div> --}}
                <i class="icon fa">2</i>
                <div class="content">
                    <h2>Document collection</h2>
                    <p>
                        We will certain documents to be submitted to GST Department.
                    </p>
                </div>
            </div>
            <div class="containerr left">
                {{-- <div class="date">3</div> --}}
                <i class="icon fa ">3</i>
                <div class="content">
                    <h2>GST APPLICATION</h2>
                    <p>
                        We draft your GST application and file it on your behalf.
                    </p>
                </div>
            </div>
            <div class="containerr right">
                {{-- <div class="date"></div> --}}
                <i class="icon fa ">4</i>
                <div class="content">
                    <h2>REGISTRATION APPROVAL</h2>
                    <p>
                        Once your GST registration is completed we will send GST license to you.
                    </p>
                </div>
            </div>
            {{-- <div class="containerr left">
              <div class="date">10 Feb</div>
              <i class="icon fa fa-cog"></i>
              <div class="content">
                <h2>Lorem ipsum dolor sit amet</h2>
                <p>
                  Lorem ipsum dolor sit amet elit. Aliquam odio dolor, id luctus erat sagittis non. Ut blandit semper pretium.
                </p>
              </div>
            </div>
            <div class="containerr right">
              <div class="date">01 Jan</div>
              <i class="icon fa fa-certificate"></i>
              <div class="content">
                <h2>Lorem ipsum dolor sit amet</h2>
                <p>
                  Lorem ipsum dolor sit amet elit. Aliquam odio dolor, id luctus erat sagittis non. Ut blandit semper pretium.
                </p>
              </div>
            </div> --}}
        </div>
    </section>
    <!-- End of about section
 ============================================= -->

    <!-- Start of achivement section
 ============================================= -->
    <section id="it-up-achivement" class="it-up-achivement-section position-relative">
        <span class="it-up-achive-shape1 position-absolute"><img src="assets/img/its/achive-bg.png" alt=""></span>
        <span class="it-up-achive-shape2 position-absolute"><img src="assets/img/its/achive-bg2.png" alt=""></span>
        <div class="container">
            <div class="it-up-section-title headline-1 text-center">

                <h2>FAQs</h2>
            </div>
            <div class="it-up-achivement-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="accordion" id="accordionExample">
                            <div class="card m-1">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left" type="button"
                                            data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne">
                                            Is this required?
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Yes it is.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseTwo"
                                            aria-expanded="false" aria-controls="collapseTwo">
                                            What is GST Registration?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Every business carrying out a taxable supply of goods or services under GST
                                        regime and whose turnover exceeds the threshold limit of Rs. 40 lakh/ 20 Lakh as
                                        applicable will be required to register as a normal taxable person. This process
                                        is of registration is referred as GST registration.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseThree"
                                            aria-expanded="false" aria-controls="collapseThree">
                                            Penalties for Not Registering Under GST
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        An offender not paying tax or making short payments has to pay a penalty of 10%
                                        of the tax amount due subject to a minimum of Rs.10,000. The penalty will be
                                        high at 100% of the tax amount when the offender has evaded i.e., where there is
                                        a deliberate fraud. However, for other genuine errors, the penalty is 10% of the
                                        tax due.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingFour">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseFour"
                                            aria-expanded="false" aria-controls="collapseFour">
                                            What is Goods and Services Tax (GST)
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        It is a destination based tax on consumption of goods and services. It is
                                        proposed to be levied at all stages right from manufacture up to final
                                        consumption with credit of taxes paid at previous stages available as set off.
                                        In a nutshell, only value addition will be taxed and burden of tax is to be
                                        borne by the final consumer.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingFive">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseFive"
                                            aria-expanded="false" aria-controls="collapseFive">
                                            Which of the existing taxes are proposed to be subsumed under GST?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFive" class="collapse" aria-labelledby="headingFive"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        The GST would replace the following taxes: (i) Taxes currently levied and
                                        collected by the Centre: Central Excise duty Duties of Excise (Medicinal and
                                        Toilet Preparations) Additional Duties of Excise (Goods of Special Importance)
                                        Additional Duties of Excise (Textiles and Textile Products) Additional Duties of
                                        Customs (commonly known as CVD) Special Additional Duty of Customs (SAD) Service
                                        Tax. Central Surcharges and Cesses so far as they relate to supply of goods and
                                        services (ii) State taxes that would be subsumed under the GST are: State VAT
                                        Central Sales Tax Luxury Taxd. Entry Tax (all forms) Entertainment and Amusement
                                        Tax (except when levied by the local bodies) Taxes on advertisements Purchase
                                        Tax. Taxes on lotteries, betting and gambling. State Surcharges and Cesses so
                                        far as they are late to supply of goods and services. The GST Council shall make
                                        recommendations to the Union and States on the taxes, cesses and surcharges
                                        levied by the Centre, the States and the local bodies which may be subsumed in
                                        the GST.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingSix">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseSix"
                                            aria-expanded="false" aria-controls="collapseSix">
                                            What type of GST is proposed to be implemented?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseSix" class="collapse" aria-labelledby="headingSix"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        It would be a dual GST with the Centre and States simultaneously levying it on a
                                        common tax base. The GST to be levied by the Centre on intra-State supply of
                                        goods and / or services would be called the Central GST (CGST) and that to be
                                        levied by the States/ Union territory would be called the State GST (SGST)/
                                        UTGST. Similarly, Integrated GST (IGST) will be levied and administered by
                                        Centre on every inter-state supply of goods and services.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingSeven">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseSeven"
                                            aria-expanded="false" aria-controls="collapseSeven">
                                            What is IGST?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Under the GST regime, an Integrated GST (IGST) would be levied and collected by
                                        the Centre on inter-State supply of goods and services. Under Article 269A of
                                        the Constitution, the GST on supplies in the course of inter-state trade or
                                        commerce shall be levied and collected by the Government of India and such tax
                                        shall be apportioned between the Union and the States in the manner as may be
                                        provided by Parliament by law on the recommendations of the Goods and Services
                                        Tax Council.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingEight">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseEight"
                                            aria-expanded="false" aria-controls="collapseEight">
                                            What are the benefits available to small tax payers under the GST regime?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseEight" class="collapse" aria-labelledby="headingEight"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Tax payers with an aggregate turnover in a financial year upto [Rs.40 lakhs &
                                        Rs.20 Lakhs for NE and special category states] would be exempt from GST
                                        registration.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingNine">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseNine"
                                            aria-expanded="false" aria-controls="collapseNine">
                                            How will the goods and services be classified under GST regime?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseNine" class="collapse" aria-labelledby="headingNine"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        HSN (Harmonised System of Nomenclature) code shall be used for classifying the
                                        goods under the GST regime. Taxpayers whose turnover is above Rs 1.5 crores but
                                        below Rs 5 crores shall use 2-digit code and the taxpayers whose turnover is Rs
                                        5 crores and above shall use 4-digit code. Taxpayers whose turnover is below Rs.
                                        1.5 crores are not required to mention HSN Code in their invoices. Services will
                                        be classified as per the Services Accounting Code (SAC).
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingTen">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseTen"
                                            aria-expanded="false" aria-controls="collapseTen">
                                            How will Exports be treated under GST?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTen" class="collapse" aria-labelledby="headingTen"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Exports will be treated as zero rated supplies. No tax will be payable on
                                        exports of goods or services, however credit of input tax credit will be
                                        available and same will be available as refund to the exporters. The Exporter
                                        will have an option to either pay tax on the output and claimrefund of IGST or
                                        export under Bond without payment of IGST and claim refund of Input Tax Credit
                                        (ITC).
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingEleven">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseEleven"
                                            aria-expanded="false" aria-controls="collapseEleven">
                                            What is Anti-Profiteering measure?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseEleven" class="collapse" aria-labelledby="headingEleven"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        As per section 171 of the CGST/SGST Act, any reduction in rate of tax on any
                                        supply of goods or services or the benefit of input tax credit shall be passed
                                        on to the recipient by way of commensurate reduction in prices. An authority may
                                        be constituted by the government to examine whether input tax credits availed by
                                        any registered person or the reduction in the tax rate have actually resulted in
                                        a commensurate reduction in the price of the goods or services or both supplied
                                        by him.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingTwelve">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseTwelve"
                                            aria-expanded="false" aria-controls="collapseTwelve">
                                            What is the taxable event under GST?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwelve" class="collapse" aria-labelledby="headingTwelve"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Taxable event under GST is supply of goods or services or both. CGST and SGST/
                                        UTGST will be levied on intra-State supplies. IGST will be levied on inter-State
                                        supplies.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingThirteen">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseThirteen"
                                            aria-expanded="false" aria-controls="collapseThirteen">
                                            Whether supplies made without consideration will also come within the
                                            purview of supply under GST?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseThirteen" class="collapse" aria-labelledby="headingThirteen"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Yes, but only those activities which are specified in Schedule I to the CGST Act
                                        / SGST Act. The said provision has been adopted in IGST Act as well as in UTGST
                                        Act also.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingFourteen">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseFourteen"
                                            aria-expanded="false" aria-controls="collapseFourteen">
                                            What is meant by Reverse Charge?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFourteen" class="collapse" aria-labelledby="headingFourteen"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        It means the liability to pay tax is on the recipient of supply of goods and
                                        services instead of the supplier of such goods or services in respect of
                                        notified categories of supply.
                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingFifteen">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseFifteen"
                                            aria-expanded="false" aria-controls="collapseFifteen">
                                            Is the reverse charge mechanism applicable only to services?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFifteen" class="collapse" aria-labelledby="headingFifteen"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        No, reverse charge applies to supplies of both goods and services, as notified
                                        by the Government on the recommendations of the GST Council.
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="it-up-achivement" class="it-up-achivement-section position-relative it-sec">
        <span class="it-up-achive-shape1 position-absolute"><img src="assets/img/its/achive-bg.png" alt=""></span>
        <span class="it-up-achive-shape2 position-absolute"><img src="assets/img/its/achive-bg2.png" alt=""></span>
        <div class="container">
            <div class="it-up-section-title headline-1 text-center">

                <h2>WHY AITF</h2>
            </div>
            <div class="it-up-achivement-content">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/taxes.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">24/7 SUPPORT</h5>
                                {{-- <p>Satisfied Clients</p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/idea.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">EXPERT ADVICE</h5>
                                {{-- <p>Team Member</p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/cyber-security.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">SECURITY</h5>
                                {{-- <p>Award Winner</p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/pie-chart.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">FILE IT RIGHT</h5>
                                {{-- <p>Works Done</p> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="it-up-service" class="it-up-service-section position-relative">
        <span class="it-up-service-shape position-absolute deco1"><img src="assets/img/its/s-shape1.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco2"><img src="assets/img/its/s-shape2.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco3"><img src="assets/img/its/s-shape3.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco4"><img src="assets/img/its/s-shape4.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco5"><img src="assets/img/its/s-shape5.png" alt=""></span>
        <div class="container">
            {{-- <div class="it-up-section-title headline-1 text-center">
               
                <h2>We are happy to share our client’s review.</h2>
            </div> --}}
            <div class="it-up-service-content">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="it-up-service-tab-btn">
                            <ul id="tabs" class="nav text-capitalize nav-tabs">
                                <li class="nav-item"><a href="#" data-target="#marketing" data-toggle="tab"
                                        class="nav-link text-capitalize active">WE ARE DIFFERENT!</a></li>
                                <li class="nav-item"><a href="#" data-target="#designing" data-toggle="tab"
                                        class="nav-link text-capitalize">WHY CHOOSE</a></li>
                                <li class="nav-item"><a href="#" data-target="#development" data-toggle="tab"
                                        class="nav-link text-capitalize">SERVICES OFFERING !</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="it-up-service-tab-content">
                            <div id="tabsContent" class="tab-content">
                                <div id="marketing" class="tab-pane fade active show">
                                    <div class="it-up-service-tab-wrap clearfix">
                                        <div
                                            class="it-up-service-tab-text position-relative float-left ul-li-block headline-1">
                                            <h3>WE ARE DIFFERENT!</h3>
                                            <h5>At Allindiataxfiling.com we ensure complete transparency at every step
                                                of the business incorporation process.We specialise in excellent
                                                customer service. When you call, email with our offices, you will
                                                receive personal attention from a knowledgeable specialist happy to help
                                                you .Our team of business startup specialists are here to keep you
                                                updated on the latest incorporation, tax and financial strategies and to
                                                help you manage important business details.

                                            </h5>
                                            {{-- <a class="it-up-ser-btn" href="#">Check Details <i class="flaticon-right-arrow"></i></a>
                                            <span class="it-up-tab-icon position-absolute"><img src="assets/img/its/icon/tab-icon1.png" alt=""></span> --}}
                                        </div>
                                        <div class="it-up-service-img float-right">
                                            <img src="assets/img/its/ser-tab1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div id="designing" class="tab-pane fade">
                                    <div class="it-up-service-tab-wrap clearfix">
                                        <div
                                            class="it-up-service-tab-text position-relative float-left ul-li-block headline-1">
                                            <h3>WHY CHOOSE</h3>
                                            <ul>
                                                <li>Specialized knowledge and experience in Taxation & Business
                                                    Registration.</li>
                                                <li>More communication than what larger agencies provide.</li>
                                                <li>Strategic partnership and advice for potential opportunities to grow
                                                    your brand.</li>
                                                <li>Work is done in-house and not outsourced to a third party.</li>
                                                <li>We practice what we preach and use our own services to grow our
                                                    business.</li>
                                            </ul>
                                            {{-- <a class="it-up-ser-btn" href="#">Check Details <i
                                                    class="flaticon-right-arrow"></i></a>
                                            <span class="it-up-tab-icon position-absolute"><img
                                                    src="assets/img/its/icon/tab-icon1.png" alt=""></span> --}}
                                        </div>
                                        <div class="it-up-service-img float-right">
                                            <img src="assets/img/its/ser-tab1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div id="development" class="tab-pane fade">
                                    <div class="it-up-service-tab-wrap clearfix">
                                        <div
                                            class="it-up-service-tab-text position-relative float-left ul-li-block headline-1">
                                            <h3>SERVICES OFFERING !</h3>
                                            <h5>Allindiataxfiling.com provides fast and end-to-end Business
                                                Incorporation and associated services. We also provide Accounting,
                                                Taxation and Legal Services. We guarantee hassle-free service delivery
                                                in the shortest possible time without compromising on quality.</h5>
                                            <h5>Our team of experienced professionals assists entrepreneurs in selecting
                                                the best business structure suiting individual requirements, and to
                                                convert their dream business into appropriate corporate entities.</h5>
                                            {{-- <a class="it-up-ser-btn" href="#">Check Details <i
                                                    class="flaticon-right-arrow"></i></a>
                                            <span class="it-up-tab-icon position-absolute"><img
                                                    src="assets/img/its/icon/tab-icon1.png" alt=""></span> --}}
                                        </div>
                                        <div class="it-up-service-img float-right">
                                            <img src="assets/img/its/ser-tab1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-3">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr">
                        <div class="counter-icon">
                            <i class="fas fa-medal"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Years</h3>
                            <span class="counter-value">10</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr green">
                        <div class="counter-icon">
                            <i class="fas fa-briefcase"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Projects</h3>
                            <span class="counter-value">947</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr blue">
                        <div class="counter-icon">
                            <i class="fas fa-user-tie"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Clients</h3>
                            <span class="counter-value">867</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr purple">
                        <div class="counter-icon">
                            <i class="fas fa-map-marker-alt"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Branches</h3>
                            <span class="counter-value">4</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </section>
    <section class="pt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 pt-5 pb-5">
                    <div class="it-up-section-title headline-1 text-center">
                        <h2>OUR PARTNERS</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="it-up-sponsor" class="it-up-sponsor-section">
                        <div class="container">
                            <div id="it-up-sponsor-slide" class="it-up-sponsor-slider owl-carousel">
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/gluehost.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/spellbound.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/aa.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/db.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/daxy.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img pt-4">
                                    <img src="assets/img/its/shrex.png" alt="">
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="it-up-testimonial" class="it-up-testimonial-section">
        <div class="container">
            <div class="it-up-section-title-2 headline-1 text-center">
                <span>Our Client’s Feedback</span>
                <h2>We are happy to share our client’s review.</h2>
            </div>
            <div class="it-up-testimonial-content position-relative">
                <span class="it-up-testi-shape1 position-absolute"><img src="assets/img/its/b-shape3.png" alt=""></span>
                <span class="it-up-testi-shape2 position-absolute" data-parallax='{"y" : 70}'><img src="assets/img/its/tst-sh1.png" alt=""></span>
                <span class="it-up-testi-shape3 position-absolute" data-parallax='{"x" : -70}'><img src="assets/img/its/tst-sh-1.png" alt=""></span>
                <span class="it-up-testi-shape4 position-absolute"><img src="assets/img/its/tst-sh3.png" alt=""></span>
                <div class="it-up-testimonial-slider-wrap owl-carousel">
                    <div class="it-up-testimonial-innerbox it-up-testimonial-green text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst2.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p>I really like the services provided by AITF for its admirable service, instructive and precise. 
AITF always accommodating and ready to support for any query. I do recommend AITF.
</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Abhishek Raj Rathod</a></h4>
                                <span>- Founder- Aximo infotech Pvt Ltd</span>
                            </div>
                        </div>
                    </div>
                    <div class="it-up-testimonial-innerbox it-up-testimonial-pink text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst1.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p>Everything from incorporation of your company to the trademark and all filing they coverd it all. one of the best CA consultant services they provide.</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Pranav Dhakulkar</a></h4>
                                <span>- Founder- Dryomix Concrete Solutions Pvt Ltd</span>
                            </div>
                        </div>
                    </div>
                    <div class="it-up-testimonial-innerbox it-up-testimonial-green text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst2.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p>In single sentence "AITF " is one stop solution for services Like Company Incorporation, Trademark Registration or any work related to CA. Cost & services are very good.</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Bestayur Lifecare</a></h4>
                                <span>- Bestayur Lifecare</span>
                            </div>
                        </div>
                    </div>
                    <div class="it-up-testimonial-innerbox it-up-testimonial-blue text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst3.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p>Professional services at competitive price. They give us complete end to end support from incorporation to expansion. Highly recommended.

                                Keep it up.</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Abhishek kumar</a></h4>
                                <span>-  Founder-Koibhikaamcube</span>
                            </div>
                        </div>
                    </div>
                   
                    
                </div>
            </div>
        </div>
    </section>
    @include('components/footer')
    @include('components/foot-link')
    <script>
        var acc = document.getElementsByClassName("accordion");
        var i;

        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var panel = this.nextElementSibling;
                if (panel.style.display === "block") {
                    panel.style.display = "none";
                } else {
                    panel.style.display = "block";
                }
            });
        }
    </script>

</body>

</html>

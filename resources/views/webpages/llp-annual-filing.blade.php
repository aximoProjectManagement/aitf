<!DOCTYPE html>
<html lang="en">

<head>
    <title>LLP Registration</title>
    @include('components/head-link')
</head>
@include('components/header')

<body class="s-it">
    {{-- <div id="preloader"></div> --}}
    <div class="up">
        <a href="#" class="scrollup text-center"><i class="fas fa-chevron-up"></i></a>
    </div>
    <section class="pb-5">

<div class=" page-banner">
    <span class=""></span>
    <div class="row">
        <div class="col-md-12">
            <div>
            <ul class="my-breadcrump">
               <li class="home-bread"><a href="index">Home</a></li>
                <li class=""><i class="fas fa-chevron-right my-arrow"><a class="home-bread-1" href="filing">Filing</a></i></li>
                <li class=""><i class="fas fa-chevron-right my-arrow"><a class="home-bread-1" href="#">LLP Annual Filing</a></i></li>

            </ul>

           <h1 class="page-banner-heading">LLP Annual Filing</h1>

            </div>
        </div>
    </div>
</div>
</section>


    <!-- Start of banner section
 ============================================= -->
    <section id="it-up-banner" class="it-up-banner-section position-relative">
        <span class="it-up-banner-deco1 position-absolute"> <img src="assets/img/its/b-vector1.png" alt=""></span>
        <span class="it-up-banner-deco2 position-absolute"> <img src="assets/img/its/b-shape1.png" alt=""></span>
        <span class="it-up-banner-deco3 position-absolute"> <img src="assets/img/its/b-shape2.png" alt=""></span>
        <span class="it-up-banner-deco4 position-absolute"> <img src="assets/img/its/b-shape3.png" alt=""></span>
        <div class="container">

            <div class="it-up-banner-content position-relative ">
                <div class="it-up-banner-text headline-1 pera-content it-up-pages-text">

                    <!-- <h1>LLP Annual Filing</h1> -->
                    {{-- <p>Our vertical solutions expertise allows your business to streamline workflow.</p> --}}
                    <div>
                    <img src="assets/img/its/banner-pic/llp-annual-filing.png" alt="gst registration" class="img-class img-fluid">
                    </div>

                </div>
                <div class="it-up-banner-img my-form">
                    <div class="card bg-light mb-3">
                        <div class="p-3">
                            <h5 class="text-center">LLP ANNUAL FILING</h5>
                            <h5 class="text-center">Rs. 6000.00/- All Inclusive</h5>
                        </div>
                    </div>
                    <div class="card bg-light form-bg">
                        <article class="card-body ">

                            <form method="post" action="save_llpannual">
                                {{csrf_field()}}
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-user"></i> </span>
                                    </div>
                                    <input name="name" class="form-control" placeholder="Full name" type="text" required>
                                </div> <!-- form-group// -->
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
                                    </div>
                                    <input name="email" class="form-control" placeholder="Email address" type="email" required>
                                </div> <!-- form-group// -->
                                <div class="form-group input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
                                    </div>

                                    <input name="mobile" class="form-control" placeholder="Phone number" type="text" required>
                                </div>
                                <div class="form-group input-group">
                                    <div class="form-group w-100">
                                        <label for="comment">Type your message: </label>
                                        <textarea class="form-control" rows="" id="comment" name="message" required></textarea>
                                    </div>
                                </div>
                                <div>
                                    <div class="it-up-header-cta-btn  text-center">
                                        <button type="submit" class="new-btn">Submit Now</button>
                                    </div>
                                </div>

                            </form>
                        </article>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- End of banner section
 ============================================= -->

    <!-- Start of Featured section
 ============================================= -->
    <section id="it-up-featured" class="it-up-featured-section position-relative">
        <span class="it-up-ft-bg position-absolute"><img src="assets/img/its/ft-bg.png" alt=""></span>
        <span class="it-up-ft-shape position-absolute"><img src="assets/img/its/ft-shape1.png" alt=""></span>
        <span class="it-up-ft-shape2 position-absolute"><img src="assets/img/its/b-shape3.png" alt=""></span>
        <div class="container">
            <div class="it-up-section-title-2 headline-1 text-center">
                {{-- <span>Our Featured Services</span> --}}
                <h2>LLP Annual Filing</h2>
            </div>
            <div class="it-up-featured-content">
                <div class="row">
                    {{-- <div class="col-md-12">
                        <div class="it-up-featured-innerbox text-center">
                            <div class="it-up-featured-icon">
                                <img src="assets/img/its/icon/ft-icon1.png" alt="">
                            </div>
                            <div class="it-up-featured-text headline-1 pera-content">
                                <h3><a href="#">GST REGISTRATION</a></h3>
                                <p>As per the GST Council, entities in the Northeaster and hill states with an annual turnover of Rs.20 lakhs and above would be required to obtain GST registration..</p>
                                <a class="it-up-ft-more" href="#">Read more <i class="flaticon-right-arrow"></i></a>
                            </div>
                        </div>
                    </div> --}}
                    <div class="col-md-12">
                        <div>
                            <p class="section-p">All LLPs shall be under obligation to maintain annual accounts
                                reflecting true and fair view of its state of affairs. Every Limited Liability
                                Partnership (LLP) is required to file LLP Form-8 (Statement of Account & Solvency) and
                                LLP Form-11 (Annual Return) annually. Even if LLP does not do any business, it has to
                                comply with statutory requirement such as Annual Return, Balance Sheet, Profit and loss
                                Account and Income tax return every year. The statutory fees will depend upon the
                                capital contribution of LLP.

                            </p>
                            <br><br>
                            <p class="section-p">Only those LLPs whose annual turnover exceeds Rs. 40 lakhs or whose
                                contribution exceeds Rs. 25 lakhs are required to get their accounts audited by a
                                qualified Chartered Accountant. In case of LLPs with turnover more than 5 crore rupees
                                in a financial year or contribution more than 50 lakh rupees, the annual return shall be
                                certified by a Company Secretary in Practice.

                            </p>
                            <br><br>
                            <p class="section-p">Form-8 is to be filed with the Registrar of LLP on or before 30th
                                October every year. Every LLP is required to file Annual Return in Form-11 to the
                                Registrar within 60 days from the closure of financial year i.e the Annual Returns has
                                to be filed on or before 30th May every year.



                            </p>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
    <!-- End of Featured section
 ============================================= -->
    <!-- start-pacakage section -->
    <div class="demo pt-5 mt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="it-up-section-title headline-1 text-center pb-5">

                        <h2>Packages</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="pricingTable">
                        <div class="pricing-content">
                            <div class="pricingTable-header">
                                <h3 class="title">Basic</h3>
                                <p>LLP with a Turnover of Less than 10 lac

                                </p>
                            </div>
                            <ul>
                                <li>Roc Return filing</li>

                                <li>Income tax Return Filing</li>
                                <li>Secretarial Practices
                                </li>
                            </ul>
                            <div class="price-value">
                                <p><strong>₹6000.00</p></strong>
                                    <p><strong> (All Inclusive)
                                        </p></strong>
                            </div>
                            <div>

                                <p>Just pay ₹3000.00 to start
                                </p>

                            </div>
                            <div class="pricingTable-signup">
                                <a href="#">Buy Now</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="pricingTable purple">
                        <div class="pricing-content">
                            <div class="pricingTable-header">
                                <h3 class="title">Advance</h3>
                                <p>LLP with a Turnover of Less than 40 lac

                                </p>
                            </div>
                            <ul>
                                <li>Roc Return filing</li>

                                <li>Income tax Return Filing</li>
                                <li>Secretarial Practices
                                </li>

                            </ul>
                            <div class="price-value">
                                <p><strong>₹8000.00</p></strong>
                                    <p><strong>(All Inclusive)
                                        </p></strong>
                            </div>
                            <div>

                                <p>Just pay ₹4000.00 to start
                                </p>

                            </div>
                            <div class="pricingTable-signup">
                                <a href="#">Buy Now</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="pricingTable orange">
                        <div class="pricing-content">
                            <div class="pricingTable-header">
                                <h3 class="title">Premium</h3>
                                <p>Turnover of Less than 100 lac

                                </p>
                            </div>
                            <ul>
                                <li>Roc Return filing</li>

                                <li>Income tax Return Filing</li>
                                <li>Secretarial Practices
                                </li>

                            </ul>
                            <div class="price-value">
                                <p><strong>₹12000.00</p></strong>
                                    <p><strong>(All Inclusive)
                                </span></p></strong>
                            <div>

                                <p>Just pay ₹6000.00 to start
                                </p>

                            </div>
                            <div class="pricingTable-signup">
                                <a href="#">Buy Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- end-package section -->




    <!-- Start of about section
 ============================================= -->
    <section id="it-up-about" class="it-up-about-section">
        <div class="it-up-section-title-2 headline-1 text-center">

            <h2 class="pb-5">Process We Follow</h2>
        </div>
        <div class="timeline">
            <div class="containerr left">
                {{-- <div class="date">1</div> --}}
                <i class="icon fa ">1</i>
                <div class="content">
                    <h2>LLP ANNUAL FILING
                    </h2>
                    <p>
                        Annual filing of LLP is mandatorily to be filed with ROC every year. Allindiataxfiling.com
                        assist you to maintain annual compliance with the Ministry of Corporate Affairs.


                    </p>
                </div>
            </div>
            <div class="containerr right">
                {{-- <div class="date">2</div> --}}
                <i class="icon fa">2</i>
                <div class="content">
                    <h2>ANNUAL RETURN PREPARATION
                    </h2>
                    <p>
                        Based on the financials and performance during the previous financial year,
                        Allindiataxfiling.com Expert will prepare the Annual return of your LLP.


                    </p>
                </div>
            </div>
            <div class="containerr left">
                {{-- <div class="date">3</div> --}}
                <i class="icon fa ">3</i>
                <div class="content">
                    <h2>ANNUAL RETURN VERIFICATION
                    </h2>
                    <p>
                        Once the Annual Return is prepared in the requisite format, the Client's can verify the prepared
                        annual return and affix the digital signature.


                    </p>
                </div>
            </div>
            <div class="containerr right">
                {{-- <div class="date"></div> --}}
                <i class="icon fa ">4</i>
                <div class="content">
                    <h2>ANNUAL RETURN UPLOAD
                    </h2>
                    <p>
                        Once DSC has been affixed then our expert will upload your return with Ministry of Corporate
                        Affairs.


                    </p>
                </div>
            </div>
            {{-- <div class="containerr left">
              <div class="date">10 Feb</div>
              <i class="icon fa fa-cog"></i>
              <div class="content">
                <h2>Lorem ipsum dolor sit amet</h2>
                <p>
                  Lorem ipsum dolor sit amet elit. Aliquam odio dolor, id luctus erat sagittis non. Ut blandit semper pretium.
                </p>
              </div>
            </div>
            <div class="containerr right">
              <div class="date">01 Jan</div>
              <i class="icon fa fa-certificate"></i>
              <div class="content">
                <h2>Lorem ipsum dolor sit amet</h2>
                <p>
                  Lorem ipsum dolor sit amet elit. Aliquam odio dolor, id luctus erat sagittis non. Ut blandit semper pretium.
                </p>
              </div>
            </div> --}}
        </div>
    </section>
    <!-- End of about section
 ============================================= -->

    <!-- Start of achivement section
 ============================================= -->
    <section id="it-up-achivement" class="it-up-achivement-section position-relative">
        <span class="it-up-achive-shape1 position-absolute"><img src="assets/img/its/achive-bg.png" alt=""></span>
        <span class="it-up-achive-shape2 position-absolute"><img src="assets/img/its/achive-bg2.png" alt=""></span>
        <div class="container">
            <div class="it-up-section-title headline-1 text-center">

                <h2>FAQs</h2>
            </div>
            <div class="it-up-achivement-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="accordion" id="accordionExample">
                            <div class="card m-1">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left" type="button"
                                            data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne">
                                            What is Annual Return?
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Annual return is a mandatory filing to be made by all LLPs in India. The annual
                                        return must be filed in the prescribed format with the Ministry of Corporate
                                        Affairs (MCA). Filing of annual return with the MCA is different from the filing
                                        of annual return with the Income Tax department.

                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseTwo"
                                            aria-expanded="false" aria-controls="collapseTwo">
                                            How are LLP Taxed?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        LLPs to be taxed on the lines similar to general partnerships under Indian
                                        Partnership Act, 1932, i.e. taxation in the hands of the entity and exemption
                                        from tax in the hands of its partners.

                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseThree"
                                            aria-expanded="false" aria-controls="collapseThree">
                                            What are the documents required to be filed by a LLP annually?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        LLP is required to file LLP Form-8 (Statement of Account & Solvency) and LLP
                                        Form-11 (Annual Return) annually. The ‘Annual Return’ is required to be filed
                                        within 60 days of close of the financial year and ‘Statement of Accounts &
                                        Solvency’ shall be filed within 30 days from the end of six months of the
                                        financial year to which it relates. Every LLP has to maintain uniform financial
                                        year ending on 31st March of a year.

                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingFour">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseFour"
                                            aria-expanded="false" aria-controls="collapseFour">
                                            Can the LLP be closed / wound up without filing Annual return?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        The LLP cannot be closed/ wound up unless all pending annual filing is complete.

                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingFive">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseFive"
                                            aria-expanded="false" aria-controls="collapseFive">
                                            What is the due date for filing LLP Annual Return?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFive" class="collapse" aria-labelledby="headingFive"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Annual return of a LLP is due within 60 days of close of financial year. LLPs
                                        must uniformly maintain a financial year that starts on April 1st and ends on
                                        March 31st, therefore the Annual return of a LLP is due on or before May 30th of
                                        each financial year.

                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingSix">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseSix"
                                            aria-expanded="false" aria-controls="collapseSix">
                                            Is certification of Company secretary compulsory in all cases?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseSix" class="collapse" aria-labelledby="headingSix"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        In case total obligation of contribution of partners of the LLP exceeds Rs. 50
                                        lakhs or turnover of LLP exceeds Rs. 5 crores, then the e-Form needs to be
                                        certified by a Company Secretary in whole time practice.

                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingSeven">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseSeven"
                                            aria-expanded="false" aria-controls="collapseSeven">
                                            I am a newly incorporated LLP, when is my annual return due?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        If a LLP was incorporated on or after 1st October of a financial year, then the
                                        LLP can close its first financial year either on the coming or next 31st March.
                                        Therefore, if required, a newly incorporated LLP can file its first Annual
                                        return and Statement of Accounts and Solvency for 18 months.

                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingEight">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseEight"
                                            aria-expanded="false" aria-controls="collapseEight">
                                            Is Annual Filing compulsory even though LLP has not done any transaction
                                            since incorporation?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseEight" class="collapse" aria-labelledby="headingEight"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Even though the LLP is not functional since incorporation, the LLP shall be
                                        required to file e-form-11 by its due date. Filing Form-11 is a compulsory
                                        annual compliance.

                                    </div>
                                </div>
                            </div>
                            <div class="card m-1">
                                <div class="card-header" id="headingNine">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link accordian-btn btn-block text-left collapsed"
                                            type="button" data-toggle="collapse" data-target="#collapseNine"
                                            aria-expanded="false" aria-controls="collapseNine">
                                            What is the due date for filing LLP Statement of Accounts and Solvency?
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseNine" class="collapse" aria-labelledby="headingNine"
                                    data-parent="#accordionExample">
                                    <div class="card-body">
                                        Statement of Accounts and Solvency of a LLP is due within 30 days from the end
                                        of six months of close of financial year. LLPs must uniformly maintain a
                                        financial year that starts on April 1st and ends on March 31st, therefore the
                                        Statement of Accounts and Solvency of a LLP is due on or before October 30th of
                                        each financial year.

                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="it-up-achivement" class="it-up-achivement-section position-relative it-sec">
        <span class="it-up-achive-shape1 position-absolute"><img src="assets/img/its/achive-bg.png" alt=""></span>
        <span class="it-up-achive-shape2 position-absolute"><img src="assets/img/its/achive-bg2.png" alt=""></span>
        <div class="container">
            <div class="it-up-section-title headline-1 text-center">

                <h2>WHY AITF</h2>
            </div>
            <div class="it-up-achivement-content">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/taxes.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">24/7 SUPPORT</h5>
                                {{-- <p>Satisfied Clients</p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/idea.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">EXPERT ADVICE</h5>
                                {{-- <p>Team Member</p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/cyber-security.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">SECURITY</h5>
                                {{-- <p>Award Winner</p> --}}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="it-up-achivement-innerbox position-relative text-center">
                            <span class="inner-border inner-border-2"></span>
                            <div class="it-up-achivement-icon">
                                <img src="assets/img/its/pie-chart.png" alt="">
                            </div>
                            <div class="it-up-achivement-text pera-content headline-1">
                                <h5 class="my-h5 pt-2">FILE IT RIGHT</h5>
                                {{-- <p>Works Done</p> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="it-up-service" class="it-up-service-section position-relative">
        <span class="it-up-service-shape position-absolute deco1"><img src="assets/img/its/s-shape1.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco2"><img src="assets/img/its/s-shape2.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco3"><img src="assets/img/its/s-shape3.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco4"><img src="assets/img/its/s-shape4.png" alt=""></span>
        <span class="it-up-service-shape position-absolute deco5"><img src="assets/img/its/s-shape5.png" alt=""></span>
        <div class="container">
            {{-- <div class="it-up-section-title headline-1 text-center">
               
                <h2>We are happy to share our client’s review.</h2>
            </div> --}}
            <div class="it-up-service-content">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="it-up-service-tab-btn">
                            <ul id="tabs" class="nav text-capitalize nav-tabs">
                                <li class="nav-item"><a href="#" data-target="#marketing" data-toggle="tab"
                                        class="nav-link text-capitalize active">WE ARE DIFFERENT!</a></li>
                                <li class="nav-item"><a href="#" data-target="#designing" data-toggle="tab"
                                        class="nav-link text-capitalize">WHY CHOOSE</a></li>
                                <li class="nav-item"><a href="#" data-target="#development" data-toggle="tab"
                                        class="nav-link text-capitalize">SERVICES OFFERING !</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="it-up-service-tab-content">
                            <div id="tabsContent" class="tab-content">
                                <div id="marketing" class="tab-pane fade active show">
                                    <div class="it-up-service-tab-wrap clearfix">
                                        <div
                                            class="it-up-service-tab-text position-relative float-left ul-li-block headline-1">
                                            <h3>WE ARE DIFFERENT!</h3>
                                            <h5>At Allindiataxfiling.com we ensure complete transparency at every step
                                                of the business incorporation process.We specialise in excellent
                                                customer service. When you call, email with our offices, you will
                                                receive personal attention from a knowledgeable specialist happy to help
                                                you .Our team of business startup specialists are here to keep you
                                                updated on the latest incorporation, tax and financial strategies and to
                                                help you manage important business details.

                                            </h5>
                                            {{-- <a class="it-up-ser-btn" href="#">Check Details <i class="flaticon-right-arrow"></i></a>
                                            <span class="it-up-tab-icon position-absolute"><img src="assets/img/its/icon/tab-icon1.png" alt=""></span> --}}
                                        </div>
                                        <div class="it-up-service-img float-right">
                                            <img src="assets/img/its/ser-tab1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div id="designing" class="tab-pane fade">
                                    <div class="it-up-service-tab-wrap clearfix">
                                        <div
                                            class="it-up-service-tab-text position-relative float-left ul-li-block headline-1">
                                            <h3>WHY CHOOSE</h3>
                                            <ul>
                                                <li>Specialized knowledge and experience in Taxation & Business
                                                    Registration.</li>
                                                <li>More communication than what larger agencies provide.</li>
                                                <li>Strategic partnership and advice for potential opportunities to grow
                                                    your brand.</li>
                                                <li>Work is done in-house and not outsourced to a third party.</li>
                                                <li>We practice what we preach and use our own services to grow our
                                                    business.</li>
                                            </ul>
                                            {{-- <a class="it-up-ser-btn" href="#">Check Details <i
                                                    class="flaticon-right-arrow"></i></a>
                                            <span class="it-up-tab-icon position-absolute"><img
                                                    src="assets/img/its/icon/tab-icon1.png" alt=""></span> --}}
                                        </div>
                                        <div class="it-up-service-img float-right">
                                            <img src="assets/img/its/ser-tab1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div id="development" class="tab-pane fade">
                                    <div class="it-up-service-tab-wrap clearfix">
                                        <div
                                            class="it-up-service-tab-text position-relative float-left ul-li-block headline-1">
                                            <h3>SERVICES OFFERING !</h3>
                                            <h5>Allindiataxfiling.com provides fast and end-to-end Business
                                                Incorporation and associated services. We also provide Accounting,
                                                Taxation and Legal Services. We guarantee hassle-free service delivery
                                                in the shortest possible time without compromising on quality.</h5>
                                            <h5>Our team of experienced professionals assists entrepreneurs in selecting
                                                the best business structure suiting individual requirements, and to
                                                convert their dream business into appropriate corporate entities.</h5>
                                            {{-- <a class="it-up-ser-btn" href="#">Check Details <i
                                                    class="flaticon-right-arrow"></i></a>
                                            <span class="it-up-tab-icon position-absolute"><img
                                                    src="assets/img/its/icon/tab-icon1.png" alt=""></span> --}}
                                        </div>
                                        <div class="it-up-service-img float-right">
                                            <img src="assets/img/its/ser-tab1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-3">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr">
                        <div class="counter-icon">
                            <i class="fas fa-medal"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Years</h3>
                            <span class="counter-value">10</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr green">
                        <div class="counter-icon">
                            <i class="fas fa-briefcase"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Projects</h3>
                            <span class="counter-value">947</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr blue">
                        <div class="counter-icon">
                            <i class="fas fa-user-tie"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Clients</h3>
                            <span class="counter-value">867</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counterrr purple">
                        <div class="counter-icon">
                            <i class="fas fa-map-marker-alt"></i>
                        </div>
                        <div class="counter-content">
                            <h3>Branches</h3>
                            <span class="counter-value">4</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </section>

    <section class="pt-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 pt-5 pb-5">
                    <div class="it-up-section-title headline-1 text-center">
                        <h2>OUR PARTNERS</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="it-up-sponsor" class="it-up-sponsor-section">
                        <div class="container">
                            <div id="it-up-sponsor-slide" class="it-up-sponsor-slider owl-carousel">
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/gluehost.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/spellbound.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/aa.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/db.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img">
                                    <img src="assets/img/its/daxy.png" alt="">
                                </div>
                                <div class="it-up-sponsor-img pt-4">
                                    <img src="assets/img/its/shrex.png" alt="">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="it-up-testimonial" class="it-up-testimonial-section">
        <div class="container">
            <div class="it-up-section-title-2 headline-1 text-center">
                <span>Our Client’s Feedback</span>
                <h2>We are happy to share our client’s review.</h2>
            </div>
            <div class="it-up-testimonial-content position-relative">
                <span class="it-up-testi-shape1 position-absolute"><img src="assets/img/its/b-shape3.png" alt=""></span>
                <span class="it-up-testi-shape2 position-absolute" data-parallax='{"y" : 70}'><img
                        src="assets/img/its/tst-sh1.png" alt=""></span>
                <span class="it-up-testi-shape3 position-absolute" data-parallax='{"x" : -70}'><img
                        src="assets/img/its/tst-sh-1.png" alt=""></span>
                <span class="it-up-testi-shape4 position-absolute"><img src="assets/img/its/tst-sh3.png" alt=""></span>
                <div class="it-up-testimonial-slider-wrap owl-carousel">
                    <div class="it-up-testimonial-innerbox it-up-testimonial-green text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst2.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p>I really like the services provided by AITF for its admirable service, instructive and precise. 
AITF always accommodating and ready to support for any query. I do recommend AITF.
</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Abhishek Raj Rathod</a></h4>
                                <span>- Founder- Aximo infotech Pvt Ltd</span>
                            </div>
                        </div>
                    </div>
                    <div class="it-up-testimonial-innerbox it-up-testimonial-pink text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst1.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p>Everything from incorporation of your company to the trademark and all filing they coverd
                                it all. one of the best CA consultant services they provide.</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Pranav Dhakulkar</a></h4>
                                <span>- Founder- Dryomix Concrete Solutions Pvt Ltd</span>
                            </div>
                        </div>
                    </div>
                    <div class="it-up-testimonial-innerbox it-up-testimonial-green text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst2.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p>In single sentence "AITF " is one stop solution for services Like Company Incorporation,
                                Trademark Registration or any work related to CA. Cost & services are very good.</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Bestayur Lifecare</a></h4>
                                <span>- Bestayur Lifecare</span>
                            </div>
                        </div>
                    </div>
                    <div class="it-up-testimonial-innerbox it-up-testimonial-blue text-center">
                        <div class="it-up-testimonial-img-wrap position-relative">
                            <div class="it-up-testimonial-img">
                                {{-- <img src="assets/img/its/tst3.jpg"> --}}
                            </div>
                            <span class="quote-sign">“</span>
                        </div>
                        <div class="it-up-testimonial-text pera-content headline-1">
                            <p>Professional services at competitive price. They give us complete end to end support from
                                incorporation to expansion. Highly recommended.

                                Keep it up.</p>
                            <div class="it-up-testi-author position-relative">
                                <h4><a href="#">Abhishek kumar</a></h4>
                                <span>- Founder-Koibhikaamcube</span>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>
    @include('components/footer')
    @include('components/foot-link')
    <script>
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.display === "block") {
                panel.style.display = "none";
            } else {
                panel.style.display = "block";
            }
        });
    }
    </script>

</body>

</html>
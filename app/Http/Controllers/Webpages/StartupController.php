<?php

namespace App\Http\Controllers\Webpages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Proprietorship;
use App\Models\Partnership;
use App\Models\OnePersonCompany;
use App\Models\PrivateLimitedCompany;
use App\Models\LimitedLiabilityPartnership;
use App\Models\PublicLimitedCompany;
use App\Models\Section8Company;


class StartupController extends Controller
{
    public function index(){
        return view('webpages.startups');
    }
    public function Propritership(){
        return view('webpages.propritership');
    }
    public function Partnership(){
        return view('webpages.partnership');
    }
    public function OnePersonCompany(){
        return view('webpages.one-person-company');
    }
    public function PrivateLimitedCompany(){
        return view('webpages.private-limited-company');
    }
    public function LLP(){
        return view('webpages.limited-liability-partnership');
    }
    public function PublicLimitedCompany(){
        return view('webpages.public-limited-company');
    }
    public function Section_8(){
        return view('webpages.section-8-company-ngo');
    }

    public function saveProprietorship(Request $request){
        $data = new Proprietorship;
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->mobile = $request->input('mobile');
        $data->message = $request->input('message');
        $data->save();
        return redirect('/propritership');
    }
    public function savePartnership(Request $request){
        $data = new Partnership;
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->mobile = $request->input('mobile');
        $data->message = $request->input('message');
        $data->save();
        return redirect('/partnership');
    }
    public function saveOnePersonCompany(Request $request){
        $data = new OnePersonCompany;
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->mobile = $request->input('mobile');
        $data->message = $request->input('message');
        $data->save();
        return redirect('/one_person_company');
    }
    public function savePrivateLimitedCompany(Request $request){
        $data = new PrivateLimitedCompany;
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->mobile = $request->input('mobile');
        $data->message = $request->input('message');
        $data->save();
        return redirect('/private_limited_company');
    }
    public function saveLimitedLiabilityPartnership(Request $request){
        $data = new LimitedLiabilityPartnership;
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->mobile = $request->input('mobile');
        $data->message = $request->input('message');
        $data->save();
        return redirect('/llp');
    }
    public function savePublicLimitedCompany(Request $request){
        $data = new PublicLimitedCompany;
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->mobile = $request->input('mobile');
        $data->message = $request->input('message');
        $data->save();
        return redirect('/public_limited_company');
    }
    public function saveSection8Company(Request $request){
        $data = new Section8Company;
        $data->name = $request->input('name');
        $data->email = $request->input('email');
        $data->mobile = $request->input('mobile');
        $data->message = $request->input('message');
        $data->save();
        return redirect('/section_8_company');
    }
    
}

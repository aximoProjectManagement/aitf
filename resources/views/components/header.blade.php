<header id="it-header-up" class="it-header-up-seaction">
    <div class="it-header-up-top clearfix">
            <div class="container">
                <div class="it-header-top-cta float-left">
                    <a href="#"><i class="fas fa-envelope"></i> info@webmail.com</a>
                    <a href="#"><i class="fas fa-phone"></i> +987 876 765 65 5</a>
                    <a href="#"><i class="fas fa-map-marker-alt"></i> 14/A, Brown City, NYC</a>
                </div>
                <div class="it-header-top-social float-right">
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                    <a href="#"><i class="fab fa-behance"></i></a>
                    <a href="#"><i class="fab fa-youtube"></i></a>
                </div>
            </div>
        </div>
    <div class="it-up-header-main">
        <div class="container">
            <div class="it-up-brand-logo float-left">
                <a href="/"><img src="assets/img/its/logo-1.png" alt=""></a>
            </div>
            <div class="it-up-main-menu-wrap clearfix">
                <nav class="it-up-main-navigation float-left ul-li">
                    <ul id="main-nav" class="navbar-nav text-capitalize clearfix">
                        <li class="">
                            <a href="/" class="hvr-underline-from-left ">Home</a>

                        </li>
                        <li class=" ">
                            <a href="about_us" class="hvr-underline-from-left">About</a>

                        </li>
                        <li class="dropdown"><a href="start_ups" class="hvr-underline-from-left">Startups</a>
                            <ul class="dropdown-menu clearfix">
                                <li><a href="propritership" >Proprietorship</a></li>
                                <li><a href="partnership">Partnership</a></li>
                                <li><a href="one_person_company">One Person Company</a></li>
                                <li><a href="private_limited_company">Private Limited Company</a></li>
                                <li><a href="llp">Limited Liability Partnership</a></li>
                                <li><a href="public_limited_company">Public Limited Company</a></li>
                                <li><a href="section_8_company">Section 8 Company(NGO)</a></li>

                            </ul>
                        </li>
                        <li class="dropdown"><a href="filing" class="hvr-underline-from-left">Filing</a>
                            <ul class="dropdown-menu clearfix">
                                <li><a href="income_tax_return">Income Tax Return</a></li>
                                <li><a href="tds_return">TDS Return</a></li>
                                <li><a href="company_annual_filling">Company Annual filing</a></li>
                                <li><a href="llp_annual_filing">LLP Annual filing</a></li>
                                <li><a href="digital_signature">Digital Signature Certificate (DSC)</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="registration" class="hvr-underline-from-left">Registration</a>
                            <ul class="dropdown-menu clearfix">
                                <li><a href="gst_regis">GST Registration</a></li>
                                <li><a href="import_export">Import Export Code</a></li>
                                <li><a href="trademark_registration">Trademark Registration</a></li>
                                <li><a href="gem_registration">GEM Registration</a></li>
                                <li><a href="startup_india">Startup India Registration</a></li>
                                <li><a href="msme_registration">MSME Registration</a></li>
                            </ul>
                        </li>
                        <li><a href="contact_us" class="hvr-underline-from-left">Contact Us</a></li>
                       
                    </ul>
                </nav>
                <div class="it-up-header-cta-btn float-right text-center">
                    <a href="#">PAY</a>
                </div>
            </div>
            <div class="mobile_menu relative-position">
                <div class="mobile_menu_button it-up-open_mobile_menu">
                    <i class="fas fa-bars"></i>
                </div>
                <div class="it-up-mobile_menu_wrap">
                    <div class="mobile_menu_overlay it-up-open_mobile_menu"></div>
                    <div class="mobile_menu_content">
                        <div class="mobile_menu_close it-up-open_mobile_menu">
                            <i class="far fa-times-circle"></i>
                        </div>
                        <div class="m-brand-logo text-center">
                            <img src="assets/img/its/logo-1.png" alt="">
                        </div>
                        <nav class="main-navigation mobile_menu-dropdown  clearfix ul-li">
                            <ul id="main-nav" class="navbar-nav text-capitalize clearfix">
                                <li class="dropdown">
                                    <a href="/">Home</a>
                                   
                                </li>
                                <li class="dropdown">
                                    <a href="about_us">About</a>
                                    {{-- <ul class="dropdown-menu clearfix">
                                        <li><a href="#">DropDown 1</a></li>
                                        <li><a href="#">DropDown 2</a></li>
                                        <li><a href="#">DropDown 3</a></li>
                                    </ul> --}}
                                </li>
                                <li class="dropdown"><a href="start_ups" class="hvr-underline-from-left">Startups</a>
                                    <ul class="dropdown-menu clearfix">
                                        <li><a href="propritership">Propritership</a></li>
                                        <li><a href="partnership">Partnership</a></li>
                                        <li><a href="one_person_company">One Person Company</a></li>
                                        <li><a href="private_limited_company">Private Limited Company</a></li>
                                        <li><a href="llp">Limited Liability Partnership</a></li>
                                        <li><a href="public_limited_company">Public Limited Company</a></li>
                                        <li><a href="section_8_company">Section 8 Company(NGO)</a></li>
        
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="filing" class="hvr-underline-from-left">Filing</a>
                                    <ul class="dropdown-menu clearfix">
                                        <li><a href="income_tax_return">Income Tax Return</a></li>
                                        <li><a href="tds_return">TDS Return</a></li>
                                        <li><a href="company_annual_filling">Company Annual filing</a></li>
                                        <li><a href="llp_annual_filing">LLP Annual filing</a></li>
                                        <li><a href="digital_signature">Digital Signature Certificate (DSC)</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown"><a href="registration" class="hvr-underline-from-left">Registration</a>
                                    <ul class="dropdown-menu clearfix">
                                        <li><a href="gst_regis">GST Registration</a></li>
                                        <li><a href="import_export">Import Export Code</a></li>
                                        <li><a href="trademark_registration">Trademark Registration</a></li>
                                        <li><a href="gem_registration">GEM Registration</a></li>
                                        <li><a href="startup_india">Startup India Registration</a></li>
                                        <li><a href="msme_registration">MSME Registration</a></li>
                                    </ul>
                                </li>
                                <li><a href="contact_us" class="hvr-underline-from-left">Contact Us</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

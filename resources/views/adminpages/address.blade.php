<!DOCTYPE html>
<html lang="en">

<head>
<?php include './include/head-link.php'; ?>

 

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <?php include './include/side-bar.php'; ?>
        
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <?php include './include/header.php'; ?>
               
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                   
                    <!-- <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below.
                        For more information about DataTables, please visit the <a target="_blank"
                            href="https://datatables.net">official DataTables documentation</a>.</p> -->

                    <!-- DataTales Example -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-2 text-gray-800">Address</h1>
                        <a href="add-address.php" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                        <i class="fas fa-plus fa-sm "></i> Add Address</a>
                    </div>
                    <div class="card shadow mb-4">
                        
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Address</th>
                                            <th>City</th>
                                            <th>State</th>
                                            <th>Pincode</th>
                                            
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        <tr>
                                            <td>Tiger Nixon</td>
                                            <td>info@gmail.com</td>
                                            <td>7654327897</td>
                                            <td>helo</td>
                                        
                                            <td>
                                        <a href="edit-address.php"><button data-toggle="tooltip" title="Edit"
                                                class="pd-setting-ed"><i class="fas fa-pencil-alt"></i></button></a>

                                        <a href="#"><button data-toggle="tooltip" title="Trash" class="pd-setting-ed-1"><i class="fas fa-trash"></i></button></a>
                                    </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>Garrett Winters</td>
                                            <td>info@gmail.com</td>
                                            <td>7654327897</td>
                                            <td>helo</td>
                                        
                                            <td>
                                            <a href="edit-address.php"><button data-toggle="tooltip" title="Edit"
                                                class="pd-setting-ed"><i class="fas fa-pencil-alt"></i></button></a>

                                        <a href="#"><button data-toggle="tooltip" title="Trash" class="pd-setting-ed-1"><i class="fas fa-trash"></i></button></a>
                                    </td>                                        
                                        </tr>
                                        <tr>
                                            <td>Ashton Cox</td>
                                            <td>info@gmail.com</td>
                                            <td>7654327897</td>
                                            <td>helo</td>
                                        
                                            <td>
                                            <a href="edit-address.php"><button data-toggle="tooltip" title="Edit"
                                                class="pd-setting-ed"><i class="fas fa-pencil-alt"></i></button></a>

                                        <a href="#"><button data-toggle="tooltip" title="Trash" class="pd-setting-ed-1"><i class="fas fa-trash"></i></button></a>
                                    </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>Cedric Kelly</td>
                                            <td>info@gmail.com</td>
                                            <td>7654327897</td>
                                            <td>helo</td>
                                        
                                            <td>
                                            <a href="edit-address.php"><button data-toggle="tooltip" title="Edit"
                                                class="pd-setting-ed"><i class="fas fa-pencil-alt"></i></button></a>

                                        <a href="#"><button data-toggle="tooltip" title="Trash" class="pd-setting-ed-1"><i class="fas fa-trash"></i></button></a>
                                    </td>
                    
                                        </tr>
                                        <tr>
                                            <td>Airi Satou</td>
                                            <td>info@gmail.com</td>
                                            <td>7654327897</td>
                                            <td>helo</td>
                                        
                                            <td>
                                            <a href="edit-address.php"><button data-toggle="tooltip" title="Edit"
                                                class="pd-setting-ed"><i class="fas fa-pencil-alt"></i></button></a>

                                        <a href="#"><button data-toggle="tooltip" title="Trash" class="pd-setting-ed-1"><i class="fas fa-trash"></i></button></a>
                                    </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>Brielle Williamson</td>
                                            <td>info@gmail.com</td>
                                            <td>7654327897</td>
                                            <td>helo</td>
                                        
                                            <td>
                                            <a href="edit-address.php"><button data-toggle="tooltip" title="Edit"
                                                class="pd-setting-ed"><i class="fas fa-pencil-alt"></i></button></a>

                                        <a href="#"><button data-toggle="tooltip" title="Trash" class="pd-setting-ed-1"><i class="fas fa-trash"></i></button></a>
                                    </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>Herrod Chandler</td>
                                            <td>info@gmail.com</td>
                                            <td>7654327897</td>
                                            <td>helo</td>
                                        
                                            <td>
                                            <a href="edit-address.php"><button data-toggle="tooltip" title="Edit"
                                                class="pd-setting-ed"><i class="fas fa-pencil-alt"></i></button></a>

                                        <a href="#"><button data-toggle="tooltip" title="Trash" class="pd-setting-ed-1"><i class="fas fa-trash"></i></button></a>
                                    </td>                                
                                        </tr>
                                        <tr>
                                            <td>Rhona Davidson</td>
                                            <td>info@gmail.com</td>
                                            <td>7654327897</td>
                                            <td>helo</td>
                                        
                                            <td>
                                            <a href="edit-address.php"><button data-toggle="tooltip" title="Edit"
                                                class="pd-setting-ed"><i class="fas fa-pencil-alt"></i></button></a>

                                        <a href="#"><button data-toggle="tooltip" title="Trash" class="pd-setting-ed-1"><i class="fas fa-trash"></i></button></a>
                                    </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>Colleen Hurst</td>
                                            <td>info@gmail.com</td>
                                            <td>7654327897</td>
                                            <td>helo</td>
                                        
                                            <td>
                                            <a href="edit-address.php"><button data-toggle="tooltip" title="Edit"
                                                class="pd-setting-ed"><i class="fas fa-pencil-alt"></i></button></a>

                                        <a href="#"><button data-toggle="tooltip" title="Trash" class="pd-setting-ed-1"><i class="fas fa-trash"></i></button></a>
                                    </td>
                                
                                        </tr>
                                        <tr>
                                            <td>Sonya Frost</td>
                                            <td>info@gmail.com</td>
                                            <td>7654327897</td>
                                            <td>helo</td>
                                        
                                            <td>
                                            <a href="edit-address.php"><button data-toggle="tooltip" title="Edit"
                                                class="pd-setting-ed"><i class="fas fa-pencil-alt"></i></button></a>

                                        <a href="#"><button data-toggle="tooltip" title="Trash" class="pd-setting-ed-1"><i class="fas fa-trash"></i></button></a>
                                    </td>
                            
                                        </tr>
                                        <tr>
                                            <td>Jena Gaines</td>
                                            <td>info@gmail.com</td>
                                            <td>7654327897</td>
                                            <td>helo</td>
                                        
                                            <td>
                                            <a href="edit-address.php"><button data-toggle="tooltip" title="Edit"
                                                class="pd-setting-ed"><i class="fas fa-pencil-alt"></i></button></a>

                                        <a href="#"><button data-toggle="tooltip" title="Trash" class="pd-setting-ed-1"><i class="fas fa-trash"></i></button></a>
                                    </td>
                            
                                        </tr>
                                        <tr>
                                            <td>Quinn Flynn</td>
                                            <td>info@gmail.com</td>
                                            <td>7654327897</td>
                                            <td>helo</td>
                                        
                                            <td>
                                            <a href="edit-address.php"><button data-toggle="tooltip" title="Edit"
                                                class="pd-setting-ed"><i class="fas fa-pencil-alt"></i></button></a>

                                        <a href="#"><button data-toggle="tooltip" title="Trash" class="pd-setting-ed-1"><i class="fas fa-trash"></i></button></a>
                                    </td>
                                    
                                        </tr>
                                        <tr>
                                            <td>Charde Marshall</td>
                                            <td>info@gmail.com</td>
                                            <td>7654327897</td>
                                            <td>helo</td>
                                        
                                            <td>
                                            <a href="edit-address.php"><button data-toggle="tooltip" title="Edit"
                                                class="pd-setting-ed"><i class="fas fa-pencil-alt"></i></button></a>

                                        <a href="#"><button data-toggle="tooltip" title="Trash" class="pd-setting-ed-1"><i class="fas fa-trash"></i></button></a>
                                    </td>
                                        
                                        </tr>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
            <?php include './include/footer.php'; ?>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
   <?php include './include/foot-link.php'; ?>

</body>

</html>
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LlpAnnualFiling extends Model
{
    protected $table = 'llp_annual_filing';
    use HasFactory;
}

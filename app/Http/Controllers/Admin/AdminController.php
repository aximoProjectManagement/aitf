<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\Proprietorship;
use App\Models\Partnership;
use App\Models\OnePersonCompany;
use App\Models\PrivateLimitedCompany;
use App\Models\LimitedLiabilityPartnership;
use App\Models\PublicLimitedCompany;
use App\Models\Section8Company;
use App\Models\IncomeTaxReturn;
use App\Models\TdsReturn;
use App\Models\CompanyAnnualFiling;
use App\Models\LlpAnnualFiling;
use App\Models\DigitalSignatureCertificate;
use App\Models\GstRegistration;
use App\Models\ImportExportRegistration;
use App\Models\TrademarkRegistration;
use App\Models\GemRegistration;
use App\Models\StartupIndiaRegistration;
use App\Models\MsmeRegistration;
use App\Models\Blogs;
use App\Models\Clients;
use App\Models\Blog;
use App\Models\WhoWeAreBanner;
use App\Models\Packages;
use App\Models\service;





class AdminController extends Controller
{
    public function index(){
        return view('adminpages.login');
    }
    public function dashboard(){
        return view('adminpages.index');
    }
    public function services(){

        return view('adminpages.service');
    }
    public function packages(){
        $data = Packages::all();
        return view('adminpages.packages',['data'=>$data]);
    }
    public function proprietorship(){
        $data = Proprietorship::all();
        return view('adminpages.proprietorship',['data'=>$data]);
    }
    public function partnership(){
        $data = Partnership::all();
        return view('adminpages.partnership',['data'=>$data]);
    }
    public function onepcompanyy(){
        $data = OnePersonCompany::all();
        return view('adminpages.one-person-company',['data'=>$data]);
    }
    public function PrivateLimited(){
        $data = PrivateLimitedCompany::all();
        return view('adminpages.private-limited',['data'=>$data]);
    }
    public function LimitedLiability(){
        $data = LimitedLiabilityPartnership::all();
        return view('adminpages.limited-liability',['data'=>$data]);
    }
    public function PublicLimited(){
        $data = PublicLimitedCompany::all();
        return view('adminpages.public-limited',['data'=>$data]);
    }
    public function Section_8(){
        $data = Section8Company::all();
        return view('adminpages.section-8',['data'=>$data]);
    }
    public function IncomeTaxReturn(){
        $data = IncomeTaxReturn::all();
        return view('adminpages.income-tax',['data'=>$data]);
    }
    public function TdsReturn(){
        $data = TdsReturn::all();
        return view('adminpages.tds-return',['data'=>$data]);
    }
    public function CompanyAnnualFiling(){
        $data = CompanyAnnualFiling::all();
        return view('adminpages.company-annual',['data'=>$data]);
    }
    public function LlpAnnualFiling(){
        $data = LlpAnnualFiling::all();
        return view('adminpages.llp-annual',['data'=>$data]);
    }
    public function DigitalSignatureCertificate(){
        $data = DigitalSignatureCertificate::all();
        return view('adminpages.digital-signature',['data'=>$data]);
    }
    public function GstRegistration(){
        $data = GstRegistration::all();
        return view('adminpages.gst-registration',['data'=>$data]);
    }
    public function ImportExport(){
        $data = ImportExportRegistration::all();
        return view('adminpages.import-export',['data'=>$data]);
    }
    public function TrademarkRegistration(){
        $data = TrademarkRegistration::all();
        return view('adminpages.trademark-registration',['data'=>$data]);
    }
    public function GemRegistration(){
        $data = GemRegistration::all();
        return view('adminpages.gem-registration',['data'=>$data]);
    }
    public function StartupIndiaRegistration(){
        $data = StartupIndiaRegistration::all();
        return view('adminpages.start-up-india',['data'=>$data]);
    }
    public function MsmeRegistration(){
        $data = MsmeRegistration::all();
        return view('adminpages.msme-registration',['data'=>$data]);
    }
    public function MyProfile(){
        return view('adminpages.my-profile');
    }
    public function HomeBanner(){
        return view('adminpages.home-banner');
    }
    public function ServiceDashboard(){
        $data = service::all();
        return view('adminpages.service',['data'=>$data]);
    }
    public function ServiceDashboard_delete($id){
        service::destroy($id);
        return back();
    }


    public function ServiceDashboard_update($id, Request $request)
    {
        if($request->hasFile('image')){
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('assets/servicesimg'), $imageName);

            $data = service::find($id);

            $data->heading= $request->input('heading');
            $data->description = $request->input('description');
            $data->read_more_link = $request->input('read_more_link');
            $data->image = $imageName;
            $data->save();
            return redirect('/admin/service_dashboard');
        }

    }

    public function WhoWeAre(){
        $data = WhoweareBanner::all();
        return view('adminpages.who-we-are',['data'=>$data]);
    }
    public function Partner(){
        return view('adminpages.partners');
    }
    public function Client(){
        $data = Clients::all();
        return view('adminpages.client-review',['data'=>$data]);
    }
    public function Blogs(){
        $data = Blog::all();
        return view('adminpages.blogs',['data'=>$data]);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GstRegistration extends Model
{
    protected $table = 'gst_registration';
    use HasFactory;
}

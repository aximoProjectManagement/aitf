<script src="assets/js/jquery.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/owl.js"></script>
<script src="assets/js/jquery.magnific-popup.min.js"></script>
<script src="assets/js/appear.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/parallax-scroll.js"></script>
<script src="assets/js/circle-progress.js"></script>
<script src="assets/js/jquery.counterup.min.js"></script>
<script src="assets/js/waypoints.min.js"></script>
<script src="assets/js/typer-new.js"></script>
<script src="assets/js/it-source.js"></script>

<script>
    $(document).ready(function(){
$('.counter-value').each(function(){
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    },{
        duration: 3500,
        easing: 'swing',
        step: function (now){
            $(this).text(Math.ceil(now));
        }
    });
});
});
</script>
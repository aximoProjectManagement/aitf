<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PublicLimitedCompany extends Model
{
    protected $table = 'public_limited_company';
    use HasFactory;
}
